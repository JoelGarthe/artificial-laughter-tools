from sklearn.manifold import TSNE
from numpy import reshape
import pickle
import matplotlib.pyplot as plt
import sys

file_path = sys.argv[1]

if len(sys.argv) == 1:
    print(">>> No data file was given!")

# open a file, where you stored the pickled data
file = open(file_path, 'rb')

# load pickle dataset
data = pickle.load(file)

print(data.shape)

tsne = TSNE(n_components=2, verbose=1, random_state=141414, learning_rate="auto", perplexity = 10)

data_reshaped = reshape(data, [data.shape[0], data.shape[1]*data.shape[2]])


z = tsne.fit_transform(data_reshaped) 


plt.figure(figsize=[25,10])
plt.scatter(
    z[:, 0],
    z[:, 1]
)
plt.gca().set_aspect('equal', 'datalim')
plt.title('t-SNE projection of the MNIST dataset', fontsize=24)

plt.show()

