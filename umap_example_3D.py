# - import libraries
import numpy as np
import matplotlib.pyplot as plt
# import the UMAP library
import umap
import seaborn as sns
import pickle
import sys


file_path = sys.argv[1]

if len(sys.argv) == 1:
    print(">>> No data file was given!")

# open a file, where you stored the pickled data
file = open(file_path, 'rb')

# load pickle dataset
data = pickle.load(file)


# create a UMAP reducer that will compress the data
reducer3D = umap.UMAP(n_components=3)

# get information about number of samples, rows, and columns in the dataset. 
# note the shape of each variable in the output above 
nsamples, nx, ny = data.shape

# reshape the data to comply with UMAP reducer requirements
data2 = data.reshape((nsamples,nx*ny))

# create an embedding using the fit_transform function. 
embedding3D = reducer3D.fit_transform(data2)

# display the embedding shape
print(np.shape(embedding3D))


fig = plt.figure(figsize=[25,10])
ax = plt.axes(projection ="3d")
p = ax.scatter(
    embedding3D[:, 0],
    embedding3D[:, 1],
    embedding3D[:, 2],
)
plt.title('UMAP projection of the MNIST dataset', fontsize=24)
plt.show()