import glob
import wave
import contextlib
import numpy as np
import matplotlib.pyplot as plt
import librosa as lr
import statistics


files = glob.glob('./laughter/*.wav')


def plot_mel_spectogram(spectogram):

    '''
    Plot spectogram data and display it
    '''

    plt.figure(figsize=(10, 4))
    lr.display.specshow(lr.power_to_db(spectogram, ref=np.max), y_axis='mel', fmax=8000, x_axis='time')
    plt.colorbar(format='%+2.0f dB')
    plt.title('Mel spectrogram')
    plt.tight_layout()
    plt.show()


def plot_audio(audio):

    '''
    Plot an audio file in wave form and display it 
    '''

    y, sr = lr.load('./{}.wav'.format(audio))
    time = np.arange(0,len(y))/sr
    fig, ax = plt.subplots()
    ax.plot(time,y)
    ax.set(xlabel='Time(s)',ylabel='sound amplitude')
    plt.show()


def get_wave_length(files):

    '''
    Retrive an array of the lengths of all wave files in a specified folder
    '''

    length_array = []

    for file in files:
        with contextlib.closing(wave.open(file,'r')) as file_open:

            print(file)
            frames = file_open.getnframes()
            rate = file_open.getframerate()
            duration = frames / float(rate)
            length_array.append(duration)

    return length_array


def show_length_boxplot():

    '''
    Boxplot an array of the lengths of all wave files in a specified folder and display it
    '''
    
    fig = plt.figure(figsize =(10, 7))
    ax = fig.add_subplot(111)
    
    data = get_wave_length(files)

    # Creating axes instance
    bp = ax.boxplot(data, vert = 0)

    print(statistics.median(data))

    plt.title('Total Amount: ' + str(len(data)))

    plt.show()


if __name__ == '__main__':
    show_length_boxplot()

