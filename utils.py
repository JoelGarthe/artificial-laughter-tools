import tensorflow as tf

# Print iterations progress
def print_progress_bar (iteration, total, prefix = '', suffix = '', decimals = 1, length = 100, fill = '█', printEnd = "\r"):
    """
    Call in a loop to create terminal progress bar
    @params:
        iteration   - Required  : current iteration (Int)
        total       - Required  : total iterations (Int)
        prefix      - Optional  : prefix string (Str)
        suffix      - Optional  : suffix string (Str)
        decimals    - Optional  : positive number of decimals in percent complete (Int)
        length      - Optional  : character length of bar (Int)
        fill        - Optional  : bar fill character (Str)
        printEnd    - Optional  : end character (e.g. "\r", "\r\n") (Str)
    """
    percent = ("{0:." + str(decimals) + "f}").format(100 * (iteration / float(total)))
    filledLength = int(length * iteration // total)
    bar = fill * filledLength + '-' * (length - filledLength)
    print(f'\r{prefix} |{bar}| {percent}% {suffix}', end = printEnd)
    # Print New Line on Complete
    if iteration == total: 
        print('\r{prefix} |{bar}| {percent}% {suffix}', end = printEnd)


def get_confusion_matrix(test_data, pred_data, threshold=0.5):
    
    m = tf.keras.metrics.TruePositives(thresholds=threshold)
    m.update_state(test_data, pred_data)
    true_positives = m.result().numpy()

    m = tf.keras.metrics.TrueNegatives(thresholds=threshold)
    m.update_state(test_data, pred_data)
    true_negatives = m.result().numpy()

    m = tf.keras.metrics.FalsePositives(thresholds=threshold)
    m.update_state(test_data, pred_data)
    false_positives = m.result().numpy()

    m = tf.keras.metrics.FalseNegatives(thresholds=threshold)
    m.update_state(test_data, pred_data)
    false_negatives = m.result().numpy()

    return [[true_positives, false_positives], [true_negatives, false_negatives]]

