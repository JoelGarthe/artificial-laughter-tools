import glob
import sys
import spec_converter
import numpy as np 
import utils
import csv
import matplotlib.pyplot as plt
import numpy as np
from tensorflow import keras





path_wave_files = sys.argv[1]
path_ai = sys.argv[2]

# Retrieve all wave files in a folder
files = glob.glob(path_wave_files + '/*.wav')

# Load a pretrained model
# './saved_models/91.5-06-27-2022-12-46-24'
model = keras.models.load_model(path_ai)

# print summary
model.summary()

result_list = []
value_list = []

counter = 0

for file in files:

    # Load audio and convert it into a spectogram
    spectogram = spec_converter.create_mel_spectogram(file)

    #print(spectogram.shape)

    spectogram = np.delete(spectogram, slice(173,216),1)

    #print(spectogram.shape)

    spectogram = np.expand_dims(spectogram, axis=0)


    # Predict if it is a laughter
    result = model.predict(spectogram)


    # Save value for logging
    result_list.append([file, result[0][0]])
    value_list.append(result[0][0])

    utils.print_progress_bar(counter, len(files), prefix="Analysis progress:", suffix="Complete", length=50)

    counter +=1

    
    # Throw into ai

    # Log result in list 


def write_csv(list):
    
    with open('LaughProbabilities.csv', 'w') as f:
      
        # using csv.writer method from CSV package
        write = csv.writer(f)
        
        write.writerow(["File", "Laughter_Prob"])
        write.writerows(list)


plt.hist(value_list, bins=30)  # density=False would make counts
plt.ylabel('Amount')
plt.xlabel('Laughter Probability')
plt.show()

write_csv(result_list)

# Plot the result





