import librosa as lr
import pickle
import sys
import utils
import os
import soundfile as sf
from os.path import exists

# Possible pitch shifts
shifts = [-6, -3, 3, 6]

def blow_up_data(path):

    #Load labels
    with open(path + "/complete_labels.pkl", 'rb') as f:
        data = pickle.load(f)

    new_labeldata = []

    new_folder = path + '/../shift_padded_data/'

    if not os.path.exists(new_folder):
        os.makedirs(new_folder)


    label_counter = 0
    
    for file in data:

        audio, sr = lr.load('{}'.format(path + "/" + file[0]))

        
        if not exists(new_folder + file[0]):
            sf.write(new_folder + file[0], audio, sr, 'PCM_24')

        new_labeldata.append(file)

        label_counter += 1


        for shift_amount in shifts:

            label_counter += 1

            file_name = file[0].replace(".wav", "") + str(shift_amount) + ".wav"

            shift_path = new_folder + file_name

            shifted_audio = lr.effects.pitch_shift(audio, sr=sr, n_steps=3)
            
            #lr.output.write_wav(shift_path, audio, sr, norm=False)

            if not exists(shift_path):
                sf.write(shift_path, audio, sr, 'PCM_24')

            new_labeldata.append([file_name, file[1]])

        utils.print_progress_bar(label_counter, len(data) * (len(shifts)+1))

    
    filehandler = open(new_folder + 'complete_labels.pkl' ,"wb")
    pickle.dump(new_labeldata ,filehandler)
    filehandler.close()






if __name__ == '__main__':
    path = sys.argv[1]

    if len(sys.argv) == 1:
        print(">>> No path was given!")


    if os.path.exists(path):

        print(">>> Starting pitch shifting process for files in folder " + path)

        if os.path.exists(path + "/complete_labels.pkl"):
            blow_up_data(path)

        else:
            print(">>> No 'complete_labels.pkl' file was found! Execution aborted.")

