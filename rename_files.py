import os
import sys
import glob 
import hashlib
import utils
import codecs
import base64

#TODO Rename table

def rename_files(folder):

    # Get all files in the folder
    files = glob.glob(folder + '/*.wav')

    counter = 0


    for file in files:

        utils.print_progress_bar(counter, len(files), prefix=">>> Renaming progress:", suffix="Complete", length=50)

        #print(str(hash_file(file)))


        # Renaming the file
        os.rename(file, os.path.dirname(file) + "/" + str(hash(file)) + ".wav")

        counter += 1

    print("")



def hash_file(filename):
   """"
   This function returns the SHA-1 hash
   of the file passed into it
   """

   # make a hash object
   h = hashlib.md5()

   # open file for reading in binary mode
   with open(filename,'rb') as file:

       # loop till the end of the file
       chunk = 0
       while chunk != b'':

           # read only 1024 bytes at a time
           chunk = file.read(1024)
           h.update(chunk)

   # return the hex representation of digest
   return h.hexdigest()



path = sys.argv[1]

if len(sys.argv) == 1:
    print(">>> No path was given!")


if os.path.exists(path):

    print(">>> Renaming files in folder " + path)

    rename_files(path)