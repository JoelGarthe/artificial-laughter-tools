from asyncio.windows_events import NULL
import glob
from os.path import exists
import pickle
import os 
import sys
import simpleaudio as sa



commands = ["y", "n", "x"]

def start_labeling_process(folder):

    # Get all files in the folder
    files = glob.glob(folder + '/*.wav')

    data_file = folder + '/incomplete_labels.pkl'

    # Data array to save later
    label_data = []

    # Load the csv/labeled data if there is an uncomplete one 
    if exists(data_file):
        file_opener = open(data_file,'rb')
        label_data = pickle.load(file_opener)
        file_opener.close()

        print(">>> Incomplete data was loaded.")

    else:

        # Add all files to the list to be labeled
        for file in files:
            label_data.append([os.path.basename(file), -1])

        print(">>> File data was collected and added to the list to label.")


    print(">>> Loaded data: " + str(len(label_data)) + " files.")


    
    label_counter = 0

    for label in label_data:

        label_counter += 1
        
        # 0 Corresponds to NULL after a dump apparently? 
        if label[1] == -1:

            print('>>> Sound Nr. "' + str(label_counter) )

            print('>>> Playing "' + label[0] + '" ...')


            print(folder + "/" + label[0])

            

            filename = folder + "/" + label[0]

            # Play the audio
            wave_obj = sa.WaveObject.from_wave_file(filename)
            play_obj = wave_obj.play()
            play_obj.wait_done()  # Wait until sound has finished playing
            
  
            # Wait for valid user input
            user_input = ""
            while not user_input in commands:
                user_input = input(">>> User Input pls: \n")


            # Handle user input
            # TODO If possible add a chance to redo a label or relisten to a file
            if user_input == "y":
                label[1] = 1
            elif user_input == "n" or user_input == "x":
                label[1] = 0

            

            # Save incomplete data after each lableing 
            filehandler = open(data_file ,"wb")
            print(">>> Saving incomplete data...")
            pickle.dump(label_data ,filehandler)
            filehandler.close()



        # If all data has been labeled save it under a different name
        if label_counter == len(label_data):

            print(">>> Completly labeled the data. Saving...")
            
            filehandler = open(folder + '/complete_labels.pkl' ,"wb")
            pickle.dump(label_data ,filehandler)
            filehandler.close()

            # Remove "incomplete" file
            os.remove(data_file)


            print('>>> Data was saved in "complete_labels.pkl".')

    pass


path = sys.argv[1]

if len(sys.argv) == 1:
    print(">>> No path was given!")


if os.path.exists(path):

    print(">>> Starting labeling process for files in folder " + path)

    start_labeling_process(path)