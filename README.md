# README Laughter Project

Herzlich Wilkommen zur README des Artificial Laughter Projekts. Im Rahmen dieses Projekts sind diverse Python Skripte und Tools entstanden welche zum manuellen und KI-gestützten Labeling von Daten und dem Trainieren von Neuronalen Netzen genutzt werden können. 

**Inhalt**
 1. [Labeling Tool](#labeling-tool)
 2. [Analyse Tools](#analyse-tools)
 4. [Training der Neuronalen Netze](#training-der-neuronalen-netze)


## Labeling Tool

Um mit dem Daten Labeling zu beginnen ist wird zunächst ein Ordner voller Wave Files benötigt welche gelabelt werden sollen.
Anschließend sollten die folgenden Skripte in der hier aufgeführten Reihenfolge ausgeführt werden um einen nutzbaren Trainingsdatensatz zu erzeugen.

 1. File Renaming
 2. Data Processing
 3. Labeling
 4. (Optional) Pitch Shifting
 5. Konvertieren zu einem Trainingsdatensatz

  

*Hinweis: 
Zum aktuellen Zeitpunkt ist mp3 noch nicht supported.*

  

### Installation

Um dieses Tool zu nutzen ist eine Python Installation vorrausgesetzt. Getestet wurde das Tool mit Python 3.8, es sollte aber auch mit anderen Versionen kompatibel sein.

  

Zunächst müssen die Dependencies mithilfe von pip oder einem vergleichbaren Paket-Manager installiert werden. Hierzu ist die 'requiremments.txt' im Hauptverzeichnis des Projekts zu finden.

Im Falle von pip sieht der entsprechende Befehl wie folgt aus:

```
pip install -r ./requirements.txt

```

  
  

### File Renaming

Um die Nutzung von Sonderzeichen in den teilweise automatisch erzeugten Dateinamen vorzubeugen müssen die Wave-Files zunächst umbenannt werden. Als Dateinamen wird fortan der SHA1-Hash der Datei genutzt. Umgesetzt wird diese Umbenennung mithilfe des folgenden Skripts:

````
python rename_files.py ./folder

````

***'./folder' muss bei dem Aufruf gegen den entsprechenden Ordner ausgetauscht werden!***

  

### Data Processing

Um Audio Daten bestimmter Längen herauszufiltern und alle Audiodaten fürs das Training auf die gleiche Länge zu schneiden bzw zu padden muss zusätzlich Data Processing ausgeführt werden:

````
python data_processing.py ./folder

````

  

Ausgegeben werden die Dateien dann im "prepared_data" Ordner.

  

***'./folder' muss bei dem Aufruf gegen den entsprechenden Ordner mit den renameden Files ausgetauscht werden!***

  

Die aufbereiteten Daten werden im Ordner "prepared_data" zu finden sein. In den folgenden Schritten sollte also darauf geachtet werden dass die verarbeiteten Daten verwendet werden, nicht die Rohdaten. Also fortan den "prepared_data" bei den Skript Aufrufen angeben!

  

### Labeling

Nachdem die Wave-Daten nun umbennant und zurechtgeschnitten wurden kann das Labeling mit folgendem Aufruf gestartet werden:

````
python click_labeling.py ./folder

````

  

Als Input wird "y" erwartet wenn es sich um ein Lachen handelt, "n" wenn dies nicht der Fall ist.

  

***'./folder' muss bei dem Aufruf gegen den entsprechenden Ordner (idR "prepared_data") ausgetauscht werden!***

  

Hierbei ist wichtig zu erwähnen dass der Lebeling Vorgang auch mittendrin abgebrochen werden kann, da der Fortschritt nach jedem Labeling gespeichert wird. Wird das Skript beim nächsten Mal gestartet wird der vorherige Fortschritt aus der 'incomplete_labels.pkl' Datei geladen und der Vorgang fortgesetzt.

  

Sind alle Daten gelabelt folgt der nächste Schritt:

  

### (Optional) Pitch Shifting

Je nach Belieben kann ein vorhandener Datensatz aus .wav Daten mit pitch gesshifteden daten aus dem selben Datensatz ergänzt werden.

````
python pitch_shifter.py ./folder

````

  

***'./folder' muss bei dem Aufruf gegen den Ordner mit den zu ergänzenden .wav Daten ergänzt werden (idR "prepared_data")***

  

Der aufgestockte Datensatz kann nach dem Erzeugen im Ordner "shift_padded_data" gefunden werden. Dieser sollte dann auch beim letzten Schritt der Pipeline als Ordner angegeben werden.

  

### Konvertieren zu einem Trainingsdatensatz

Um die Daten nun abschließend in einen fertigen Traingsdatensatz zu konvertieren wird folgender Aufruf genutzt:

````

python spec_converter.py ./folder

````

  

Ausgegeben wird eine "my_dataset.pkl" welche die gelabelten Mel-Spektogramme in einem Array enthält.

  

***'./folder' muss bei dem Aufruf gegen den entsprechenden Ordner ausgetauscht werden!***


## Analyse Tools

### Einschätzung von Wave-Daten 

Das Analyse Tool nimmt einen Ordner voller Audiodaten entgegen und weist diesen einen Wert zwischen 0 und 1 zu welcher angibt wie wahrscheinlich der Audio-Clip ein Lachen enthält. 

Um die Daten auf entpsrechend vorzubereiten sollten bei dem entsprechenden Ordner zuerst 'rename_files.py' und 'data_processing.py' ausgeführt werden. Für eine genauere Anleitung zur Nutzung dieser Skripte siehe [oben](#labeling-tool). 

````
python analyse.py ./folder ./ai_folder

````

***'./folder' und './ai_folder'  müssen bei dem Aufruf gegen die entsprechenden Ordner ausgetauscht werden!***


Output ist ein Diagramm der Werteverteilung und eine CVS Datei im Hauptverzeichnis. 


### UMAP

Anwendung des UMAP Algorithmus zur 2 oder 3 dimensionalen Kategorisierung von Audiodaten. 

````
python umap_example.py ./folder/data.pkl

````

````
python umap_example_3D.py ./folder/data.pkl

````

***'./folder/data.pkl'  muss bei dem Aufruf gegen den Pfad zum entsprechenden Datensatz ausgetauscht werden***


### TSNE
Anwendung des TSNE Algorithmus zur 2 dimensionalen Kategorisierung von Audiodaten. 

````
python tsne_example.py ./folder/data.pkl

````







## Training der Neuronalen Netze

````
python ai_cnn.py ./complete_datasets/my_dataset_shifted.pkl

````


````
python ai_transfer.py ./complete_datasets/my_dataset_shifted.pkl

````

Das mitgegebene Argument gibt den Pfad zum Datensatz an welcher zum Training der KI genutzt wird.

Während dem Training wird immer das am Besten performende Model abgespeichert und mit einem entsprechenden Timestamp in dem Ordner *'saved_models'* gespeichert. 
Die Installation von NVIDIAs CUDA ist nicht vorrausgesetzt, aber von großem zeitlichen Vorteil. 
