��
��
^
AssignVariableOp
resource
value"dtype"
dtypetype"
validate_shapebool( �
~
BiasAdd

value"T	
bias"T
output"T" 
Ttype:
2	"-
data_formatstringNHWC:
NHWCNCHW
8
Const
output"dtype"
valuetensor"
dtypetype
�
Conv2D

input"T
filter"T
output"T"
Ttype:	
2"
strides	list(int)"
use_cudnn_on_gpubool(",
paddingstring:
SAMEVALIDEXPLICIT""
explicit_paddings	list(int)
 "-
data_formatstringNHWC:
NHWCNCHW" 
	dilations	list(int)

�
FusedBatchNormV3
x"T

scale"U
offset"U	
mean"U
variance"U
y"T

batch_mean"U
batch_variance"U
reserve_space_1"U
reserve_space_2"U
reserve_space_3"U"
Ttype:
2"
Utype:
2"
epsilonfloat%��8"&
exponential_avg_factorfloat%  �?";
data_formatstringNHWC:
NHWCNCHWNDHWCNCDHW"
is_trainingbool(
.
Identity

input"T
output"T"	
Ttype
q
MatMul
a"T
b"T
product"T"
transpose_abool( "
transpose_bbool( "
Ttype:

2	
�
Max

input"T
reduction_indices"Tidx
output"T"
	keep_dimsbool( " 
Ttype:
2	"
Tidxtype0:
2	
�
MaxPool

input"T
output"T"
Ttype0:
2	"
ksize	list(int)(0"
strides	list(int)(0",
paddingstring:
SAMEVALIDEXPLICIT""
explicit_paddings	list(int)
 ":
data_formatstringNHWC:
NHWCNCHWNCHW_VECT_C
e
MergeV2Checkpoints
checkpoint_prefixes
destination_prefix"
delete_old_dirsbool(�

NoOp
M
Pack
values"T*N
output"T"
Nint(0"	
Ttype"
axisint 
C
Placeholder
output"dtype"
dtypetype"
shapeshape:
@
ReadVariableOp
resource
value"dtype"
dtypetype�
E
Relu
features"T
activations"T"
Ttype:
2	
o
	RestoreV2

prefix
tensor_names
shape_and_slices
tensors2dtypes"
dtypes
list(type)(0�
l
SaveV2

prefix
tensor_names
shape_and_slices
tensors2dtypes"
dtypes
list(type)(0�
?
Select
	condition

t"T
e"T
output"T"	
Ttype
H
ShardedFilename
basename	
shard

num_shards
filename
0
Sigmoid
x"T
y"T"
Ttype:

2
�
StatefulPartitionedCall
args2Tin
output2Tout"
Tin
list(type)("
Tout
list(type)("	
ffunc"
configstring "
config_protostring "
executor_typestring ��
@
StaticRegexFullMatch	
input

output
"
patternstring
N

StringJoin
inputs*N

output"
Nint(0"
	separatorstring 
�
VarHandleOp
resource"
	containerstring "
shared_namestring "
dtypetype"
shapeshape"#
allowed_deviceslist(string)
 �"serve*2.8.02v2.8.0-rc1-32-g3f878cff5b68��
�
Block0_Conv/kernelVarHandleOp*
_output_shapes
: *
dtype0*
shape:*#
shared_nameBlock0_Conv/kernel
�
&Block0_Conv/kernel/Read/ReadVariableOpReadVariableOpBlock0_Conv/kernel*&
_output_shapes
:*
dtype0
x
Block0_Conv/biasVarHandleOp*
_output_shapes
: *
dtype0*
shape:*!
shared_nameBlock0_Conv/bias
q
$Block0_Conv/bias/Read/ReadVariableOpReadVariableOpBlock0_Conv/bias*
_output_shapes
:*
dtype0
z
Block0_Norm/gammaVarHandleOp*
_output_shapes
: *
dtype0*
shape:*"
shared_nameBlock0_Norm/gamma
s
%Block0_Norm/gamma/Read/ReadVariableOpReadVariableOpBlock0_Norm/gamma*
_output_shapes
:*
dtype0
x
Block0_Norm/betaVarHandleOp*
_output_shapes
: *
dtype0*
shape:*!
shared_nameBlock0_Norm/beta
q
$Block0_Norm/beta/Read/ReadVariableOpReadVariableOpBlock0_Norm/beta*
_output_shapes
:*
dtype0
�
Block0_Norm/moving_meanVarHandleOp*
_output_shapes
: *
dtype0*
shape:*(
shared_nameBlock0_Norm/moving_mean

+Block0_Norm/moving_mean/Read/ReadVariableOpReadVariableOpBlock0_Norm/moving_mean*
_output_shapes
:*
dtype0
�
Block0_Norm/moving_varianceVarHandleOp*
_output_shapes
: *
dtype0*
shape:*,
shared_nameBlock0_Norm/moving_variance
�
/Block0_Norm/moving_variance/Read/ReadVariableOpReadVariableOpBlock0_Norm/moving_variance*
_output_shapes
:*
dtype0
�
Block1_Conv/kernelVarHandleOp*
_output_shapes
: *
dtype0*
shape: *#
shared_nameBlock1_Conv/kernel
�
&Block1_Conv/kernel/Read/ReadVariableOpReadVariableOpBlock1_Conv/kernel*&
_output_shapes
: *
dtype0
x
Block1_Conv/biasVarHandleOp*
_output_shapes
: *
dtype0*
shape: *!
shared_nameBlock1_Conv/bias
q
$Block1_Conv/bias/Read/ReadVariableOpReadVariableOpBlock1_Conv/bias*
_output_shapes
: *
dtype0
z
Block1_Norm/gammaVarHandleOp*
_output_shapes
: *
dtype0*
shape: *"
shared_nameBlock1_Norm/gamma
s
%Block1_Norm/gamma/Read/ReadVariableOpReadVariableOpBlock1_Norm/gamma*
_output_shapes
: *
dtype0
x
Block1_Norm/betaVarHandleOp*
_output_shapes
: *
dtype0*
shape: *!
shared_nameBlock1_Norm/beta
q
$Block1_Norm/beta/Read/ReadVariableOpReadVariableOpBlock1_Norm/beta*
_output_shapes
: *
dtype0
�
Block1_Norm/moving_meanVarHandleOp*
_output_shapes
: *
dtype0*
shape: *(
shared_nameBlock1_Norm/moving_mean

+Block1_Norm/moving_mean/Read/ReadVariableOpReadVariableOpBlock1_Norm/moving_mean*
_output_shapes
: *
dtype0
�
Block1_Norm/moving_varianceVarHandleOp*
_output_shapes
: *
dtype0*
shape: *,
shared_nameBlock1_Norm/moving_variance
�
/Block1_Norm/moving_variance/Read/ReadVariableOpReadVariableOpBlock1_Norm/moving_variance*
_output_shapes
: *
dtype0
�
Block2_Conv/kernelVarHandleOp*
_output_shapes
: *
dtype0*
shape: @*#
shared_nameBlock2_Conv/kernel
�
&Block2_Conv/kernel/Read/ReadVariableOpReadVariableOpBlock2_Conv/kernel*&
_output_shapes
: @*
dtype0
x
Block2_Conv/biasVarHandleOp*
_output_shapes
: *
dtype0*
shape:@*!
shared_nameBlock2_Conv/bias
q
$Block2_Conv/bias/Read/ReadVariableOpReadVariableOpBlock2_Conv/bias*
_output_shapes
:@*
dtype0
z
Block2_Norm/gammaVarHandleOp*
_output_shapes
: *
dtype0*
shape:@*"
shared_nameBlock2_Norm/gamma
s
%Block2_Norm/gamma/Read/ReadVariableOpReadVariableOpBlock2_Norm/gamma*
_output_shapes
:@*
dtype0
x
Block2_Norm/betaVarHandleOp*
_output_shapes
: *
dtype0*
shape:@*!
shared_nameBlock2_Norm/beta
q
$Block2_Norm/beta/Read/ReadVariableOpReadVariableOpBlock2_Norm/beta*
_output_shapes
:@*
dtype0
�
Block2_Norm/moving_meanVarHandleOp*
_output_shapes
: *
dtype0*
shape:@*(
shared_nameBlock2_Norm/moving_mean

+Block2_Norm/moving_mean/Read/ReadVariableOpReadVariableOpBlock2_Norm/moving_mean*
_output_shapes
:@*
dtype0
�
Block2_Norm/moving_varianceVarHandleOp*
_output_shapes
: *
dtype0*
shape:@*,
shared_nameBlock2_Norm/moving_variance
�
/Block2_Norm/moving_variance/Read/ReadVariableOpReadVariableOpBlock2_Norm/moving_variance*
_output_shapes
:@*
dtype0
�
Block3_Conv/kernelVarHandleOp*
_output_shapes
: *
dtype0*
shape:@�*#
shared_nameBlock3_Conv/kernel
�
&Block3_Conv/kernel/Read/ReadVariableOpReadVariableOpBlock3_Conv/kernel*'
_output_shapes
:@�*
dtype0
y
Block3_Conv/biasVarHandleOp*
_output_shapes
: *
dtype0*
shape:�*!
shared_nameBlock3_Conv/bias
r
$Block3_Conv/bias/Read/ReadVariableOpReadVariableOpBlock3_Conv/bias*
_output_shapes	
:�*
dtype0
{
Block3_Norm/gammaVarHandleOp*
_output_shapes
: *
dtype0*
shape:�*"
shared_nameBlock3_Norm/gamma
t
%Block3_Norm/gamma/Read/ReadVariableOpReadVariableOpBlock3_Norm/gamma*
_output_shapes	
:�*
dtype0
y
Block3_Norm/betaVarHandleOp*
_output_shapes
: *
dtype0*
shape:�*!
shared_nameBlock3_Norm/beta
r
$Block3_Norm/beta/Read/ReadVariableOpReadVariableOpBlock3_Norm/beta*
_output_shapes	
:�*
dtype0
�
Block3_Norm/moving_meanVarHandleOp*
_output_shapes
: *
dtype0*
shape:�*(
shared_nameBlock3_Norm/moving_mean
�
+Block3_Norm/moving_mean/Read/ReadVariableOpReadVariableOpBlock3_Norm/moving_mean*
_output_shapes	
:�*
dtype0
�
Block3_Norm/moving_varianceVarHandleOp*
_output_shapes
: *
dtype0*
shape:�*,
shared_nameBlock3_Norm/moving_variance
�
/Block3_Norm/moving_variance/Read/ReadVariableOpReadVariableOpBlock3_Norm/moving_variance*
_output_shapes	
:�*
dtype0
�
Final_Dense/kernelVarHandleOp*
_output_shapes
: *
dtype0*
shape:
��*#
shared_nameFinal_Dense/kernel
{
&Final_Dense/kernel/Read/ReadVariableOpReadVariableOpFinal_Dense/kernel* 
_output_shapes
:
��*
dtype0
y
Final_Dense/biasVarHandleOp*
_output_shapes
: *
dtype0*
shape:�*!
shared_nameFinal_Dense/bias
r
$Final_Dense/bias/Read/ReadVariableOpReadVariableOpFinal_Dense/bias*
_output_shapes	
:�*
dtype0
w
output/kernelVarHandleOp*
_output_shapes
: *
dtype0*
shape:	�*
shared_nameoutput/kernel
p
!output/kernel/Read/ReadVariableOpReadVariableOpoutput/kernel*
_output_shapes
:	�*
dtype0
n
output/biasVarHandleOp*
_output_shapes
: *
dtype0*
shape:*
shared_nameoutput/bias
g
output/bias/Read/ReadVariableOpReadVariableOpoutput/bias*
_output_shapes
:*
dtype0
f
	Adam/iterVarHandleOp*
_output_shapes
: *
dtype0	*
shape: *
shared_name	Adam/iter
_
Adam/iter/Read/ReadVariableOpReadVariableOp	Adam/iter*
_output_shapes
: *
dtype0	
j
Adam/beta_1VarHandleOp*
_output_shapes
: *
dtype0*
shape: *
shared_nameAdam/beta_1
c
Adam/beta_1/Read/ReadVariableOpReadVariableOpAdam/beta_1*
_output_shapes
: *
dtype0
j
Adam/beta_2VarHandleOp*
_output_shapes
: *
dtype0*
shape: *
shared_nameAdam/beta_2
c
Adam/beta_2/Read/ReadVariableOpReadVariableOpAdam/beta_2*
_output_shapes
: *
dtype0
h

Adam/decayVarHandleOp*
_output_shapes
: *
dtype0*
shape: *
shared_name
Adam/decay
a
Adam/decay/Read/ReadVariableOpReadVariableOp
Adam/decay*
_output_shapes
: *
dtype0
x
Adam/learning_rateVarHandleOp*
_output_shapes
: *
dtype0*
shape: *#
shared_nameAdam/learning_rate
q
&Adam/learning_rate/Read/ReadVariableOpReadVariableOpAdam/learning_rate*
_output_shapes
: *
dtype0
^
totalVarHandleOp*
_output_shapes
: *
dtype0*
shape: *
shared_nametotal
W
total/Read/ReadVariableOpReadVariableOptotal*
_output_shapes
: *
dtype0
^
countVarHandleOp*
_output_shapes
: *
dtype0*
shape: *
shared_namecount
W
count/Read/ReadVariableOpReadVariableOpcount*
_output_shapes
: *
dtype0
b
total_1VarHandleOp*
_output_shapes
: *
dtype0*
shape: *
shared_name	total_1
[
total_1/Read/ReadVariableOpReadVariableOptotal_1*
_output_shapes
: *
dtype0
b
count_1VarHandleOp*
_output_shapes
: *
dtype0*
shape: *
shared_name	count_1
[
count_1/Read/ReadVariableOpReadVariableOpcount_1*
_output_shapes
: *
dtype0
�
Adam/Block0_Conv/kernel/mVarHandleOp*
_output_shapes
: *
dtype0*
shape:**
shared_nameAdam/Block0_Conv/kernel/m
�
-Adam/Block0_Conv/kernel/m/Read/ReadVariableOpReadVariableOpAdam/Block0_Conv/kernel/m*&
_output_shapes
:*
dtype0
�
Adam/Block0_Conv/bias/mVarHandleOp*
_output_shapes
: *
dtype0*
shape:*(
shared_nameAdam/Block0_Conv/bias/m

+Adam/Block0_Conv/bias/m/Read/ReadVariableOpReadVariableOpAdam/Block0_Conv/bias/m*
_output_shapes
:*
dtype0
�
Adam/Block0_Norm/gamma/mVarHandleOp*
_output_shapes
: *
dtype0*
shape:*)
shared_nameAdam/Block0_Norm/gamma/m
�
,Adam/Block0_Norm/gamma/m/Read/ReadVariableOpReadVariableOpAdam/Block0_Norm/gamma/m*
_output_shapes
:*
dtype0
�
Adam/Block0_Norm/beta/mVarHandleOp*
_output_shapes
: *
dtype0*
shape:*(
shared_nameAdam/Block0_Norm/beta/m

+Adam/Block0_Norm/beta/m/Read/ReadVariableOpReadVariableOpAdam/Block0_Norm/beta/m*
_output_shapes
:*
dtype0
�
Adam/Block1_Conv/kernel/mVarHandleOp*
_output_shapes
: *
dtype0*
shape: **
shared_nameAdam/Block1_Conv/kernel/m
�
-Adam/Block1_Conv/kernel/m/Read/ReadVariableOpReadVariableOpAdam/Block1_Conv/kernel/m*&
_output_shapes
: *
dtype0
�
Adam/Block1_Conv/bias/mVarHandleOp*
_output_shapes
: *
dtype0*
shape: *(
shared_nameAdam/Block1_Conv/bias/m

+Adam/Block1_Conv/bias/m/Read/ReadVariableOpReadVariableOpAdam/Block1_Conv/bias/m*
_output_shapes
: *
dtype0
�
Adam/Block1_Norm/gamma/mVarHandleOp*
_output_shapes
: *
dtype0*
shape: *)
shared_nameAdam/Block1_Norm/gamma/m
�
,Adam/Block1_Norm/gamma/m/Read/ReadVariableOpReadVariableOpAdam/Block1_Norm/gamma/m*
_output_shapes
: *
dtype0
�
Adam/Block1_Norm/beta/mVarHandleOp*
_output_shapes
: *
dtype0*
shape: *(
shared_nameAdam/Block1_Norm/beta/m

+Adam/Block1_Norm/beta/m/Read/ReadVariableOpReadVariableOpAdam/Block1_Norm/beta/m*
_output_shapes
: *
dtype0
�
Adam/Block2_Conv/kernel/mVarHandleOp*
_output_shapes
: *
dtype0*
shape: @**
shared_nameAdam/Block2_Conv/kernel/m
�
-Adam/Block2_Conv/kernel/m/Read/ReadVariableOpReadVariableOpAdam/Block2_Conv/kernel/m*&
_output_shapes
: @*
dtype0
�
Adam/Block2_Conv/bias/mVarHandleOp*
_output_shapes
: *
dtype0*
shape:@*(
shared_nameAdam/Block2_Conv/bias/m

+Adam/Block2_Conv/bias/m/Read/ReadVariableOpReadVariableOpAdam/Block2_Conv/bias/m*
_output_shapes
:@*
dtype0
�
Adam/Block2_Norm/gamma/mVarHandleOp*
_output_shapes
: *
dtype0*
shape:@*)
shared_nameAdam/Block2_Norm/gamma/m
�
,Adam/Block2_Norm/gamma/m/Read/ReadVariableOpReadVariableOpAdam/Block2_Norm/gamma/m*
_output_shapes
:@*
dtype0
�
Adam/Block2_Norm/beta/mVarHandleOp*
_output_shapes
: *
dtype0*
shape:@*(
shared_nameAdam/Block2_Norm/beta/m

+Adam/Block2_Norm/beta/m/Read/ReadVariableOpReadVariableOpAdam/Block2_Norm/beta/m*
_output_shapes
:@*
dtype0
�
Adam/Block3_Conv/kernel/mVarHandleOp*
_output_shapes
: *
dtype0*
shape:@�**
shared_nameAdam/Block3_Conv/kernel/m
�
-Adam/Block3_Conv/kernel/m/Read/ReadVariableOpReadVariableOpAdam/Block3_Conv/kernel/m*'
_output_shapes
:@�*
dtype0
�
Adam/Block3_Conv/bias/mVarHandleOp*
_output_shapes
: *
dtype0*
shape:�*(
shared_nameAdam/Block3_Conv/bias/m
�
+Adam/Block3_Conv/bias/m/Read/ReadVariableOpReadVariableOpAdam/Block3_Conv/bias/m*
_output_shapes	
:�*
dtype0
�
Adam/Block3_Norm/gamma/mVarHandleOp*
_output_shapes
: *
dtype0*
shape:�*)
shared_nameAdam/Block3_Norm/gamma/m
�
,Adam/Block3_Norm/gamma/m/Read/ReadVariableOpReadVariableOpAdam/Block3_Norm/gamma/m*
_output_shapes	
:�*
dtype0
�
Adam/Block3_Norm/beta/mVarHandleOp*
_output_shapes
: *
dtype0*
shape:�*(
shared_nameAdam/Block3_Norm/beta/m
�
+Adam/Block3_Norm/beta/m/Read/ReadVariableOpReadVariableOpAdam/Block3_Norm/beta/m*
_output_shapes	
:�*
dtype0
�
Adam/Final_Dense/kernel/mVarHandleOp*
_output_shapes
: *
dtype0*
shape:
��**
shared_nameAdam/Final_Dense/kernel/m
�
-Adam/Final_Dense/kernel/m/Read/ReadVariableOpReadVariableOpAdam/Final_Dense/kernel/m* 
_output_shapes
:
��*
dtype0
�
Adam/Final_Dense/bias/mVarHandleOp*
_output_shapes
: *
dtype0*
shape:�*(
shared_nameAdam/Final_Dense/bias/m
�
+Adam/Final_Dense/bias/m/Read/ReadVariableOpReadVariableOpAdam/Final_Dense/bias/m*
_output_shapes	
:�*
dtype0
�
Adam/output/kernel/mVarHandleOp*
_output_shapes
: *
dtype0*
shape:	�*%
shared_nameAdam/output/kernel/m
~
(Adam/output/kernel/m/Read/ReadVariableOpReadVariableOpAdam/output/kernel/m*
_output_shapes
:	�*
dtype0
|
Adam/output/bias/mVarHandleOp*
_output_shapes
: *
dtype0*
shape:*#
shared_nameAdam/output/bias/m
u
&Adam/output/bias/m/Read/ReadVariableOpReadVariableOpAdam/output/bias/m*
_output_shapes
:*
dtype0
�
Adam/Block0_Conv/kernel/vVarHandleOp*
_output_shapes
: *
dtype0*
shape:**
shared_nameAdam/Block0_Conv/kernel/v
�
-Adam/Block0_Conv/kernel/v/Read/ReadVariableOpReadVariableOpAdam/Block0_Conv/kernel/v*&
_output_shapes
:*
dtype0
�
Adam/Block0_Conv/bias/vVarHandleOp*
_output_shapes
: *
dtype0*
shape:*(
shared_nameAdam/Block0_Conv/bias/v

+Adam/Block0_Conv/bias/v/Read/ReadVariableOpReadVariableOpAdam/Block0_Conv/bias/v*
_output_shapes
:*
dtype0
�
Adam/Block0_Norm/gamma/vVarHandleOp*
_output_shapes
: *
dtype0*
shape:*)
shared_nameAdam/Block0_Norm/gamma/v
�
,Adam/Block0_Norm/gamma/v/Read/ReadVariableOpReadVariableOpAdam/Block0_Norm/gamma/v*
_output_shapes
:*
dtype0
�
Adam/Block0_Norm/beta/vVarHandleOp*
_output_shapes
: *
dtype0*
shape:*(
shared_nameAdam/Block0_Norm/beta/v

+Adam/Block0_Norm/beta/v/Read/ReadVariableOpReadVariableOpAdam/Block0_Norm/beta/v*
_output_shapes
:*
dtype0
�
Adam/Block1_Conv/kernel/vVarHandleOp*
_output_shapes
: *
dtype0*
shape: **
shared_nameAdam/Block1_Conv/kernel/v
�
-Adam/Block1_Conv/kernel/v/Read/ReadVariableOpReadVariableOpAdam/Block1_Conv/kernel/v*&
_output_shapes
: *
dtype0
�
Adam/Block1_Conv/bias/vVarHandleOp*
_output_shapes
: *
dtype0*
shape: *(
shared_nameAdam/Block1_Conv/bias/v

+Adam/Block1_Conv/bias/v/Read/ReadVariableOpReadVariableOpAdam/Block1_Conv/bias/v*
_output_shapes
: *
dtype0
�
Adam/Block1_Norm/gamma/vVarHandleOp*
_output_shapes
: *
dtype0*
shape: *)
shared_nameAdam/Block1_Norm/gamma/v
�
,Adam/Block1_Norm/gamma/v/Read/ReadVariableOpReadVariableOpAdam/Block1_Norm/gamma/v*
_output_shapes
: *
dtype0
�
Adam/Block1_Norm/beta/vVarHandleOp*
_output_shapes
: *
dtype0*
shape: *(
shared_nameAdam/Block1_Norm/beta/v

+Adam/Block1_Norm/beta/v/Read/ReadVariableOpReadVariableOpAdam/Block1_Norm/beta/v*
_output_shapes
: *
dtype0
�
Adam/Block2_Conv/kernel/vVarHandleOp*
_output_shapes
: *
dtype0*
shape: @**
shared_nameAdam/Block2_Conv/kernel/v
�
-Adam/Block2_Conv/kernel/v/Read/ReadVariableOpReadVariableOpAdam/Block2_Conv/kernel/v*&
_output_shapes
: @*
dtype0
�
Adam/Block2_Conv/bias/vVarHandleOp*
_output_shapes
: *
dtype0*
shape:@*(
shared_nameAdam/Block2_Conv/bias/v

+Adam/Block2_Conv/bias/v/Read/ReadVariableOpReadVariableOpAdam/Block2_Conv/bias/v*
_output_shapes
:@*
dtype0
�
Adam/Block2_Norm/gamma/vVarHandleOp*
_output_shapes
: *
dtype0*
shape:@*)
shared_nameAdam/Block2_Norm/gamma/v
�
,Adam/Block2_Norm/gamma/v/Read/ReadVariableOpReadVariableOpAdam/Block2_Norm/gamma/v*
_output_shapes
:@*
dtype0
�
Adam/Block2_Norm/beta/vVarHandleOp*
_output_shapes
: *
dtype0*
shape:@*(
shared_nameAdam/Block2_Norm/beta/v

+Adam/Block2_Norm/beta/v/Read/ReadVariableOpReadVariableOpAdam/Block2_Norm/beta/v*
_output_shapes
:@*
dtype0
�
Adam/Block3_Conv/kernel/vVarHandleOp*
_output_shapes
: *
dtype0*
shape:@�**
shared_nameAdam/Block3_Conv/kernel/v
�
-Adam/Block3_Conv/kernel/v/Read/ReadVariableOpReadVariableOpAdam/Block3_Conv/kernel/v*'
_output_shapes
:@�*
dtype0
�
Adam/Block3_Conv/bias/vVarHandleOp*
_output_shapes
: *
dtype0*
shape:�*(
shared_nameAdam/Block3_Conv/bias/v
�
+Adam/Block3_Conv/bias/v/Read/ReadVariableOpReadVariableOpAdam/Block3_Conv/bias/v*
_output_shapes	
:�*
dtype0
�
Adam/Block3_Norm/gamma/vVarHandleOp*
_output_shapes
: *
dtype0*
shape:�*)
shared_nameAdam/Block3_Norm/gamma/v
�
,Adam/Block3_Norm/gamma/v/Read/ReadVariableOpReadVariableOpAdam/Block3_Norm/gamma/v*
_output_shapes	
:�*
dtype0
�
Adam/Block3_Norm/beta/vVarHandleOp*
_output_shapes
: *
dtype0*
shape:�*(
shared_nameAdam/Block3_Norm/beta/v
�
+Adam/Block3_Norm/beta/v/Read/ReadVariableOpReadVariableOpAdam/Block3_Norm/beta/v*
_output_shapes	
:�*
dtype0
�
Adam/Final_Dense/kernel/vVarHandleOp*
_output_shapes
: *
dtype0*
shape:
��**
shared_nameAdam/Final_Dense/kernel/v
�
-Adam/Final_Dense/kernel/v/Read/ReadVariableOpReadVariableOpAdam/Final_Dense/kernel/v* 
_output_shapes
:
��*
dtype0
�
Adam/Final_Dense/bias/vVarHandleOp*
_output_shapes
: *
dtype0*
shape:�*(
shared_nameAdam/Final_Dense/bias/v
�
+Adam/Final_Dense/bias/v/Read/ReadVariableOpReadVariableOpAdam/Final_Dense/bias/v*
_output_shapes	
:�*
dtype0
�
Adam/output/kernel/vVarHandleOp*
_output_shapes
: *
dtype0*
shape:	�*%
shared_nameAdam/output/kernel/v
~
(Adam/output/kernel/v/Read/ReadVariableOpReadVariableOpAdam/output/kernel/v*
_output_shapes
:	�*
dtype0
|
Adam/output/bias/vVarHandleOp*
_output_shapes
: *
dtype0*
shape:*#
shared_nameAdam/output/bias/v
u
&Adam/output/bias/v/Read/ReadVariableOpReadVariableOpAdam/output/bias/v*
_output_shapes
:*
dtype0

NoOpNoOp
�
ConstConst"/device:CPU:0*
_output_shapes
: *
dtype0*��
value��B�� B��
�
layer_with_weights-0
layer-0
layer-1
layer_with_weights-1
layer-2
layer_with_weights-2
layer-3
layer-4
layer_with_weights-3
layer-5
layer_with_weights-4
layer-6
layer-7
	layer_with_weights-5
	layer-8

layer_with_weights-6

layer-9
layer-10
layer_with_weights-7
layer-11
layer-12
layer_with_weights-8
layer-13
layer_with_weights-9
layer-14
	optimizer
	variables
trainable_variables
regularization_losses
	keras_api
__call__
*&call_and_return_all_conditional_losses
_default_save_signature

signatures*
�

kernel
bias
	variables
trainable_variables
regularization_losses
	keras_api
__call__
* &call_and_return_all_conditional_losses*
�
!	variables
"trainable_variables
#regularization_losses
$	keras_api
%__call__
*&&call_and_return_all_conditional_losses* 
�
'axis
	(gamma
)beta
*moving_mean
+moving_variance
,	variables
-trainable_variables
.regularization_losses
/	keras_api
0__call__
*1&call_and_return_all_conditional_losses*
�

2kernel
3bias
4	variables
5trainable_variables
6regularization_losses
7	keras_api
8__call__
*9&call_and_return_all_conditional_losses*
�
:	variables
;trainable_variables
<regularization_losses
=	keras_api
>__call__
*?&call_and_return_all_conditional_losses* 
�
@axis
	Agamma
Bbeta
Cmoving_mean
Dmoving_variance
E	variables
Ftrainable_variables
Gregularization_losses
H	keras_api
I__call__
*J&call_and_return_all_conditional_losses*
�

Kkernel
Lbias
M	variables
Ntrainable_variables
Oregularization_losses
P	keras_api
Q__call__
*R&call_and_return_all_conditional_losses*
�
S	variables
Ttrainable_variables
Uregularization_losses
V	keras_api
W__call__
*X&call_and_return_all_conditional_losses* 
�
Yaxis
	Zgamma
[beta
\moving_mean
]moving_variance
^	variables
_trainable_variables
`regularization_losses
a	keras_api
b__call__
*c&call_and_return_all_conditional_losses*
�

dkernel
ebias
f	variables
gtrainable_variables
hregularization_losses
i	keras_api
j__call__
*k&call_and_return_all_conditional_losses*
�
l	variables
mtrainable_variables
nregularization_losses
o	keras_api
p__call__
*q&call_and_return_all_conditional_losses* 
�
raxis
	sgamma
tbeta
umoving_mean
vmoving_variance
w	variables
xtrainable_variables
yregularization_losses
z	keras_api
{__call__
*|&call_and_return_all_conditional_losses*
�
}	variables
~trainable_variables
regularization_losses
�	keras_api
�__call__
+�&call_and_return_all_conditional_losses* 
�
�kernel
	�bias
�	variables
�trainable_variables
�regularization_losses
�	keras_api
�__call__
+�&call_and_return_all_conditional_losses*
�
�kernel
	�bias
�	variables
�trainable_variables
�regularization_losses
�	keras_api
�__call__
+�&call_and_return_all_conditional_losses*
�
	�iter
�beta_1
�beta_2

�decay
�learning_ratem�m�(m�)m�2m�3m�Am�Bm�Km�Lm�Zm�[m�dm�em�sm�tm�	�m�	�m�	�m�	�m�v�v�(v�)v�2v�3v�Av�Bv�Kv�Lv�Zv�[v�dv�ev�sv�tv�	�v�	�v�	�v�	�v�*
�
0
1
(2
)3
*4
+5
26
37
A8
B9
C10
D11
K12
L13
Z14
[15
\16
]17
d18
e19
s20
t21
u22
v23
�24
�25
�26
�27*
�
0
1
(2
)3
24
35
A6
B7
K8
L9
Z10
[11
d12
e13
s14
t15
�16
�17
�18
�19*
* 
�
�non_trainable_variables
�layers
�metrics
 �layer_regularization_losses
�layer_metrics
	variables
trainable_variables
regularization_losses
__call__
_default_save_signature
*&call_and_return_all_conditional_losses
&"call_and_return_conditional_losses*
* 
* 
* 

�serving_default* 
b\
VARIABLE_VALUEBlock0_Conv/kernel6layer_with_weights-0/kernel/.ATTRIBUTES/VARIABLE_VALUE*
^X
VARIABLE_VALUEBlock0_Conv/bias4layer_with_weights-0/bias/.ATTRIBUTES/VARIABLE_VALUE*

0
1*

0
1*
* 
�
�non_trainable_variables
�layers
�metrics
 �layer_regularization_losses
�layer_metrics
	variables
trainable_variables
regularization_losses
__call__
* &call_and_return_all_conditional_losses
& "call_and_return_conditional_losses*
* 
* 
* 
* 
* 
�
�non_trainable_variables
�layers
�metrics
 �layer_regularization_losses
�layer_metrics
!	variables
"trainable_variables
#regularization_losses
%__call__
*&&call_and_return_all_conditional_losses
&&"call_and_return_conditional_losses* 
* 
* 
* 
`Z
VARIABLE_VALUEBlock0_Norm/gamma5layer_with_weights-1/gamma/.ATTRIBUTES/VARIABLE_VALUE*
^X
VARIABLE_VALUEBlock0_Norm/beta4layer_with_weights-1/beta/.ATTRIBUTES/VARIABLE_VALUE*
lf
VARIABLE_VALUEBlock0_Norm/moving_mean;layer_with_weights-1/moving_mean/.ATTRIBUTES/VARIABLE_VALUE*
tn
VARIABLE_VALUEBlock0_Norm/moving_variance?layer_with_weights-1/moving_variance/.ATTRIBUTES/VARIABLE_VALUE*
 
(0
)1
*2
+3*

(0
)1*
* 
�
�non_trainable_variables
�layers
�metrics
 �layer_regularization_losses
�layer_metrics
,	variables
-trainable_variables
.regularization_losses
0__call__
*1&call_and_return_all_conditional_losses
&1"call_and_return_conditional_losses*
* 
* 
b\
VARIABLE_VALUEBlock1_Conv/kernel6layer_with_weights-2/kernel/.ATTRIBUTES/VARIABLE_VALUE*
^X
VARIABLE_VALUEBlock1_Conv/bias4layer_with_weights-2/bias/.ATTRIBUTES/VARIABLE_VALUE*

20
31*

20
31*
* 
�
�non_trainable_variables
�layers
�metrics
 �layer_regularization_losses
�layer_metrics
4	variables
5trainable_variables
6regularization_losses
8__call__
*9&call_and_return_all_conditional_losses
&9"call_and_return_conditional_losses*
* 
* 
* 
* 
* 
�
�non_trainable_variables
�layers
�metrics
 �layer_regularization_losses
�layer_metrics
:	variables
;trainable_variables
<regularization_losses
>__call__
*?&call_and_return_all_conditional_losses
&?"call_and_return_conditional_losses* 
* 
* 
* 
`Z
VARIABLE_VALUEBlock1_Norm/gamma5layer_with_weights-3/gamma/.ATTRIBUTES/VARIABLE_VALUE*
^X
VARIABLE_VALUEBlock1_Norm/beta4layer_with_weights-3/beta/.ATTRIBUTES/VARIABLE_VALUE*
lf
VARIABLE_VALUEBlock1_Norm/moving_mean;layer_with_weights-3/moving_mean/.ATTRIBUTES/VARIABLE_VALUE*
tn
VARIABLE_VALUEBlock1_Norm/moving_variance?layer_with_weights-3/moving_variance/.ATTRIBUTES/VARIABLE_VALUE*
 
A0
B1
C2
D3*

A0
B1*
* 
�
�non_trainable_variables
�layers
�metrics
 �layer_regularization_losses
�layer_metrics
E	variables
Ftrainable_variables
Gregularization_losses
I__call__
*J&call_and_return_all_conditional_losses
&J"call_and_return_conditional_losses*
* 
* 
b\
VARIABLE_VALUEBlock2_Conv/kernel6layer_with_weights-4/kernel/.ATTRIBUTES/VARIABLE_VALUE*
^X
VARIABLE_VALUEBlock2_Conv/bias4layer_with_weights-4/bias/.ATTRIBUTES/VARIABLE_VALUE*

K0
L1*

K0
L1*
* 
�
�non_trainable_variables
�layers
�metrics
 �layer_regularization_losses
�layer_metrics
M	variables
Ntrainable_variables
Oregularization_losses
Q__call__
*R&call_and_return_all_conditional_losses
&R"call_and_return_conditional_losses*
* 
* 
* 
* 
* 
�
�non_trainable_variables
�layers
�metrics
 �layer_regularization_losses
�layer_metrics
S	variables
Ttrainable_variables
Uregularization_losses
W__call__
*X&call_and_return_all_conditional_losses
&X"call_and_return_conditional_losses* 
* 
* 
* 
`Z
VARIABLE_VALUEBlock2_Norm/gamma5layer_with_weights-5/gamma/.ATTRIBUTES/VARIABLE_VALUE*
^X
VARIABLE_VALUEBlock2_Norm/beta4layer_with_weights-5/beta/.ATTRIBUTES/VARIABLE_VALUE*
lf
VARIABLE_VALUEBlock2_Norm/moving_mean;layer_with_weights-5/moving_mean/.ATTRIBUTES/VARIABLE_VALUE*
tn
VARIABLE_VALUEBlock2_Norm/moving_variance?layer_with_weights-5/moving_variance/.ATTRIBUTES/VARIABLE_VALUE*
 
Z0
[1
\2
]3*

Z0
[1*
* 
�
�non_trainable_variables
�layers
�metrics
 �layer_regularization_losses
�layer_metrics
^	variables
_trainable_variables
`regularization_losses
b__call__
*c&call_and_return_all_conditional_losses
&c"call_and_return_conditional_losses*
* 
* 
b\
VARIABLE_VALUEBlock3_Conv/kernel6layer_with_weights-6/kernel/.ATTRIBUTES/VARIABLE_VALUE*
^X
VARIABLE_VALUEBlock3_Conv/bias4layer_with_weights-6/bias/.ATTRIBUTES/VARIABLE_VALUE*

d0
e1*

d0
e1*
* 
�
�non_trainable_variables
�layers
�metrics
 �layer_regularization_losses
�layer_metrics
f	variables
gtrainable_variables
hregularization_losses
j__call__
*k&call_and_return_all_conditional_losses
&k"call_and_return_conditional_losses*
* 
* 
* 
* 
* 
�
�non_trainable_variables
�layers
�metrics
 �layer_regularization_losses
�layer_metrics
l	variables
mtrainable_variables
nregularization_losses
p__call__
*q&call_and_return_all_conditional_losses
&q"call_and_return_conditional_losses* 
* 
* 
* 
`Z
VARIABLE_VALUEBlock3_Norm/gamma5layer_with_weights-7/gamma/.ATTRIBUTES/VARIABLE_VALUE*
^X
VARIABLE_VALUEBlock3_Norm/beta4layer_with_weights-7/beta/.ATTRIBUTES/VARIABLE_VALUE*
lf
VARIABLE_VALUEBlock3_Norm/moving_mean;layer_with_weights-7/moving_mean/.ATTRIBUTES/VARIABLE_VALUE*
tn
VARIABLE_VALUEBlock3_Norm/moving_variance?layer_with_weights-7/moving_variance/.ATTRIBUTES/VARIABLE_VALUE*
 
s0
t1
u2
v3*

s0
t1*
* 
�
�non_trainable_variables
�layers
�metrics
 �layer_regularization_losses
�layer_metrics
w	variables
xtrainable_variables
yregularization_losses
{__call__
*|&call_and_return_all_conditional_losses
&|"call_and_return_conditional_losses*
* 
* 
* 
* 
* 
�
�non_trainable_variables
�layers
�metrics
 �layer_regularization_losses
�layer_metrics
}	variables
~trainable_variables
regularization_losses
�__call__
+�&call_and_return_all_conditional_losses
'�"call_and_return_conditional_losses* 
* 
* 
b\
VARIABLE_VALUEFinal_Dense/kernel6layer_with_weights-8/kernel/.ATTRIBUTES/VARIABLE_VALUE*
^X
VARIABLE_VALUEFinal_Dense/bias4layer_with_weights-8/bias/.ATTRIBUTES/VARIABLE_VALUE*

�0
�1*

�0
�1*
* 
�
�non_trainable_variables
�layers
�metrics
 �layer_regularization_losses
�layer_metrics
�	variables
�trainable_variables
�regularization_losses
�__call__
+�&call_and_return_all_conditional_losses
'�"call_and_return_conditional_losses*
* 
* 
]W
VARIABLE_VALUEoutput/kernel6layer_with_weights-9/kernel/.ATTRIBUTES/VARIABLE_VALUE*
YS
VARIABLE_VALUEoutput/bias4layer_with_weights-9/bias/.ATTRIBUTES/VARIABLE_VALUE*

�0
�1*

�0
�1*
* 
�
�non_trainable_variables
�layers
�metrics
 �layer_regularization_losses
�layer_metrics
�	variables
�trainable_variables
�regularization_losses
�__call__
+�&call_and_return_all_conditional_losses
'�"call_and_return_conditional_losses*
* 
* 
LF
VARIABLE_VALUE	Adam/iter)optimizer/iter/.ATTRIBUTES/VARIABLE_VALUE*
PJ
VARIABLE_VALUEAdam/beta_1+optimizer/beta_1/.ATTRIBUTES/VARIABLE_VALUE*
PJ
VARIABLE_VALUEAdam/beta_2+optimizer/beta_2/.ATTRIBUTES/VARIABLE_VALUE*
NH
VARIABLE_VALUE
Adam/decay*optimizer/decay/.ATTRIBUTES/VARIABLE_VALUE*
^X
VARIABLE_VALUEAdam/learning_rate2optimizer/learning_rate/.ATTRIBUTES/VARIABLE_VALUE*
<
*0
+1
C2
D3
\4
]5
u6
v7*
r
0
1
2
3
4
5
6
7
	8

9
10
11
12
13
14*

�0
�1*
* 
* 
* 
* 
* 
* 
* 
* 
* 
* 
* 
* 
* 

*0
+1*
* 
* 
* 
* 
* 
* 
* 
* 
* 
* 
* 
* 
* 
* 

C0
D1*
* 
* 
* 
* 
* 
* 
* 
* 
* 
* 
* 
* 
* 
* 

\0
]1*
* 
* 
* 
* 
* 
* 
* 
* 
* 
* 
* 
* 
* 
* 

u0
v1*
* 
* 
* 
* 
* 
* 
* 
* 
* 
* 
* 
* 
* 
* 
* 
* 
* 
* 
* 
<

�total

�count
�	variables
�	keras_api*
M

�total

�count
�
_fn_kwargs
�	variables
�	keras_api*
SM
VARIABLE_VALUEtotal4keras_api/metrics/0/total/.ATTRIBUTES/VARIABLE_VALUE*
SM
VARIABLE_VALUEcount4keras_api/metrics/0/count/.ATTRIBUTES/VARIABLE_VALUE*

�0
�1*

�	variables*
UO
VARIABLE_VALUEtotal_14keras_api/metrics/1/total/.ATTRIBUTES/VARIABLE_VALUE*
UO
VARIABLE_VALUEcount_14keras_api/metrics/1/count/.ATTRIBUTES/VARIABLE_VALUE*
* 

�0
�1*

�	variables*
�
VARIABLE_VALUEAdam/Block0_Conv/kernel/mRlayer_with_weights-0/kernel/.OPTIMIZER_SLOT/optimizer/m/.ATTRIBUTES/VARIABLE_VALUE*
�{
VARIABLE_VALUEAdam/Block0_Conv/bias/mPlayer_with_weights-0/bias/.OPTIMIZER_SLOT/optimizer/m/.ATTRIBUTES/VARIABLE_VALUE*
�}
VARIABLE_VALUEAdam/Block0_Norm/gamma/mQlayer_with_weights-1/gamma/.OPTIMIZER_SLOT/optimizer/m/.ATTRIBUTES/VARIABLE_VALUE*
�{
VARIABLE_VALUEAdam/Block0_Norm/beta/mPlayer_with_weights-1/beta/.OPTIMIZER_SLOT/optimizer/m/.ATTRIBUTES/VARIABLE_VALUE*
�
VARIABLE_VALUEAdam/Block1_Conv/kernel/mRlayer_with_weights-2/kernel/.OPTIMIZER_SLOT/optimizer/m/.ATTRIBUTES/VARIABLE_VALUE*
�{
VARIABLE_VALUEAdam/Block1_Conv/bias/mPlayer_with_weights-2/bias/.OPTIMIZER_SLOT/optimizer/m/.ATTRIBUTES/VARIABLE_VALUE*
�}
VARIABLE_VALUEAdam/Block1_Norm/gamma/mQlayer_with_weights-3/gamma/.OPTIMIZER_SLOT/optimizer/m/.ATTRIBUTES/VARIABLE_VALUE*
�{
VARIABLE_VALUEAdam/Block1_Norm/beta/mPlayer_with_weights-3/beta/.OPTIMIZER_SLOT/optimizer/m/.ATTRIBUTES/VARIABLE_VALUE*
�
VARIABLE_VALUEAdam/Block2_Conv/kernel/mRlayer_with_weights-4/kernel/.OPTIMIZER_SLOT/optimizer/m/.ATTRIBUTES/VARIABLE_VALUE*
�{
VARIABLE_VALUEAdam/Block2_Conv/bias/mPlayer_with_weights-4/bias/.OPTIMIZER_SLOT/optimizer/m/.ATTRIBUTES/VARIABLE_VALUE*
�}
VARIABLE_VALUEAdam/Block2_Norm/gamma/mQlayer_with_weights-5/gamma/.OPTIMIZER_SLOT/optimizer/m/.ATTRIBUTES/VARIABLE_VALUE*
�{
VARIABLE_VALUEAdam/Block2_Norm/beta/mPlayer_with_weights-5/beta/.OPTIMIZER_SLOT/optimizer/m/.ATTRIBUTES/VARIABLE_VALUE*
�
VARIABLE_VALUEAdam/Block3_Conv/kernel/mRlayer_with_weights-6/kernel/.OPTIMIZER_SLOT/optimizer/m/.ATTRIBUTES/VARIABLE_VALUE*
�{
VARIABLE_VALUEAdam/Block3_Conv/bias/mPlayer_with_weights-6/bias/.OPTIMIZER_SLOT/optimizer/m/.ATTRIBUTES/VARIABLE_VALUE*
�}
VARIABLE_VALUEAdam/Block3_Norm/gamma/mQlayer_with_weights-7/gamma/.OPTIMIZER_SLOT/optimizer/m/.ATTRIBUTES/VARIABLE_VALUE*
�{
VARIABLE_VALUEAdam/Block3_Norm/beta/mPlayer_with_weights-7/beta/.OPTIMIZER_SLOT/optimizer/m/.ATTRIBUTES/VARIABLE_VALUE*
�
VARIABLE_VALUEAdam/Final_Dense/kernel/mRlayer_with_weights-8/kernel/.OPTIMIZER_SLOT/optimizer/m/.ATTRIBUTES/VARIABLE_VALUE*
�{
VARIABLE_VALUEAdam/Final_Dense/bias/mPlayer_with_weights-8/bias/.OPTIMIZER_SLOT/optimizer/m/.ATTRIBUTES/VARIABLE_VALUE*
�z
VARIABLE_VALUEAdam/output/kernel/mRlayer_with_weights-9/kernel/.OPTIMIZER_SLOT/optimizer/m/.ATTRIBUTES/VARIABLE_VALUE*
|v
VARIABLE_VALUEAdam/output/bias/mPlayer_with_weights-9/bias/.OPTIMIZER_SLOT/optimizer/m/.ATTRIBUTES/VARIABLE_VALUE*
�
VARIABLE_VALUEAdam/Block0_Conv/kernel/vRlayer_with_weights-0/kernel/.OPTIMIZER_SLOT/optimizer/v/.ATTRIBUTES/VARIABLE_VALUE*
�{
VARIABLE_VALUEAdam/Block0_Conv/bias/vPlayer_with_weights-0/bias/.OPTIMIZER_SLOT/optimizer/v/.ATTRIBUTES/VARIABLE_VALUE*
�}
VARIABLE_VALUEAdam/Block0_Norm/gamma/vQlayer_with_weights-1/gamma/.OPTIMIZER_SLOT/optimizer/v/.ATTRIBUTES/VARIABLE_VALUE*
�{
VARIABLE_VALUEAdam/Block0_Norm/beta/vPlayer_with_weights-1/beta/.OPTIMIZER_SLOT/optimizer/v/.ATTRIBUTES/VARIABLE_VALUE*
�
VARIABLE_VALUEAdam/Block1_Conv/kernel/vRlayer_with_weights-2/kernel/.OPTIMIZER_SLOT/optimizer/v/.ATTRIBUTES/VARIABLE_VALUE*
�{
VARIABLE_VALUEAdam/Block1_Conv/bias/vPlayer_with_weights-2/bias/.OPTIMIZER_SLOT/optimizer/v/.ATTRIBUTES/VARIABLE_VALUE*
�}
VARIABLE_VALUEAdam/Block1_Norm/gamma/vQlayer_with_weights-3/gamma/.OPTIMIZER_SLOT/optimizer/v/.ATTRIBUTES/VARIABLE_VALUE*
�{
VARIABLE_VALUEAdam/Block1_Norm/beta/vPlayer_with_weights-3/beta/.OPTIMIZER_SLOT/optimizer/v/.ATTRIBUTES/VARIABLE_VALUE*
�
VARIABLE_VALUEAdam/Block2_Conv/kernel/vRlayer_with_weights-4/kernel/.OPTIMIZER_SLOT/optimizer/v/.ATTRIBUTES/VARIABLE_VALUE*
�{
VARIABLE_VALUEAdam/Block2_Conv/bias/vPlayer_with_weights-4/bias/.OPTIMIZER_SLOT/optimizer/v/.ATTRIBUTES/VARIABLE_VALUE*
�}
VARIABLE_VALUEAdam/Block2_Norm/gamma/vQlayer_with_weights-5/gamma/.OPTIMIZER_SLOT/optimizer/v/.ATTRIBUTES/VARIABLE_VALUE*
�{
VARIABLE_VALUEAdam/Block2_Norm/beta/vPlayer_with_weights-5/beta/.OPTIMIZER_SLOT/optimizer/v/.ATTRIBUTES/VARIABLE_VALUE*
�
VARIABLE_VALUEAdam/Block3_Conv/kernel/vRlayer_with_weights-6/kernel/.OPTIMIZER_SLOT/optimizer/v/.ATTRIBUTES/VARIABLE_VALUE*
�{
VARIABLE_VALUEAdam/Block3_Conv/bias/vPlayer_with_weights-6/bias/.OPTIMIZER_SLOT/optimizer/v/.ATTRIBUTES/VARIABLE_VALUE*
�}
VARIABLE_VALUEAdam/Block3_Norm/gamma/vQlayer_with_weights-7/gamma/.OPTIMIZER_SLOT/optimizer/v/.ATTRIBUTES/VARIABLE_VALUE*
�{
VARIABLE_VALUEAdam/Block3_Norm/beta/vPlayer_with_weights-7/beta/.OPTIMIZER_SLOT/optimizer/v/.ATTRIBUTES/VARIABLE_VALUE*
�
VARIABLE_VALUEAdam/Final_Dense/kernel/vRlayer_with_weights-8/kernel/.OPTIMIZER_SLOT/optimizer/v/.ATTRIBUTES/VARIABLE_VALUE*
�{
VARIABLE_VALUEAdam/Final_Dense/bias/vPlayer_with_weights-8/bias/.OPTIMIZER_SLOT/optimizer/v/.ATTRIBUTES/VARIABLE_VALUE*
�z
VARIABLE_VALUEAdam/output/kernel/vRlayer_with_weights-9/kernel/.OPTIMIZER_SLOT/optimizer/v/.ATTRIBUTES/VARIABLE_VALUE*
|v
VARIABLE_VALUEAdam/output/bias/vPlayer_with_weights-9/bias/.OPTIMIZER_SLOT/optimizer/v/.ATTRIBUTES/VARIABLE_VALUE*
�
serving_default_input_1Placeholder*1
_output_shapes
:�����������*
dtype0*&
shape:�����������
�
StatefulPartitionedCallStatefulPartitionedCallserving_default_input_1Block0_Conv/kernelBlock0_Conv/biasBlock0_Norm/gammaBlock0_Norm/betaBlock0_Norm/moving_meanBlock0_Norm/moving_varianceBlock1_Conv/kernelBlock1_Conv/biasBlock1_Norm/gammaBlock1_Norm/betaBlock1_Norm/moving_meanBlock1_Norm/moving_varianceBlock2_Conv/kernelBlock2_Conv/biasBlock2_Norm/gammaBlock2_Norm/betaBlock2_Norm/moving_meanBlock2_Norm/moving_varianceBlock3_Conv/kernelBlock3_Conv/biasBlock3_Norm/gammaBlock3_Norm/betaBlock3_Norm/moving_meanBlock3_Norm/moving_varianceFinal_Dense/kernelFinal_Dense/biasoutput/kerneloutput/bias*(
Tin!
2*
Tout
2*
_collective_manager_ids
 *'
_output_shapes
:���������*>
_read_only_resource_inputs 
	
*0
config_proto 

CPU

GPU2*0J 8� *+
f&R$
"__inference_signature_wrapper_8558
O
saver_filenamePlaceholder*
_output_shapes
: *
dtype0*
shape: 
�
StatefulPartitionedCall_1StatefulPartitionedCallsaver_filename&Block0_Conv/kernel/Read/ReadVariableOp$Block0_Conv/bias/Read/ReadVariableOp%Block0_Norm/gamma/Read/ReadVariableOp$Block0_Norm/beta/Read/ReadVariableOp+Block0_Norm/moving_mean/Read/ReadVariableOp/Block0_Norm/moving_variance/Read/ReadVariableOp&Block1_Conv/kernel/Read/ReadVariableOp$Block1_Conv/bias/Read/ReadVariableOp%Block1_Norm/gamma/Read/ReadVariableOp$Block1_Norm/beta/Read/ReadVariableOp+Block1_Norm/moving_mean/Read/ReadVariableOp/Block1_Norm/moving_variance/Read/ReadVariableOp&Block2_Conv/kernel/Read/ReadVariableOp$Block2_Conv/bias/Read/ReadVariableOp%Block2_Norm/gamma/Read/ReadVariableOp$Block2_Norm/beta/Read/ReadVariableOp+Block2_Norm/moving_mean/Read/ReadVariableOp/Block2_Norm/moving_variance/Read/ReadVariableOp&Block3_Conv/kernel/Read/ReadVariableOp$Block3_Conv/bias/Read/ReadVariableOp%Block3_Norm/gamma/Read/ReadVariableOp$Block3_Norm/beta/Read/ReadVariableOp+Block3_Norm/moving_mean/Read/ReadVariableOp/Block3_Norm/moving_variance/Read/ReadVariableOp&Final_Dense/kernel/Read/ReadVariableOp$Final_Dense/bias/Read/ReadVariableOp!output/kernel/Read/ReadVariableOpoutput/bias/Read/ReadVariableOpAdam/iter/Read/ReadVariableOpAdam/beta_1/Read/ReadVariableOpAdam/beta_2/Read/ReadVariableOpAdam/decay/Read/ReadVariableOp&Adam/learning_rate/Read/ReadVariableOptotal/Read/ReadVariableOpcount/Read/ReadVariableOptotal_1/Read/ReadVariableOpcount_1/Read/ReadVariableOp-Adam/Block0_Conv/kernel/m/Read/ReadVariableOp+Adam/Block0_Conv/bias/m/Read/ReadVariableOp,Adam/Block0_Norm/gamma/m/Read/ReadVariableOp+Adam/Block0_Norm/beta/m/Read/ReadVariableOp-Adam/Block1_Conv/kernel/m/Read/ReadVariableOp+Adam/Block1_Conv/bias/m/Read/ReadVariableOp,Adam/Block1_Norm/gamma/m/Read/ReadVariableOp+Adam/Block1_Norm/beta/m/Read/ReadVariableOp-Adam/Block2_Conv/kernel/m/Read/ReadVariableOp+Adam/Block2_Conv/bias/m/Read/ReadVariableOp,Adam/Block2_Norm/gamma/m/Read/ReadVariableOp+Adam/Block2_Norm/beta/m/Read/ReadVariableOp-Adam/Block3_Conv/kernel/m/Read/ReadVariableOp+Adam/Block3_Conv/bias/m/Read/ReadVariableOp,Adam/Block3_Norm/gamma/m/Read/ReadVariableOp+Adam/Block3_Norm/beta/m/Read/ReadVariableOp-Adam/Final_Dense/kernel/m/Read/ReadVariableOp+Adam/Final_Dense/bias/m/Read/ReadVariableOp(Adam/output/kernel/m/Read/ReadVariableOp&Adam/output/bias/m/Read/ReadVariableOp-Adam/Block0_Conv/kernel/v/Read/ReadVariableOp+Adam/Block0_Conv/bias/v/Read/ReadVariableOp,Adam/Block0_Norm/gamma/v/Read/ReadVariableOp+Adam/Block0_Norm/beta/v/Read/ReadVariableOp-Adam/Block1_Conv/kernel/v/Read/ReadVariableOp+Adam/Block1_Conv/bias/v/Read/ReadVariableOp,Adam/Block1_Norm/gamma/v/Read/ReadVariableOp+Adam/Block1_Norm/beta/v/Read/ReadVariableOp-Adam/Block2_Conv/kernel/v/Read/ReadVariableOp+Adam/Block2_Conv/bias/v/Read/ReadVariableOp,Adam/Block2_Norm/gamma/v/Read/ReadVariableOp+Adam/Block2_Norm/beta/v/Read/ReadVariableOp-Adam/Block3_Conv/kernel/v/Read/ReadVariableOp+Adam/Block3_Conv/bias/v/Read/ReadVariableOp,Adam/Block3_Norm/gamma/v/Read/ReadVariableOp+Adam/Block3_Norm/beta/v/Read/ReadVariableOp-Adam/Final_Dense/kernel/v/Read/ReadVariableOp+Adam/Final_Dense/bias/v/Read/ReadVariableOp(Adam/output/kernel/v/Read/ReadVariableOp&Adam/output/bias/v/Read/ReadVariableOpConst*Z
TinS
Q2O	*
Tout
2*
_collective_manager_ids
 *
_output_shapes
: * 
_read_only_resource_inputs
 *0
config_proto 

CPU

GPU2*0J 8� *&
f!R
__inference__traced_save_9231
�
StatefulPartitionedCall_2StatefulPartitionedCallsaver_filenameBlock0_Conv/kernelBlock0_Conv/biasBlock0_Norm/gammaBlock0_Norm/betaBlock0_Norm/moving_meanBlock0_Norm/moving_varianceBlock1_Conv/kernelBlock1_Conv/biasBlock1_Norm/gammaBlock1_Norm/betaBlock1_Norm/moving_meanBlock1_Norm/moving_varianceBlock2_Conv/kernelBlock2_Conv/biasBlock2_Norm/gammaBlock2_Norm/betaBlock2_Norm/moving_meanBlock2_Norm/moving_varianceBlock3_Conv/kernelBlock3_Conv/biasBlock3_Norm/gammaBlock3_Norm/betaBlock3_Norm/moving_meanBlock3_Norm/moving_varianceFinal_Dense/kernelFinal_Dense/biasoutput/kerneloutput/bias	Adam/iterAdam/beta_1Adam/beta_2
Adam/decayAdam/learning_ratetotalcounttotal_1count_1Adam/Block0_Conv/kernel/mAdam/Block0_Conv/bias/mAdam/Block0_Norm/gamma/mAdam/Block0_Norm/beta/mAdam/Block1_Conv/kernel/mAdam/Block1_Conv/bias/mAdam/Block1_Norm/gamma/mAdam/Block1_Norm/beta/mAdam/Block2_Conv/kernel/mAdam/Block2_Conv/bias/mAdam/Block2_Norm/gamma/mAdam/Block2_Norm/beta/mAdam/Block3_Conv/kernel/mAdam/Block3_Conv/bias/mAdam/Block3_Norm/gamma/mAdam/Block3_Norm/beta/mAdam/Final_Dense/kernel/mAdam/Final_Dense/bias/mAdam/output/kernel/mAdam/output/bias/mAdam/Block0_Conv/kernel/vAdam/Block0_Conv/bias/vAdam/Block0_Norm/gamma/vAdam/Block0_Norm/beta/vAdam/Block1_Conv/kernel/vAdam/Block1_Conv/bias/vAdam/Block1_Norm/gamma/vAdam/Block1_Norm/beta/vAdam/Block2_Conv/kernel/vAdam/Block2_Conv/bias/vAdam/Block2_Norm/gamma/vAdam/Block2_Norm/beta/vAdam/Block3_Conv/kernel/vAdam/Block3_Conv/bias/vAdam/Block3_Norm/gamma/vAdam/Block3_Norm/beta/vAdam/Final_Dense/kernel/vAdam/Final_Dense/bias/vAdam/output/kernel/vAdam/output/bias/v*Y
TinR
P2N*
Tout
2*
_collective_manager_ids
 *
_output_shapes
: * 
_read_only_resource_inputs
 *0
config_proto 

CPU

GPU2*0J 8� *)
f$R"
 __inference__traced_restore_9472߾
�G
�
D__inference_sequential_layer_call_and_return_conditional_losses_7624

inputs*
block0_conv_7492:
block0_conv_7494:
block0_norm_7498:
block0_norm_7500:
block0_norm_7502:
block0_norm_7504:*
block1_conv_7519: 
block1_conv_7521: 
block1_norm_7525: 
block1_norm_7527: 
block1_norm_7529: 
block1_norm_7531: *
block2_conv_7546: @
block2_conv_7548:@
block2_norm_7552:@
block2_norm_7554:@
block2_norm_7556:@
block2_norm_7558:@+
block3_conv_7573:@�
block3_conv_7575:	�
block3_norm_7579:	�
block3_norm_7581:	�
block3_norm_7583:	�
block3_norm_7585:	�$
final_dense_7601:
��
final_dense_7603:	�
output_7618:	�
output_7620:
identity��#Block0_Conv/StatefulPartitionedCall�#Block0_Norm/StatefulPartitionedCall�#Block1_Conv/StatefulPartitionedCall�#Block1_Norm/StatefulPartitionedCall�#Block2_Conv/StatefulPartitionedCall�#Block2_Norm/StatefulPartitionedCall�#Block3_Conv/StatefulPartitionedCall�#Block3_Norm/StatefulPartitionedCall�#Final_Dense/StatefulPartitionedCall�output/StatefulPartitionedCall�
#Block0_Conv/StatefulPartitionedCallStatefulPartitionedCallinputsblock0_conv_7492block0_conv_7494*
Tin
2*
Tout
2*
_collective_manager_ids
 *1
_output_shapes
:�����������*$
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8� *N
fIRG
E__inference_Block0_Conv_layer_call_and_return_conditional_losses_7491�
Block0_Pool/PartitionedCallPartitionedCall,Block0_Conv/StatefulPartitionedCall:output:0*
Tin
2*
Tout
2*
_collective_manager_ids
 */
_output_shapes
:���������@V* 
_read_only_resource_inputs
 *0
config_proto 

CPU

GPU2*0J 8� *N
fIRG
E__inference_Block0_Pool_layer_call_and_return_conditional_losses_7165�
#Block0_Norm/StatefulPartitionedCallStatefulPartitionedCall$Block0_Pool/PartitionedCall:output:0block0_norm_7498block0_norm_7500block0_norm_7502block0_norm_7504*
Tin	
2*
Tout
2*
_collective_manager_ids
 */
_output_shapes
:���������@V*&
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8� *N
fIRG
E__inference_Block0_Norm_layer_call_and_return_conditional_losses_7190�
#Block1_Conv/StatefulPartitionedCallStatefulPartitionedCall,Block0_Norm/StatefulPartitionedCall:output:0block1_conv_7519block1_conv_7521*
Tin
2*
Tout
2*
_collective_manager_ids
 */
_output_shapes
:���������@V *$
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8� *N
fIRG
E__inference_Block1_Conv_layer_call_and_return_conditional_losses_7518�
Block1_Pool/PartitionedCallPartitionedCall,Block1_Conv/StatefulPartitionedCall:output:0*
Tin
2*
Tout
2*
_collective_manager_ids
 */
_output_shapes
:��������� + * 
_read_only_resource_inputs
 *0
config_proto 

CPU

GPU2*0J 8� *N
fIRG
E__inference_Block1_Pool_layer_call_and_return_conditional_losses_7241�
#Block1_Norm/StatefulPartitionedCallStatefulPartitionedCall$Block1_Pool/PartitionedCall:output:0block1_norm_7525block1_norm_7527block1_norm_7529block1_norm_7531*
Tin	
2*
Tout
2*
_collective_manager_ids
 */
_output_shapes
:��������� + *&
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8� *N
fIRG
E__inference_Block1_Norm_layer_call_and_return_conditional_losses_7266�
#Block2_Conv/StatefulPartitionedCallStatefulPartitionedCall,Block1_Norm/StatefulPartitionedCall:output:0block2_conv_7546block2_conv_7548*
Tin
2*
Tout
2*
_collective_manager_ids
 */
_output_shapes
:��������� +@*$
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8� *N
fIRG
E__inference_Block2_Conv_layer_call_and_return_conditional_losses_7545�
Block2_Pool/PartitionedCallPartitionedCall,Block2_Conv/StatefulPartitionedCall:output:0*
Tin
2*
Tout
2*
_collective_manager_ids
 */
_output_shapes
:���������@* 
_read_only_resource_inputs
 *0
config_proto 

CPU

GPU2*0J 8� *N
fIRG
E__inference_Block2_Pool_layer_call_and_return_conditional_losses_7317�
#Block2_Norm/StatefulPartitionedCallStatefulPartitionedCall$Block2_Pool/PartitionedCall:output:0block2_norm_7552block2_norm_7554block2_norm_7556block2_norm_7558*
Tin	
2*
Tout
2*
_collective_manager_ids
 */
_output_shapes
:���������@*&
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8� *N
fIRG
E__inference_Block2_Norm_layer_call_and_return_conditional_losses_7342�
#Block3_Conv/StatefulPartitionedCallStatefulPartitionedCall,Block2_Norm/StatefulPartitionedCall:output:0block3_conv_7573block3_conv_7575*
Tin
2*
Tout
2*
_collective_manager_ids
 *0
_output_shapes
:����������*$
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8� *N
fIRG
E__inference_Block3_Conv_layer_call_and_return_conditional_losses_7572�
Block3_Pool/PartitionedCallPartitionedCall,Block3_Conv/StatefulPartitionedCall:output:0*
Tin
2*
Tout
2*
_collective_manager_ids
 *0
_output_shapes
:���������
�* 
_read_only_resource_inputs
 *0
config_proto 

CPU

GPU2*0J 8� *N
fIRG
E__inference_Block3_Pool_layer_call_and_return_conditional_losses_7393�
#Block3_Norm/StatefulPartitionedCallStatefulPartitionedCall$Block3_Pool/PartitionedCall:output:0block3_norm_7579block3_norm_7581block3_norm_7583block3_norm_7585*
Tin	
2*
Tout
2*
_collective_manager_ids
 *0
_output_shapes
:���������
�*&
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8� *N
fIRG
E__inference_Block3_Norm_layer_call_and_return_conditional_losses_7418�
 GlobalMaxPooling/PartitionedCallPartitionedCall,Block3_Norm/StatefulPartitionedCall:output:0*
Tin
2*
Tout
2*
_collective_manager_ids
 *(
_output_shapes
:����������* 
_read_only_resource_inputs
 *0
config_proto 

CPU

GPU2*0J 8� *S
fNRL
J__inference_GlobalMaxPooling_layer_call_and_return_conditional_losses_7470�
#Final_Dense/StatefulPartitionedCallStatefulPartitionedCall)GlobalMaxPooling/PartitionedCall:output:0final_dense_7601final_dense_7603*
Tin
2*
Tout
2*
_collective_manager_ids
 *(
_output_shapes
:����������*$
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8� *N
fIRG
E__inference_Final_Dense_layer_call_and_return_conditional_losses_7600�
output/StatefulPartitionedCallStatefulPartitionedCall,Final_Dense/StatefulPartitionedCall:output:0output_7618output_7620*
Tin
2*
Tout
2*
_collective_manager_ids
 *'
_output_shapes
:���������*$
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8� *I
fDRB
@__inference_output_layer_call_and_return_conditional_losses_7617v
IdentityIdentity'output/StatefulPartitionedCall:output:0^NoOp*
T0*'
_output_shapes
:����������
NoOpNoOp$^Block0_Conv/StatefulPartitionedCall$^Block0_Norm/StatefulPartitionedCall$^Block1_Conv/StatefulPartitionedCall$^Block1_Norm/StatefulPartitionedCall$^Block2_Conv/StatefulPartitionedCall$^Block2_Norm/StatefulPartitionedCall$^Block3_Conv/StatefulPartitionedCall$^Block3_Norm/StatefulPartitionedCall$^Final_Dense/StatefulPartitionedCall^output/StatefulPartitionedCall*"
_acd_function_control_output(*
_output_shapes
 "
identityIdentity:output:0*(
_construction_contextkEagerRuntime*h
_input_shapesW
U:�����������: : : : : : : : : : : : : : : : : : : : : : : : : : : : 2J
#Block0_Conv/StatefulPartitionedCall#Block0_Conv/StatefulPartitionedCall2J
#Block0_Norm/StatefulPartitionedCall#Block0_Norm/StatefulPartitionedCall2J
#Block1_Conv/StatefulPartitionedCall#Block1_Conv/StatefulPartitionedCall2J
#Block1_Norm/StatefulPartitionedCall#Block1_Norm/StatefulPartitionedCall2J
#Block2_Conv/StatefulPartitionedCall#Block2_Conv/StatefulPartitionedCall2J
#Block2_Norm/StatefulPartitionedCall#Block2_Norm/StatefulPartitionedCall2J
#Block3_Conv/StatefulPartitionedCall#Block3_Conv/StatefulPartitionedCall2J
#Block3_Norm/StatefulPartitionedCall#Block3_Norm/StatefulPartitionedCall2J
#Final_Dense/StatefulPartitionedCall#Final_Dense/StatefulPartitionedCall2@
output/StatefulPartitionedCalloutput/StatefulPartitionedCall:Y U
1
_output_shapes
:�����������
 
_user_specified_nameinputs
�
�
*__inference_Block2_Norm_layer_call_fn_8785

inputs
unknown:@
	unknown_0:@
	unknown_1:@
	unknown_2:@
identity��StatefulPartitionedCall�
StatefulPartitionedCallStatefulPartitionedCallinputsunknown	unknown_0	unknown_1	unknown_2*
Tin	
2*
Tout
2*
_collective_manager_ids
 *A
_output_shapes/
-:+���������������������������@*&
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8� *N
fIRG
E__inference_Block2_Norm_layer_call_and_return_conditional_losses_7342�
IdentityIdentity StatefulPartitionedCall:output:0^NoOp*
T0*A
_output_shapes/
-:+���������������������������@`
NoOpNoOp^StatefulPartitionedCall*"
_acd_function_control_output(*
_output_shapes
 "
identityIdentity:output:0*(
_construction_contextkEagerRuntime*H
_input_shapes7
5:+���������������������������@: : : : 22
StatefulPartitionedCallStatefulPartitionedCall:i e
A
_output_shapes/
-:+���������������������������@
 
_user_specified_nameinputs
�
a
E__inference_Block3_Pool_layer_call_and_return_conditional_losses_8864

inputs
identity�
MaxPoolMaxPoolinputs*J
_output_shapes8
6:4������������������������������������*
ksize
*
paddingVALID*
strides
{
IdentityIdentityMaxPool:output:0*
T0*J
_output_shapes8
6:4������������������������������������"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*I
_input_shapes8
6:4������������������������������������:r n
J
_output_shapes8
6:4������������������������������������
 
_user_specified_nameinputs
͑
�
D__inference_sequential_layer_call_and_return_conditional_losses_8495

inputsD
*block0_conv_conv2d_readvariableop_resource:9
+block0_conv_biasadd_readvariableop_resource:1
#block0_norm_readvariableop_resource:3
%block0_norm_readvariableop_1_resource:B
4block0_norm_fusedbatchnormv3_readvariableop_resource:D
6block0_norm_fusedbatchnormv3_readvariableop_1_resource:D
*block1_conv_conv2d_readvariableop_resource: 9
+block1_conv_biasadd_readvariableop_resource: 1
#block1_norm_readvariableop_resource: 3
%block1_norm_readvariableop_1_resource: B
4block1_norm_fusedbatchnormv3_readvariableop_resource: D
6block1_norm_fusedbatchnormv3_readvariableop_1_resource: D
*block2_conv_conv2d_readvariableop_resource: @9
+block2_conv_biasadd_readvariableop_resource:@1
#block2_norm_readvariableop_resource:@3
%block2_norm_readvariableop_1_resource:@B
4block2_norm_fusedbatchnormv3_readvariableop_resource:@D
6block2_norm_fusedbatchnormv3_readvariableop_1_resource:@E
*block3_conv_conv2d_readvariableop_resource:@�:
+block3_conv_biasadd_readvariableop_resource:	�2
#block3_norm_readvariableop_resource:	�4
%block3_norm_readvariableop_1_resource:	�C
4block3_norm_fusedbatchnormv3_readvariableop_resource:	�E
6block3_norm_fusedbatchnormv3_readvariableop_1_resource:	�>
*final_dense_matmul_readvariableop_resource:
��:
+final_dense_biasadd_readvariableop_resource:	�8
%output_matmul_readvariableop_resource:	�4
&output_biasadd_readvariableop_resource:
identity��"Block0_Conv/BiasAdd/ReadVariableOp�!Block0_Conv/Conv2D/ReadVariableOp�Block0_Norm/AssignNewValue�Block0_Norm/AssignNewValue_1�+Block0_Norm/FusedBatchNormV3/ReadVariableOp�-Block0_Norm/FusedBatchNormV3/ReadVariableOp_1�Block0_Norm/ReadVariableOp�Block0_Norm/ReadVariableOp_1�"Block1_Conv/BiasAdd/ReadVariableOp�!Block1_Conv/Conv2D/ReadVariableOp�Block1_Norm/AssignNewValue�Block1_Norm/AssignNewValue_1�+Block1_Norm/FusedBatchNormV3/ReadVariableOp�-Block1_Norm/FusedBatchNormV3/ReadVariableOp_1�Block1_Norm/ReadVariableOp�Block1_Norm/ReadVariableOp_1�"Block2_Conv/BiasAdd/ReadVariableOp�!Block2_Conv/Conv2D/ReadVariableOp�Block2_Norm/AssignNewValue�Block2_Norm/AssignNewValue_1�+Block2_Norm/FusedBatchNormV3/ReadVariableOp�-Block2_Norm/FusedBatchNormV3/ReadVariableOp_1�Block2_Norm/ReadVariableOp�Block2_Norm/ReadVariableOp_1�"Block3_Conv/BiasAdd/ReadVariableOp�!Block3_Conv/Conv2D/ReadVariableOp�Block3_Norm/AssignNewValue�Block3_Norm/AssignNewValue_1�+Block3_Norm/FusedBatchNormV3/ReadVariableOp�-Block3_Norm/FusedBatchNormV3/ReadVariableOp_1�Block3_Norm/ReadVariableOp�Block3_Norm/ReadVariableOp_1�"Final_Dense/BiasAdd/ReadVariableOp�!Final_Dense/MatMul/ReadVariableOp�output/BiasAdd/ReadVariableOp�output/MatMul/ReadVariableOp�
!Block0_Conv/Conv2D/ReadVariableOpReadVariableOp*block0_conv_conv2d_readvariableop_resource*&
_output_shapes
:*
dtype0�
Block0_Conv/Conv2DConv2Dinputs)Block0_Conv/Conv2D/ReadVariableOp:value:0*
T0*1
_output_shapes
:�����������*
paddingSAME*
strides
�
"Block0_Conv/BiasAdd/ReadVariableOpReadVariableOp+block0_conv_biasadd_readvariableop_resource*
_output_shapes
:*
dtype0�
Block0_Conv/BiasAddBiasAddBlock0_Conv/Conv2D:output:0*Block0_Conv/BiasAdd/ReadVariableOp:value:0*
T0*1
_output_shapes
:�����������r
Block0_Conv/ReluReluBlock0_Conv/BiasAdd:output:0*
T0*1
_output_shapes
:������������
Block0_Pool/MaxPoolMaxPoolBlock0_Conv/Relu:activations:0*/
_output_shapes
:���������@V*
ksize
*
paddingVALID*
strides
z
Block0_Norm/ReadVariableOpReadVariableOp#block0_norm_readvariableop_resource*
_output_shapes
:*
dtype0~
Block0_Norm/ReadVariableOp_1ReadVariableOp%block0_norm_readvariableop_1_resource*
_output_shapes
:*
dtype0�
+Block0_Norm/FusedBatchNormV3/ReadVariableOpReadVariableOp4block0_norm_fusedbatchnormv3_readvariableop_resource*
_output_shapes
:*
dtype0�
-Block0_Norm/FusedBatchNormV3/ReadVariableOp_1ReadVariableOp6block0_norm_fusedbatchnormv3_readvariableop_1_resource*
_output_shapes
:*
dtype0�
Block0_Norm/FusedBatchNormV3FusedBatchNormV3Block0_Pool/MaxPool:output:0"Block0_Norm/ReadVariableOp:value:0$Block0_Norm/ReadVariableOp_1:value:03Block0_Norm/FusedBatchNormV3/ReadVariableOp:value:05Block0_Norm/FusedBatchNormV3/ReadVariableOp_1:value:0*
T0*
U0*K
_output_shapes9
7:���������@V:::::*
epsilon%o�:*
exponential_avg_factor%
�#<�
Block0_Norm/AssignNewValueAssignVariableOp4block0_norm_fusedbatchnormv3_readvariableop_resource)Block0_Norm/FusedBatchNormV3:batch_mean:0,^Block0_Norm/FusedBatchNormV3/ReadVariableOp*
_output_shapes
 *
dtype0�
Block0_Norm/AssignNewValue_1AssignVariableOp6block0_norm_fusedbatchnormv3_readvariableop_1_resource-Block0_Norm/FusedBatchNormV3:batch_variance:0.^Block0_Norm/FusedBatchNormV3/ReadVariableOp_1*
_output_shapes
 *
dtype0�
!Block1_Conv/Conv2D/ReadVariableOpReadVariableOp*block1_conv_conv2d_readvariableop_resource*&
_output_shapes
: *
dtype0�
Block1_Conv/Conv2DConv2D Block0_Norm/FusedBatchNormV3:y:0)Block1_Conv/Conv2D/ReadVariableOp:value:0*
T0*/
_output_shapes
:���������@V *
paddingSAME*
strides
�
"Block1_Conv/BiasAdd/ReadVariableOpReadVariableOp+block1_conv_biasadd_readvariableop_resource*
_output_shapes
: *
dtype0�
Block1_Conv/BiasAddBiasAddBlock1_Conv/Conv2D:output:0*Block1_Conv/BiasAdd/ReadVariableOp:value:0*
T0*/
_output_shapes
:���������@V p
Block1_Conv/ReluReluBlock1_Conv/BiasAdd:output:0*
T0*/
_output_shapes
:���������@V �
Block1_Pool/MaxPoolMaxPoolBlock1_Conv/Relu:activations:0*/
_output_shapes
:��������� + *
ksize
*
paddingVALID*
strides
z
Block1_Norm/ReadVariableOpReadVariableOp#block1_norm_readvariableop_resource*
_output_shapes
: *
dtype0~
Block1_Norm/ReadVariableOp_1ReadVariableOp%block1_norm_readvariableop_1_resource*
_output_shapes
: *
dtype0�
+Block1_Norm/FusedBatchNormV3/ReadVariableOpReadVariableOp4block1_norm_fusedbatchnormv3_readvariableop_resource*
_output_shapes
: *
dtype0�
-Block1_Norm/FusedBatchNormV3/ReadVariableOp_1ReadVariableOp6block1_norm_fusedbatchnormv3_readvariableop_1_resource*
_output_shapes
: *
dtype0�
Block1_Norm/FusedBatchNormV3FusedBatchNormV3Block1_Pool/MaxPool:output:0"Block1_Norm/ReadVariableOp:value:0$Block1_Norm/ReadVariableOp_1:value:03Block1_Norm/FusedBatchNormV3/ReadVariableOp:value:05Block1_Norm/FusedBatchNormV3/ReadVariableOp_1:value:0*
T0*
U0*K
_output_shapes9
7:��������� + : : : : :*
epsilon%o�:*
exponential_avg_factor%
�#<�
Block1_Norm/AssignNewValueAssignVariableOp4block1_norm_fusedbatchnormv3_readvariableop_resource)Block1_Norm/FusedBatchNormV3:batch_mean:0,^Block1_Norm/FusedBatchNormV3/ReadVariableOp*
_output_shapes
 *
dtype0�
Block1_Norm/AssignNewValue_1AssignVariableOp6block1_norm_fusedbatchnormv3_readvariableop_1_resource-Block1_Norm/FusedBatchNormV3:batch_variance:0.^Block1_Norm/FusedBatchNormV3/ReadVariableOp_1*
_output_shapes
 *
dtype0�
!Block2_Conv/Conv2D/ReadVariableOpReadVariableOp*block2_conv_conv2d_readvariableop_resource*&
_output_shapes
: @*
dtype0�
Block2_Conv/Conv2DConv2D Block1_Norm/FusedBatchNormV3:y:0)Block2_Conv/Conv2D/ReadVariableOp:value:0*
T0*/
_output_shapes
:��������� +@*
paddingSAME*
strides
�
"Block2_Conv/BiasAdd/ReadVariableOpReadVariableOp+block2_conv_biasadd_readvariableop_resource*
_output_shapes
:@*
dtype0�
Block2_Conv/BiasAddBiasAddBlock2_Conv/Conv2D:output:0*Block2_Conv/BiasAdd/ReadVariableOp:value:0*
T0*/
_output_shapes
:��������� +@p
Block2_Conv/ReluReluBlock2_Conv/BiasAdd:output:0*
T0*/
_output_shapes
:��������� +@�
Block2_Pool/MaxPoolMaxPoolBlock2_Conv/Relu:activations:0*/
_output_shapes
:���������@*
ksize
*
paddingVALID*
strides
z
Block2_Norm/ReadVariableOpReadVariableOp#block2_norm_readvariableop_resource*
_output_shapes
:@*
dtype0~
Block2_Norm/ReadVariableOp_1ReadVariableOp%block2_norm_readvariableop_1_resource*
_output_shapes
:@*
dtype0�
+Block2_Norm/FusedBatchNormV3/ReadVariableOpReadVariableOp4block2_norm_fusedbatchnormv3_readvariableop_resource*
_output_shapes
:@*
dtype0�
-Block2_Norm/FusedBatchNormV3/ReadVariableOp_1ReadVariableOp6block2_norm_fusedbatchnormv3_readvariableop_1_resource*
_output_shapes
:@*
dtype0�
Block2_Norm/FusedBatchNormV3FusedBatchNormV3Block2_Pool/MaxPool:output:0"Block2_Norm/ReadVariableOp:value:0$Block2_Norm/ReadVariableOp_1:value:03Block2_Norm/FusedBatchNormV3/ReadVariableOp:value:05Block2_Norm/FusedBatchNormV3/ReadVariableOp_1:value:0*
T0*
U0*K
_output_shapes9
7:���������@:@:@:@:@:*
epsilon%o�:*
exponential_avg_factor%
�#<�
Block2_Norm/AssignNewValueAssignVariableOp4block2_norm_fusedbatchnormv3_readvariableop_resource)Block2_Norm/FusedBatchNormV3:batch_mean:0,^Block2_Norm/FusedBatchNormV3/ReadVariableOp*
_output_shapes
 *
dtype0�
Block2_Norm/AssignNewValue_1AssignVariableOp6block2_norm_fusedbatchnormv3_readvariableop_1_resource-Block2_Norm/FusedBatchNormV3:batch_variance:0.^Block2_Norm/FusedBatchNormV3/ReadVariableOp_1*
_output_shapes
 *
dtype0�
!Block3_Conv/Conv2D/ReadVariableOpReadVariableOp*block3_conv_conv2d_readvariableop_resource*'
_output_shapes
:@�*
dtype0�
Block3_Conv/Conv2DConv2D Block2_Norm/FusedBatchNormV3:y:0)Block3_Conv/Conv2D/ReadVariableOp:value:0*
T0*0
_output_shapes
:����������*
paddingSAME*
strides
�
"Block3_Conv/BiasAdd/ReadVariableOpReadVariableOp+block3_conv_biasadd_readvariableop_resource*
_output_shapes	
:�*
dtype0�
Block3_Conv/BiasAddBiasAddBlock3_Conv/Conv2D:output:0*Block3_Conv/BiasAdd/ReadVariableOp:value:0*
T0*0
_output_shapes
:����������q
Block3_Conv/ReluReluBlock3_Conv/BiasAdd:output:0*
T0*0
_output_shapes
:�����������
Block3_Pool/MaxPoolMaxPoolBlock3_Conv/Relu:activations:0*0
_output_shapes
:���������
�*
ksize
*
paddingVALID*
strides
{
Block3_Norm/ReadVariableOpReadVariableOp#block3_norm_readvariableop_resource*
_output_shapes	
:�*
dtype0
Block3_Norm/ReadVariableOp_1ReadVariableOp%block3_norm_readvariableop_1_resource*
_output_shapes	
:�*
dtype0�
+Block3_Norm/FusedBatchNormV3/ReadVariableOpReadVariableOp4block3_norm_fusedbatchnormv3_readvariableop_resource*
_output_shapes	
:�*
dtype0�
-Block3_Norm/FusedBatchNormV3/ReadVariableOp_1ReadVariableOp6block3_norm_fusedbatchnormv3_readvariableop_1_resource*
_output_shapes	
:�*
dtype0�
Block3_Norm/FusedBatchNormV3FusedBatchNormV3Block3_Pool/MaxPool:output:0"Block3_Norm/ReadVariableOp:value:0$Block3_Norm/ReadVariableOp_1:value:03Block3_Norm/FusedBatchNormV3/ReadVariableOp:value:05Block3_Norm/FusedBatchNormV3/ReadVariableOp_1:value:0*
T0*
U0*P
_output_shapes>
<:���������
�:�:�:�:�:*
epsilon%o�:*
exponential_avg_factor%
�#<�
Block3_Norm/AssignNewValueAssignVariableOp4block3_norm_fusedbatchnormv3_readvariableop_resource)Block3_Norm/FusedBatchNormV3:batch_mean:0,^Block3_Norm/FusedBatchNormV3/ReadVariableOp*
_output_shapes
 *
dtype0�
Block3_Norm/AssignNewValue_1AssignVariableOp6block3_norm_fusedbatchnormv3_readvariableop_1_resource-Block3_Norm/FusedBatchNormV3:batch_variance:0.^Block3_Norm/FusedBatchNormV3/ReadVariableOp_1*
_output_shapes
 *
dtype0w
&GlobalMaxPooling/Max/reduction_indicesConst*
_output_shapes
:*
dtype0*
valueB"      �
GlobalMaxPooling/MaxMax Block3_Norm/FusedBatchNormV3:y:0/GlobalMaxPooling/Max/reduction_indices:output:0*
T0*(
_output_shapes
:�����������
!Final_Dense/MatMul/ReadVariableOpReadVariableOp*final_dense_matmul_readvariableop_resource* 
_output_shapes
:
��*
dtype0�
Final_Dense/MatMulMatMulGlobalMaxPooling/Max:output:0)Final_Dense/MatMul/ReadVariableOp:value:0*
T0*(
_output_shapes
:�����������
"Final_Dense/BiasAdd/ReadVariableOpReadVariableOp+final_dense_biasadd_readvariableop_resource*
_output_shapes	
:�*
dtype0�
Final_Dense/BiasAddBiasAddFinal_Dense/MatMul:product:0*Final_Dense/BiasAdd/ReadVariableOp:value:0*
T0*(
_output_shapes
:����������i
Final_Dense/ReluReluFinal_Dense/BiasAdd:output:0*
T0*(
_output_shapes
:�����������
output/MatMul/ReadVariableOpReadVariableOp%output_matmul_readvariableop_resource*
_output_shapes
:	�*
dtype0�
output/MatMulMatMulFinal_Dense/Relu:activations:0$output/MatMul/ReadVariableOp:value:0*
T0*'
_output_shapes
:����������
output/BiasAdd/ReadVariableOpReadVariableOp&output_biasadd_readvariableop_resource*
_output_shapes
:*
dtype0�
output/BiasAddBiasAddoutput/MatMul:product:0%output/BiasAdd/ReadVariableOp:value:0*
T0*'
_output_shapes
:���������d
output/SigmoidSigmoidoutput/BiasAdd:output:0*
T0*'
_output_shapes
:���������a
IdentityIdentityoutput/Sigmoid:y:0^NoOp*
T0*'
_output_shapes
:����������

NoOpNoOp#^Block0_Conv/BiasAdd/ReadVariableOp"^Block0_Conv/Conv2D/ReadVariableOp^Block0_Norm/AssignNewValue^Block0_Norm/AssignNewValue_1,^Block0_Norm/FusedBatchNormV3/ReadVariableOp.^Block0_Norm/FusedBatchNormV3/ReadVariableOp_1^Block0_Norm/ReadVariableOp^Block0_Norm/ReadVariableOp_1#^Block1_Conv/BiasAdd/ReadVariableOp"^Block1_Conv/Conv2D/ReadVariableOp^Block1_Norm/AssignNewValue^Block1_Norm/AssignNewValue_1,^Block1_Norm/FusedBatchNormV3/ReadVariableOp.^Block1_Norm/FusedBatchNormV3/ReadVariableOp_1^Block1_Norm/ReadVariableOp^Block1_Norm/ReadVariableOp_1#^Block2_Conv/BiasAdd/ReadVariableOp"^Block2_Conv/Conv2D/ReadVariableOp^Block2_Norm/AssignNewValue^Block2_Norm/AssignNewValue_1,^Block2_Norm/FusedBatchNormV3/ReadVariableOp.^Block2_Norm/FusedBatchNormV3/ReadVariableOp_1^Block2_Norm/ReadVariableOp^Block2_Norm/ReadVariableOp_1#^Block3_Conv/BiasAdd/ReadVariableOp"^Block3_Conv/Conv2D/ReadVariableOp^Block3_Norm/AssignNewValue^Block3_Norm/AssignNewValue_1,^Block3_Norm/FusedBatchNormV3/ReadVariableOp.^Block3_Norm/FusedBatchNormV3/ReadVariableOp_1^Block3_Norm/ReadVariableOp^Block3_Norm/ReadVariableOp_1#^Final_Dense/BiasAdd/ReadVariableOp"^Final_Dense/MatMul/ReadVariableOp^output/BiasAdd/ReadVariableOp^output/MatMul/ReadVariableOp*"
_acd_function_control_output(*
_output_shapes
 "
identityIdentity:output:0*(
_construction_contextkEagerRuntime*h
_input_shapesW
U:�����������: : : : : : : : : : : : : : : : : : : : : : : : : : : : 2H
"Block0_Conv/BiasAdd/ReadVariableOp"Block0_Conv/BiasAdd/ReadVariableOp2F
!Block0_Conv/Conv2D/ReadVariableOp!Block0_Conv/Conv2D/ReadVariableOp28
Block0_Norm/AssignNewValueBlock0_Norm/AssignNewValue2<
Block0_Norm/AssignNewValue_1Block0_Norm/AssignNewValue_12Z
+Block0_Norm/FusedBatchNormV3/ReadVariableOp+Block0_Norm/FusedBatchNormV3/ReadVariableOp2^
-Block0_Norm/FusedBatchNormV3/ReadVariableOp_1-Block0_Norm/FusedBatchNormV3/ReadVariableOp_128
Block0_Norm/ReadVariableOpBlock0_Norm/ReadVariableOp2<
Block0_Norm/ReadVariableOp_1Block0_Norm/ReadVariableOp_12H
"Block1_Conv/BiasAdd/ReadVariableOp"Block1_Conv/BiasAdd/ReadVariableOp2F
!Block1_Conv/Conv2D/ReadVariableOp!Block1_Conv/Conv2D/ReadVariableOp28
Block1_Norm/AssignNewValueBlock1_Norm/AssignNewValue2<
Block1_Norm/AssignNewValue_1Block1_Norm/AssignNewValue_12Z
+Block1_Norm/FusedBatchNormV3/ReadVariableOp+Block1_Norm/FusedBatchNormV3/ReadVariableOp2^
-Block1_Norm/FusedBatchNormV3/ReadVariableOp_1-Block1_Norm/FusedBatchNormV3/ReadVariableOp_128
Block1_Norm/ReadVariableOpBlock1_Norm/ReadVariableOp2<
Block1_Norm/ReadVariableOp_1Block1_Norm/ReadVariableOp_12H
"Block2_Conv/BiasAdd/ReadVariableOp"Block2_Conv/BiasAdd/ReadVariableOp2F
!Block2_Conv/Conv2D/ReadVariableOp!Block2_Conv/Conv2D/ReadVariableOp28
Block2_Norm/AssignNewValueBlock2_Norm/AssignNewValue2<
Block2_Norm/AssignNewValue_1Block2_Norm/AssignNewValue_12Z
+Block2_Norm/FusedBatchNormV3/ReadVariableOp+Block2_Norm/FusedBatchNormV3/ReadVariableOp2^
-Block2_Norm/FusedBatchNormV3/ReadVariableOp_1-Block2_Norm/FusedBatchNormV3/ReadVariableOp_128
Block2_Norm/ReadVariableOpBlock2_Norm/ReadVariableOp2<
Block2_Norm/ReadVariableOp_1Block2_Norm/ReadVariableOp_12H
"Block3_Conv/BiasAdd/ReadVariableOp"Block3_Conv/BiasAdd/ReadVariableOp2F
!Block3_Conv/Conv2D/ReadVariableOp!Block3_Conv/Conv2D/ReadVariableOp28
Block3_Norm/AssignNewValueBlock3_Norm/AssignNewValue2<
Block3_Norm/AssignNewValue_1Block3_Norm/AssignNewValue_12Z
+Block3_Norm/FusedBatchNormV3/ReadVariableOp+Block3_Norm/FusedBatchNormV3/ReadVariableOp2^
-Block3_Norm/FusedBatchNormV3/ReadVariableOp_1-Block3_Norm/FusedBatchNormV3/ReadVariableOp_128
Block3_Norm/ReadVariableOpBlock3_Norm/ReadVariableOp2<
Block3_Norm/ReadVariableOp_1Block3_Norm/ReadVariableOp_12H
"Final_Dense/BiasAdd/ReadVariableOp"Final_Dense/BiasAdd/ReadVariableOp2F
!Final_Dense/MatMul/ReadVariableOp!Final_Dense/MatMul/ReadVariableOp2>
output/BiasAdd/ReadVariableOpoutput/BiasAdd/ReadVariableOp2<
output/MatMul/ReadVariableOpoutput/MatMul/ReadVariableOp:Y U
1
_output_shapes
:�����������
 
_user_specified_nameinputs
�
�
*__inference_Block0_Norm_layer_call_fn_8614

inputs
unknown:
	unknown_0:
	unknown_1:
	unknown_2:
identity��StatefulPartitionedCall�
StatefulPartitionedCallStatefulPartitionedCallinputsunknown	unknown_0	unknown_1	unknown_2*
Tin	
2*
Tout
2*
_collective_manager_ids
 *A
_output_shapes/
-:+���������������������������*$
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8� *N
fIRG
E__inference_Block0_Norm_layer_call_and_return_conditional_losses_7221�
IdentityIdentity StatefulPartitionedCall:output:0^NoOp*
T0*A
_output_shapes/
-:+���������������������������`
NoOpNoOp^StatefulPartitionedCall*"
_acd_function_control_output(*
_output_shapes
 "
identityIdentity:output:0*(
_construction_contextkEagerRuntime*H
_input_shapes7
5:+���������������������������: : : : 22
StatefulPartitionedCallStatefulPartitionedCall:i e
A
_output_shapes/
-:+���������������������������
 
_user_specified_nameinputs
�
�
E__inference_Block3_Norm_layer_call_and_return_conditional_losses_7418

inputs&
readvariableop_resource:	�(
readvariableop_1_resource:	�7
(fusedbatchnormv3_readvariableop_resource:	�9
*fusedbatchnormv3_readvariableop_1_resource:	�
identity��FusedBatchNormV3/ReadVariableOp�!FusedBatchNormV3/ReadVariableOp_1�ReadVariableOp�ReadVariableOp_1c
ReadVariableOpReadVariableOpreadvariableop_resource*
_output_shapes	
:�*
dtype0g
ReadVariableOp_1ReadVariableOpreadvariableop_1_resource*
_output_shapes	
:�*
dtype0�
FusedBatchNormV3/ReadVariableOpReadVariableOp(fusedbatchnormv3_readvariableop_resource*
_output_shapes	
:�*
dtype0�
!FusedBatchNormV3/ReadVariableOp_1ReadVariableOp*fusedbatchnormv3_readvariableop_1_resource*
_output_shapes	
:�*
dtype0�
FusedBatchNormV3FusedBatchNormV3inputsReadVariableOp:value:0ReadVariableOp_1:value:0'FusedBatchNormV3/ReadVariableOp:value:0)FusedBatchNormV3/ReadVariableOp_1:value:0*
T0*
U0*b
_output_shapesP
N:,����������������������������:�:�:�:�:*
epsilon%o�:*
is_training( ~
IdentityIdentityFusedBatchNormV3:y:0^NoOp*
T0*B
_output_shapes0
.:,�����������������������������
NoOpNoOp ^FusedBatchNormV3/ReadVariableOp"^FusedBatchNormV3/ReadVariableOp_1^ReadVariableOp^ReadVariableOp_1*"
_acd_function_control_output(*
_output_shapes
 "
identityIdentity:output:0*(
_construction_contextkEagerRuntime*I
_input_shapes8
6:,����������������������������: : : : 2B
FusedBatchNormV3/ReadVariableOpFusedBatchNormV3/ReadVariableOp2F
!FusedBatchNormV3/ReadVariableOp_1!FusedBatchNormV3/ReadVariableOp_12 
ReadVariableOpReadVariableOp2$
ReadVariableOp_1ReadVariableOp_1:j f
B
_output_shapes0
.:,����������������������������
 
_user_specified_nameinputs
�
�
E__inference_Block2_Norm_layer_call_and_return_conditional_losses_7342

inputs%
readvariableop_resource:@'
readvariableop_1_resource:@6
(fusedbatchnormv3_readvariableop_resource:@8
*fusedbatchnormv3_readvariableop_1_resource:@
identity��FusedBatchNormV3/ReadVariableOp�!FusedBatchNormV3/ReadVariableOp_1�ReadVariableOp�ReadVariableOp_1b
ReadVariableOpReadVariableOpreadvariableop_resource*
_output_shapes
:@*
dtype0f
ReadVariableOp_1ReadVariableOpreadvariableop_1_resource*
_output_shapes
:@*
dtype0�
FusedBatchNormV3/ReadVariableOpReadVariableOp(fusedbatchnormv3_readvariableop_resource*
_output_shapes
:@*
dtype0�
!FusedBatchNormV3/ReadVariableOp_1ReadVariableOp*fusedbatchnormv3_readvariableop_1_resource*
_output_shapes
:@*
dtype0�
FusedBatchNormV3FusedBatchNormV3inputsReadVariableOp:value:0ReadVariableOp_1:value:0'FusedBatchNormV3/ReadVariableOp:value:0)FusedBatchNormV3/ReadVariableOp_1:value:0*
T0*
U0*]
_output_shapesK
I:+���������������������������@:@:@:@:@:*
epsilon%o�:*
is_training( }
IdentityIdentityFusedBatchNormV3:y:0^NoOp*
T0*A
_output_shapes/
-:+���������������������������@�
NoOpNoOp ^FusedBatchNormV3/ReadVariableOp"^FusedBatchNormV3/ReadVariableOp_1^ReadVariableOp^ReadVariableOp_1*"
_acd_function_control_output(*
_output_shapes
 "
identityIdentity:output:0*(
_construction_contextkEagerRuntime*H
_input_shapes7
5:+���������������������������@: : : : 2B
FusedBatchNormV3/ReadVariableOpFusedBatchNormV3/ReadVariableOp2F
!FusedBatchNormV3/ReadVariableOp_1!FusedBatchNormV3/ReadVariableOp_12 
ReadVariableOpReadVariableOp2$
ReadVariableOp_1ReadVariableOp_1:i e
A
_output_shapes/
-:+���������������������������@
 
_user_specified_nameinputs
�	
�
*__inference_Block3_Norm_layer_call_fn_8890

inputs
unknown:	�
	unknown_0:	�
	unknown_1:	�
	unknown_2:	�
identity��StatefulPartitionedCall�
StatefulPartitionedCallStatefulPartitionedCallinputsunknown	unknown_0	unknown_1	unknown_2*
Tin	
2*
Tout
2*
_collective_manager_ids
 *B
_output_shapes0
.:,����������������������������*$
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8� *N
fIRG
E__inference_Block3_Norm_layer_call_and_return_conditional_losses_7449�
IdentityIdentity StatefulPartitionedCall:output:0^NoOp*
T0*B
_output_shapes0
.:,����������������������������`
NoOpNoOp^StatefulPartitionedCall*"
_acd_function_control_output(*
_output_shapes
 "
identityIdentity:output:0*(
_construction_contextkEagerRuntime*I
_input_shapes8
6:,����������������������������: : : : 22
StatefulPartitionedCallStatefulPartitionedCall:j f
B
_output_shapes0
.:,����������������������������
 
_user_specified_nameinputs
�

�
E__inference_Final_Dense_layer_call_and_return_conditional_losses_7600

inputs2
matmul_readvariableop_resource:
��.
biasadd_readvariableop_resource:	�
identity��BiasAdd/ReadVariableOp�MatMul/ReadVariableOpv
MatMul/ReadVariableOpReadVariableOpmatmul_readvariableop_resource* 
_output_shapes
:
��*
dtype0j
MatMulMatMulinputsMatMul/ReadVariableOp:value:0*
T0*(
_output_shapes
:����������s
BiasAdd/ReadVariableOpReadVariableOpbiasadd_readvariableop_resource*
_output_shapes	
:�*
dtype0w
BiasAddBiasAddMatMul:product:0BiasAdd/ReadVariableOp:value:0*
T0*(
_output_shapes
:����������Q
ReluReluBiasAdd:output:0*
T0*(
_output_shapes
:����������b
IdentityIdentityRelu:activations:0^NoOp*
T0*(
_output_shapes
:����������w
NoOpNoOp^BiasAdd/ReadVariableOp^MatMul/ReadVariableOp*"
_acd_function_control_output(*
_output_shapes
 "
identityIdentity:output:0*(
_construction_contextkEagerRuntime*+
_input_shapes
:����������: : 20
BiasAdd/ReadVariableOpBiasAdd/ReadVariableOp2.
MatMul/ReadVariableOpMatMul/ReadVariableOp:P L
(
_output_shapes
:����������
 
_user_specified_nameinputs
�
�
*__inference_Block1_Conv_layer_call_fn_8659

inputs!
unknown: 
	unknown_0: 
identity��StatefulPartitionedCall�
StatefulPartitionedCallStatefulPartitionedCallinputsunknown	unknown_0*
Tin
2*
Tout
2*
_collective_manager_ids
 */
_output_shapes
:���������@V *$
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8� *N
fIRG
E__inference_Block1_Conv_layer_call_and_return_conditional_losses_7518w
IdentityIdentity StatefulPartitionedCall:output:0^NoOp*
T0*/
_output_shapes
:���������@V `
NoOpNoOp^StatefulPartitionedCall*"
_acd_function_control_output(*
_output_shapes
 "
identityIdentity:output:0*(
_construction_contextkEagerRuntime*2
_input_shapes!
:���������@V: : 22
StatefulPartitionedCallStatefulPartitionedCall:W S
/
_output_shapes
:���������@V
 
_user_specified_nameinputs
�
a
E__inference_Block2_Pool_layer_call_and_return_conditional_losses_8772

inputs
identity�
MaxPoolMaxPoolinputs*J
_output_shapes8
6:4������������������������������������*
ksize
*
paddingVALID*
strides
{
IdentityIdentityMaxPool:output:0*
T0*J
_output_shapes8
6:4������������������������������������"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*I
_input_shapes8
6:4������������������������������������:r n
J
_output_shapes8
6:4������������������������������������
 
_user_specified_nameinputs
�
�
E__inference_Block2_Norm_layer_call_and_return_conditional_losses_8834

inputs%
readvariableop_resource:@'
readvariableop_1_resource:@6
(fusedbatchnormv3_readvariableop_resource:@8
*fusedbatchnormv3_readvariableop_1_resource:@
identity��AssignNewValue�AssignNewValue_1�FusedBatchNormV3/ReadVariableOp�!FusedBatchNormV3/ReadVariableOp_1�ReadVariableOp�ReadVariableOp_1b
ReadVariableOpReadVariableOpreadvariableop_resource*
_output_shapes
:@*
dtype0f
ReadVariableOp_1ReadVariableOpreadvariableop_1_resource*
_output_shapes
:@*
dtype0�
FusedBatchNormV3/ReadVariableOpReadVariableOp(fusedbatchnormv3_readvariableop_resource*
_output_shapes
:@*
dtype0�
!FusedBatchNormV3/ReadVariableOp_1ReadVariableOp*fusedbatchnormv3_readvariableop_1_resource*
_output_shapes
:@*
dtype0�
FusedBatchNormV3FusedBatchNormV3inputsReadVariableOp:value:0ReadVariableOp_1:value:0'FusedBatchNormV3/ReadVariableOp:value:0)FusedBatchNormV3/ReadVariableOp_1:value:0*
T0*
U0*]
_output_shapesK
I:+���������������������������@:@:@:@:@:*
epsilon%o�:*
exponential_avg_factor%
�#<�
AssignNewValueAssignVariableOp(fusedbatchnormv3_readvariableop_resourceFusedBatchNormV3:batch_mean:0 ^FusedBatchNormV3/ReadVariableOp*
_output_shapes
 *
dtype0�
AssignNewValue_1AssignVariableOp*fusedbatchnormv3_readvariableop_1_resource!FusedBatchNormV3:batch_variance:0"^FusedBatchNormV3/ReadVariableOp_1*
_output_shapes
 *
dtype0}
IdentityIdentityFusedBatchNormV3:y:0^NoOp*
T0*A
_output_shapes/
-:+���������������������������@�
NoOpNoOp^AssignNewValue^AssignNewValue_1 ^FusedBatchNormV3/ReadVariableOp"^FusedBatchNormV3/ReadVariableOp_1^ReadVariableOp^ReadVariableOp_1*"
_acd_function_control_output(*
_output_shapes
 "
identityIdentity:output:0*(
_construction_contextkEagerRuntime*H
_input_shapes7
5:+���������������������������@: : : : 2 
AssignNewValueAssignNewValue2$
AssignNewValue_1AssignNewValue_12B
FusedBatchNormV3/ReadVariableOpFusedBatchNormV3/ReadVariableOp2F
!FusedBatchNormV3/ReadVariableOp_1!FusedBatchNormV3/ReadVariableOp_12 
ReadVariableOpReadVariableOp2$
ReadVariableOp_1ReadVariableOp_1:i e
A
_output_shapes/
-:+���������������������������@
 
_user_specified_nameinputs
ߔ
� 
__inference__traced_save_9231
file_prefix1
-savev2_block0_conv_kernel_read_readvariableop/
+savev2_block0_conv_bias_read_readvariableop0
,savev2_block0_norm_gamma_read_readvariableop/
+savev2_block0_norm_beta_read_readvariableop6
2savev2_block0_norm_moving_mean_read_readvariableop:
6savev2_block0_norm_moving_variance_read_readvariableop1
-savev2_block1_conv_kernel_read_readvariableop/
+savev2_block1_conv_bias_read_readvariableop0
,savev2_block1_norm_gamma_read_readvariableop/
+savev2_block1_norm_beta_read_readvariableop6
2savev2_block1_norm_moving_mean_read_readvariableop:
6savev2_block1_norm_moving_variance_read_readvariableop1
-savev2_block2_conv_kernel_read_readvariableop/
+savev2_block2_conv_bias_read_readvariableop0
,savev2_block2_norm_gamma_read_readvariableop/
+savev2_block2_norm_beta_read_readvariableop6
2savev2_block2_norm_moving_mean_read_readvariableop:
6savev2_block2_norm_moving_variance_read_readvariableop1
-savev2_block3_conv_kernel_read_readvariableop/
+savev2_block3_conv_bias_read_readvariableop0
,savev2_block3_norm_gamma_read_readvariableop/
+savev2_block3_norm_beta_read_readvariableop6
2savev2_block3_norm_moving_mean_read_readvariableop:
6savev2_block3_norm_moving_variance_read_readvariableop1
-savev2_final_dense_kernel_read_readvariableop/
+savev2_final_dense_bias_read_readvariableop,
(savev2_output_kernel_read_readvariableop*
&savev2_output_bias_read_readvariableop(
$savev2_adam_iter_read_readvariableop	*
&savev2_adam_beta_1_read_readvariableop*
&savev2_adam_beta_2_read_readvariableop)
%savev2_adam_decay_read_readvariableop1
-savev2_adam_learning_rate_read_readvariableop$
 savev2_total_read_readvariableop$
 savev2_count_read_readvariableop&
"savev2_total_1_read_readvariableop&
"savev2_count_1_read_readvariableop8
4savev2_adam_block0_conv_kernel_m_read_readvariableop6
2savev2_adam_block0_conv_bias_m_read_readvariableop7
3savev2_adam_block0_norm_gamma_m_read_readvariableop6
2savev2_adam_block0_norm_beta_m_read_readvariableop8
4savev2_adam_block1_conv_kernel_m_read_readvariableop6
2savev2_adam_block1_conv_bias_m_read_readvariableop7
3savev2_adam_block1_norm_gamma_m_read_readvariableop6
2savev2_adam_block1_norm_beta_m_read_readvariableop8
4savev2_adam_block2_conv_kernel_m_read_readvariableop6
2savev2_adam_block2_conv_bias_m_read_readvariableop7
3savev2_adam_block2_norm_gamma_m_read_readvariableop6
2savev2_adam_block2_norm_beta_m_read_readvariableop8
4savev2_adam_block3_conv_kernel_m_read_readvariableop6
2savev2_adam_block3_conv_bias_m_read_readvariableop7
3savev2_adam_block3_norm_gamma_m_read_readvariableop6
2savev2_adam_block3_norm_beta_m_read_readvariableop8
4savev2_adam_final_dense_kernel_m_read_readvariableop6
2savev2_adam_final_dense_bias_m_read_readvariableop3
/savev2_adam_output_kernel_m_read_readvariableop1
-savev2_adam_output_bias_m_read_readvariableop8
4savev2_adam_block0_conv_kernel_v_read_readvariableop6
2savev2_adam_block0_conv_bias_v_read_readvariableop7
3savev2_adam_block0_norm_gamma_v_read_readvariableop6
2savev2_adam_block0_norm_beta_v_read_readvariableop8
4savev2_adam_block1_conv_kernel_v_read_readvariableop6
2savev2_adam_block1_conv_bias_v_read_readvariableop7
3savev2_adam_block1_norm_gamma_v_read_readvariableop6
2savev2_adam_block1_norm_beta_v_read_readvariableop8
4savev2_adam_block2_conv_kernel_v_read_readvariableop6
2savev2_adam_block2_conv_bias_v_read_readvariableop7
3savev2_adam_block2_norm_gamma_v_read_readvariableop6
2savev2_adam_block2_norm_beta_v_read_readvariableop8
4savev2_adam_block3_conv_kernel_v_read_readvariableop6
2savev2_adam_block3_conv_bias_v_read_readvariableop7
3savev2_adam_block3_norm_gamma_v_read_readvariableop6
2savev2_adam_block3_norm_beta_v_read_readvariableop8
4savev2_adam_final_dense_kernel_v_read_readvariableop6
2savev2_adam_final_dense_bias_v_read_readvariableop3
/savev2_adam_output_kernel_v_read_readvariableop1
-savev2_adam_output_bias_v_read_readvariableop
savev2_const

identity_1��MergeV2Checkpointsw
StaticRegexFullMatchStaticRegexFullMatchfile_prefix"/device:CPU:**
_output_shapes
: *
pattern
^s3://.*Z
ConstConst"/device:CPU:**
_output_shapes
: *
dtype0*
valueB B.parta
Const_1Const"/device:CPU:**
_output_shapes
: *
dtype0*
valueB B
_temp/part�
SelectSelectStaticRegexFullMatch:output:0Const:output:0Const_1:output:0"/device:CPU:**
T0*
_output_shapes
: f

StringJoin
StringJoinfile_prefixSelect:output:0"/device:CPU:**
N*
_output_shapes
: L

num_shardsConst*
_output_shapes
: *
dtype0*
value	B :f
ShardedFilename/shardConst"/device:CPU:0*
_output_shapes
: *
dtype0*
value	B : �
ShardedFilenameShardedFilenameStringJoin:output:0ShardedFilename/shard:output:0num_shards:output:0"/device:CPU:0*
_output_shapes
: �*
SaveV2/tensor_namesConst"/device:CPU:0*
_output_shapes
:N*
dtype0*�*
value�*B�*NB6layer_with_weights-0/kernel/.ATTRIBUTES/VARIABLE_VALUEB4layer_with_weights-0/bias/.ATTRIBUTES/VARIABLE_VALUEB5layer_with_weights-1/gamma/.ATTRIBUTES/VARIABLE_VALUEB4layer_with_weights-1/beta/.ATTRIBUTES/VARIABLE_VALUEB;layer_with_weights-1/moving_mean/.ATTRIBUTES/VARIABLE_VALUEB?layer_with_weights-1/moving_variance/.ATTRIBUTES/VARIABLE_VALUEB6layer_with_weights-2/kernel/.ATTRIBUTES/VARIABLE_VALUEB4layer_with_weights-2/bias/.ATTRIBUTES/VARIABLE_VALUEB5layer_with_weights-3/gamma/.ATTRIBUTES/VARIABLE_VALUEB4layer_with_weights-3/beta/.ATTRIBUTES/VARIABLE_VALUEB;layer_with_weights-3/moving_mean/.ATTRIBUTES/VARIABLE_VALUEB?layer_with_weights-3/moving_variance/.ATTRIBUTES/VARIABLE_VALUEB6layer_with_weights-4/kernel/.ATTRIBUTES/VARIABLE_VALUEB4layer_with_weights-4/bias/.ATTRIBUTES/VARIABLE_VALUEB5layer_with_weights-5/gamma/.ATTRIBUTES/VARIABLE_VALUEB4layer_with_weights-5/beta/.ATTRIBUTES/VARIABLE_VALUEB;layer_with_weights-5/moving_mean/.ATTRIBUTES/VARIABLE_VALUEB?layer_with_weights-5/moving_variance/.ATTRIBUTES/VARIABLE_VALUEB6layer_with_weights-6/kernel/.ATTRIBUTES/VARIABLE_VALUEB4layer_with_weights-6/bias/.ATTRIBUTES/VARIABLE_VALUEB5layer_with_weights-7/gamma/.ATTRIBUTES/VARIABLE_VALUEB4layer_with_weights-7/beta/.ATTRIBUTES/VARIABLE_VALUEB;layer_with_weights-7/moving_mean/.ATTRIBUTES/VARIABLE_VALUEB?layer_with_weights-7/moving_variance/.ATTRIBUTES/VARIABLE_VALUEB6layer_with_weights-8/kernel/.ATTRIBUTES/VARIABLE_VALUEB4layer_with_weights-8/bias/.ATTRIBUTES/VARIABLE_VALUEB6layer_with_weights-9/kernel/.ATTRIBUTES/VARIABLE_VALUEB4layer_with_weights-9/bias/.ATTRIBUTES/VARIABLE_VALUEB)optimizer/iter/.ATTRIBUTES/VARIABLE_VALUEB+optimizer/beta_1/.ATTRIBUTES/VARIABLE_VALUEB+optimizer/beta_2/.ATTRIBUTES/VARIABLE_VALUEB*optimizer/decay/.ATTRIBUTES/VARIABLE_VALUEB2optimizer/learning_rate/.ATTRIBUTES/VARIABLE_VALUEB4keras_api/metrics/0/total/.ATTRIBUTES/VARIABLE_VALUEB4keras_api/metrics/0/count/.ATTRIBUTES/VARIABLE_VALUEB4keras_api/metrics/1/total/.ATTRIBUTES/VARIABLE_VALUEB4keras_api/metrics/1/count/.ATTRIBUTES/VARIABLE_VALUEBRlayer_with_weights-0/kernel/.OPTIMIZER_SLOT/optimizer/m/.ATTRIBUTES/VARIABLE_VALUEBPlayer_with_weights-0/bias/.OPTIMIZER_SLOT/optimizer/m/.ATTRIBUTES/VARIABLE_VALUEBQlayer_with_weights-1/gamma/.OPTIMIZER_SLOT/optimizer/m/.ATTRIBUTES/VARIABLE_VALUEBPlayer_with_weights-1/beta/.OPTIMIZER_SLOT/optimizer/m/.ATTRIBUTES/VARIABLE_VALUEBRlayer_with_weights-2/kernel/.OPTIMIZER_SLOT/optimizer/m/.ATTRIBUTES/VARIABLE_VALUEBPlayer_with_weights-2/bias/.OPTIMIZER_SLOT/optimizer/m/.ATTRIBUTES/VARIABLE_VALUEBQlayer_with_weights-3/gamma/.OPTIMIZER_SLOT/optimizer/m/.ATTRIBUTES/VARIABLE_VALUEBPlayer_with_weights-3/beta/.OPTIMIZER_SLOT/optimizer/m/.ATTRIBUTES/VARIABLE_VALUEBRlayer_with_weights-4/kernel/.OPTIMIZER_SLOT/optimizer/m/.ATTRIBUTES/VARIABLE_VALUEBPlayer_with_weights-4/bias/.OPTIMIZER_SLOT/optimizer/m/.ATTRIBUTES/VARIABLE_VALUEBQlayer_with_weights-5/gamma/.OPTIMIZER_SLOT/optimizer/m/.ATTRIBUTES/VARIABLE_VALUEBPlayer_with_weights-5/beta/.OPTIMIZER_SLOT/optimizer/m/.ATTRIBUTES/VARIABLE_VALUEBRlayer_with_weights-6/kernel/.OPTIMIZER_SLOT/optimizer/m/.ATTRIBUTES/VARIABLE_VALUEBPlayer_with_weights-6/bias/.OPTIMIZER_SLOT/optimizer/m/.ATTRIBUTES/VARIABLE_VALUEBQlayer_with_weights-7/gamma/.OPTIMIZER_SLOT/optimizer/m/.ATTRIBUTES/VARIABLE_VALUEBPlayer_with_weights-7/beta/.OPTIMIZER_SLOT/optimizer/m/.ATTRIBUTES/VARIABLE_VALUEBRlayer_with_weights-8/kernel/.OPTIMIZER_SLOT/optimizer/m/.ATTRIBUTES/VARIABLE_VALUEBPlayer_with_weights-8/bias/.OPTIMIZER_SLOT/optimizer/m/.ATTRIBUTES/VARIABLE_VALUEBRlayer_with_weights-9/kernel/.OPTIMIZER_SLOT/optimizer/m/.ATTRIBUTES/VARIABLE_VALUEBPlayer_with_weights-9/bias/.OPTIMIZER_SLOT/optimizer/m/.ATTRIBUTES/VARIABLE_VALUEBRlayer_with_weights-0/kernel/.OPTIMIZER_SLOT/optimizer/v/.ATTRIBUTES/VARIABLE_VALUEBPlayer_with_weights-0/bias/.OPTIMIZER_SLOT/optimizer/v/.ATTRIBUTES/VARIABLE_VALUEBQlayer_with_weights-1/gamma/.OPTIMIZER_SLOT/optimizer/v/.ATTRIBUTES/VARIABLE_VALUEBPlayer_with_weights-1/beta/.OPTIMIZER_SLOT/optimizer/v/.ATTRIBUTES/VARIABLE_VALUEBRlayer_with_weights-2/kernel/.OPTIMIZER_SLOT/optimizer/v/.ATTRIBUTES/VARIABLE_VALUEBPlayer_with_weights-2/bias/.OPTIMIZER_SLOT/optimizer/v/.ATTRIBUTES/VARIABLE_VALUEBQlayer_with_weights-3/gamma/.OPTIMIZER_SLOT/optimizer/v/.ATTRIBUTES/VARIABLE_VALUEBPlayer_with_weights-3/beta/.OPTIMIZER_SLOT/optimizer/v/.ATTRIBUTES/VARIABLE_VALUEBRlayer_with_weights-4/kernel/.OPTIMIZER_SLOT/optimizer/v/.ATTRIBUTES/VARIABLE_VALUEBPlayer_with_weights-4/bias/.OPTIMIZER_SLOT/optimizer/v/.ATTRIBUTES/VARIABLE_VALUEBQlayer_with_weights-5/gamma/.OPTIMIZER_SLOT/optimizer/v/.ATTRIBUTES/VARIABLE_VALUEBPlayer_with_weights-5/beta/.OPTIMIZER_SLOT/optimizer/v/.ATTRIBUTES/VARIABLE_VALUEBRlayer_with_weights-6/kernel/.OPTIMIZER_SLOT/optimizer/v/.ATTRIBUTES/VARIABLE_VALUEBPlayer_with_weights-6/bias/.OPTIMIZER_SLOT/optimizer/v/.ATTRIBUTES/VARIABLE_VALUEBQlayer_with_weights-7/gamma/.OPTIMIZER_SLOT/optimizer/v/.ATTRIBUTES/VARIABLE_VALUEBPlayer_with_weights-7/beta/.OPTIMIZER_SLOT/optimizer/v/.ATTRIBUTES/VARIABLE_VALUEBRlayer_with_weights-8/kernel/.OPTIMIZER_SLOT/optimizer/v/.ATTRIBUTES/VARIABLE_VALUEBPlayer_with_weights-8/bias/.OPTIMIZER_SLOT/optimizer/v/.ATTRIBUTES/VARIABLE_VALUEBRlayer_with_weights-9/kernel/.OPTIMIZER_SLOT/optimizer/v/.ATTRIBUTES/VARIABLE_VALUEBPlayer_with_weights-9/bias/.OPTIMIZER_SLOT/optimizer/v/.ATTRIBUTES/VARIABLE_VALUEB_CHECKPOINTABLE_OBJECT_GRAPH�
SaveV2/shape_and_slicesConst"/device:CPU:0*
_output_shapes
:N*
dtype0*�
value�B�NB B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B �
SaveV2SaveV2ShardedFilename:filename:0SaveV2/tensor_names:output:0 SaveV2/shape_and_slices:output:0-savev2_block0_conv_kernel_read_readvariableop+savev2_block0_conv_bias_read_readvariableop,savev2_block0_norm_gamma_read_readvariableop+savev2_block0_norm_beta_read_readvariableop2savev2_block0_norm_moving_mean_read_readvariableop6savev2_block0_norm_moving_variance_read_readvariableop-savev2_block1_conv_kernel_read_readvariableop+savev2_block1_conv_bias_read_readvariableop,savev2_block1_norm_gamma_read_readvariableop+savev2_block1_norm_beta_read_readvariableop2savev2_block1_norm_moving_mean_read_readvariableop6savev2_block1_norm_moving_variance_read_readvariableop-savev2_block2_conv_kernel_read_readvariableop+savev2_block2_conv_bias_read_readvariableop,savev2_block2_norm_gamma_read_readvariableop+savev2_block2_norm_beta_read_readvariableop2savev2_block2_norm_moving_mean_read_readvariableop6savev2_block2_norm_moving_variance_read_readvariableop-savev2_block3_conv_kernel_read_readvariableop+savev2_block3_conv_bias_read_readvariableop,savev2_block3_norm_gamma_read_readvariableop+savev2_block3_norm_beta_read_readvariableop2savev2_block3_norm_moving_mean_read_readvariableop6savev2_block3_norm_moving_variance_read_readvariableop-savev2_final_dense_kernel_read_readvariableop+savev2_final_dense_bias_read_readvariableop(savev2_output_kernel_read_readvariableop&savev2_output_bias_read_readvariableop$savev2_adam_iter_read_readvariableop&savev2_adam_beta_1_read_readvariableop&savev2_adam_beta_2_read_readvariableop%savev2_adam_decay_read_readvariableop-savev2_adam_learning_rate_read_readvariableop savev2_total_read_readvariableop savev2_count_read_readvariableop"savev2_total_1_read_readvariableop"savev2_count_1_read_readvariableop4savev2_adam_block0_conv_kernel_m_read_readvariableop2savev2_adam_block0_conv_bias_m_read_readvariableop3savev2_adam_block0_norm_gamma_m_read_readvariableop2savev2_adam_block0_norm_beta_m_read_readvariableop4savev2_adam_block1_conv_kernel_m_read_readvariableop2savev2_adam_block1_conv_bias_m_read_readvariableop3savev2_adam_block1_norm_gamma_m_read_readvariableop2savev2_adam_block1_norm_beta_m_read_readvariableop4savev2_adam_block2_conv_kernel_m_read_readvariableop2savev2_adam_block2_conv_bias_m_read_readvariableop3savev2_adam_block2_norm_gamma_m_read_readvariableop2savev2_adam_block2_norm_beta_m_read_readvariableop4savev2_adam_block3_conv_kernel_m_read_readvariableop2savev2_adam_block3_conv_bias_m_read_readvariableop3savev2_adam_block3_norm_gamma_m_read_readvariableop2savev2_adam_block3_norm_beta_m_read_readvariableop4savev2_adam_final_dense_kernel_m_read_readvariableop2savev2_adam_final_dense_bias_m_read_readvariableop/savev2_adam_output_kernel_m_read_readvariableop-savev2_adam_output_bias_m_read_readvariableop4savev2_adam_block0_conv_kernel_v_read_readvariableop2savev2_adam_block0_conv_bias_v_read_readvariableop3savev2_adam_block0_norm_gamma_v_read_readvariableop2savev2_adam_block0_norm_beta_v_read_readvariableop4savev2_adam_block1_conv_kernel_v_read_readvariableop2savev2_adam_block1_conv_bias_v_read_readvariableop3savev2_adam_block1_norm_gamma_v_read_readvariableop2savev2_adam_block1_norm_beta_v_read_readvariableop4savev2_adam_block2_conv_kernel_v_read_readvariableop2savev2_adam_block2_conv_bias_v_read_readvariableop3savev2_adam_block2_norm_gamma_v_read_readvariableop2savev2_adam_block2_norm_beta_v_read_readvariableop4savev2_adam_block3_conv_kernel_v_read_readvariableop2savev2_adam_block3_conv_bias_v_read_readvariableop3savev2_adam_block3_norm_gamma_v_read_readvariableop2savev2_adam_block3_norm_beta_v_read_readvariableop4savev2_adam_final_dense_kernel_v_read_readvariableop2savev2_adam_final_dense_bias_v_read_readvariableop/savev2_adam_output_kernel_v_read_readvariableop-savev2_adam_output_bias_v_read_readvariableopsavev2_const"/device:CPU:0*
_output_shapes
 *\
dtypesR
P2N	�
&MergeV2Checkpoints/checkpoint_prefixesPackShardedFilename:filename:0^SaveV2"/device:CPU:0*
N*
T0*
_output_shapes
:�
MergeV2CheckpointsMergeV2Checkpoints/MergeV2Checkpoints/checkpoint_prefixes:output:0file_prefix"/device:CPU:0*
_output_shapes
 f
IdentityIdentityfile_prefix^MergeV2Checkpoints"/device:CPU:0*
T0*
_output_shapes
: Q

Identity_1IdentityIdentity:output:0^NoOp*
T0*
_output_shapes
: [
NoOpNoOp^MergeV2Checkpoints*"
_acd_function_control_output(*
_output_shapes
 "!

identity_1Identity_1:output:0*�
_input_shapes�
�: ::::::: : : : : : : @:@:@:@:@:@:@�:�:�:�:�:�:
��:�:	�:: : : : : : : : : ::::: : : : : @:@:@:@:@�:�:�:�:
��:�:	�:::::: : : : : @:@:@:@:@�:�:�:�:
��:�:	�:: 2(
MergeV2CheckpointsMergeV2Checkpoints:C ?

_output_shapes
: 
%
_user_specified_namefile_prefix:,(
&
_output_shapes
:: 

_output_shapes
:: 

_output_shapes
:: 

_output_shapes
:: 

_output_shapes
:: 

_output_shapes
::,(
&
_output_shapes
: : 

_output_shapes
: : 	

_output_shapes
: : 


_output_shapes
: : 

_output_shapes
: : 

_output_shapes
: :,(
&
_output_shapes
: @: 

_output_shapes
:@: 

_output_shapes
:@: 

_output_shapes
:@: 

_output_shapes
:@: 

_output_shapes
:@:-)
'
_output_shapes
:@�:!

_output_shapes	
:�:!

_output_shapes	
:�:!

_output_shapes	
:�:!

_output_shapes	
:�:!

_output_shapes	
:�:&"
 
_output_shapes
:
��:!

_output_shapes	
:�:%!

_output_shapes
:	�: 

_output_shapes
::

_output_shapes
: :

_output_shapes
: :

_output_shapes
: : 

_output_shapes
: :!

_output_shapes
: :"

_output_shapes
: :#

_output_shapes
: :$

_output_shapes
: :%

_output_shapes
: :,&(
&
_output_shapes
:: '

_output_shapes
:: (

_output_shapes
:: )

_output_shapes
::,*(
&
_output_shapes
: : +

_output_shapes
: : ,

_output_shapes
: : -

_output_shapes
: :,.(
&
_output_shapes
: @: /

_output_shapes
:@: 0

_output_shapes
:@: 1

_output_shapes
:@:-2)
'
_output_shapes
:@�:!3

_output_shapes	
:�:!4

_output_shapes	
:�:!5

_output_shapes	
:�:&6"
 
_output_shapes
:
��:!7

_output_shapes	
:�:%8!

_output_shapes
:	�: 9

_output_shapes
::,:(
&
_output_shapes
:: ;

_output_shapes
:: <

_output_shapes
:: =

_output_shapes
::,>(
&
_output_shapes
: : ?

_output_shapes
: : @

_output_shapes
: : A

_output_shapes
: :,B(
&
_output_shapes
: @: C

_output_shapes
:@: D

_output_shapes
:@: E

_output_shapes
:@:-F)
'
_output_shapes
:@�:!G

_output_shapes	
:�:!H

_output_shapes	
:�:!I

_output_shapes	
:�:&J"
 
_output_shapes
:
��:!K

_output_shapes	
:�:%L!

_output_shapes
:	�: M

_output_shapes
::N

_output_shapes
: 
��
�0
 __inference__traced_restore_9472
file_prefix=
#assignvariableop_block0_conv_kernel:1
#assignvariableop_1_block0_conv_bias:2
$assignvariableop_2_block0_norm_gamma:1
#assignvariableop_3_block0_norm_beta:8
*assignvariableop_4_block0_norm_moving_mean:<
.assignvariableop_5_block0_norm_moving_variance:?
%assignvariableop_6_block1_conv_kernel: 1
#assignvariableop_7_block1_conv_bias: 2
$assignvariableop_8_block1_norm_gamma: 1
#assignvariableop_9_block1_norm_beta: 9
+assignvariableop_10_block1_norm_moving_mean: =
/assignvariableop_11_block1_norm_moving_variance: @
&assignvariableop_12_block2_conv_kernel: @2
$assignvariableop_13_block2_conv_bias:@3
%assignvariableop_14_block2_norm_gamma:@2
$assignvariableop_15_block2_norm_beta:@9
+assignvariableop_16_block2_norm_moving_mean:@=
/assignvariableop_17_block2_norm_moving_variance:@A
&assignvariableop_18_block3_conv_kernel:@�3
$assignvariableop_19_block3_conv_bias:	�4
%assignvariableop_20_block3_norm_gamma:	�3
$assignvariableop_21_block3_norm_beta:	�:
+assignvariableop_22_block3_norm_moving_mean:	�>
/assignvariableop_23_block3_norm_moving_variance:	�:
&assignvariableop_24_final_dense_kernel:
��3
$assignvariableop_25_final_dense_bias:	�4
!assignvariableop_26_output_kernel:	�-
assignvariableop_27_output_bias:'
assignvariableop_28_adam_iter:	 )
assignvariableop_29_adam_beta_1: )
assignvariableop_30_adam_beta_2: (
assignvariableop_31_adam_decay: 0
&assignvariableop_32_adam_learning_rate: #
assignvariableop_33_total: #
assignvariableop_34_count: %
assignvariableop_35_total_1: %
assignvariableop_36_count_1: G
-assignvariableop_37_adam_block0_conv_kernel_m:9
+assignvariableop_38_adam_block0_conv_bias_m::
,assignvariableop_39_adam_block0_norm_gamma_m:9
+assignvariableop_40_adam_block0_norm_beta_m:G
-assignvariableop_41_adam_block1_conv_kernel_m: 9
+assignvariableop_42_adam_block1_conv_bias_m: :
,assignvariableop_43_adam_block1_norm_gamma_m: 9
+assignvariableop_44_adam_block1_norm_beta_m: G
-assignvariableop_45_adam_block2_conv_kernel_m: @9
+assignvariableop_46_adam_block2_conv_bias_m:@:
,assignvariableop_47_adam_block2_norm_gamma_m:@9
+assignvariableop_48_adam_block2_norm_beta_m:@H
-assignvariableop_49_adam_block3_conv_kernel_m:@�:
+assignvariableop_50_adam_block3_conv_bias_m:	�;
,assignvariableop_51_adam_block3_norm_gamma_m:	�:
+assignvariableop_52_adam_block3_norm_beta_m:	�A
-assignvariableop_53_adam_final_dense_kernel_m:
��:
+assignvariableop_54_adam_final_dense_bias_m:	�;
(assignvariableop_55_adam_output_kernel_m:	�4
&assignvariableop_56_adam_output_bias_m:G
-assignvariableop_57_adam_block0_conv_kernel_v:9
+assignvariableop_58_adam_block0_conv_bias_v::
,assignvariableop_59_adam_block0_norm_gamma_v:9
+assignvariableop_60_adam_block0_norm_beta_v:G
-assignvariableop_61_adam_block1_conv_kernel_v: 9
+assignvariableop_62_adam_block1_conv_bias_v: :
,assignvariableop_63_adam_block1_norm_gamma_v: 9
+assignvariableop_64_adam_block1_norm_beta_v: G
-assignvariableop_65_adam_block2_conv_kernel_v: @9
+assignvariableop_66_adam_block2_conv_bias_v:@:
,assignvariableop_67_adam_block2_norm_gamma_v:@9
+assignvariableop_68_adam_block2_norm_beta_v:@H
-assignvariableop_69_adam_block3_conv_kernel_v:@�:
+assignvariableop_70_adam_block3_conv_bias_v:	�;
,assignvariableop_71_adam_block3_norm_gamma_v:	�:
+assignvariableop_72_adam_block3_norm_beta_v:	�A
-assignvariableop_73_adam_final_dense_kernel_v:
��:
+assignvariableop_74_adam_final_dense_bias_v:	�;
(assignvariableop_75_adam_output_kernel_v:	�4
&assignvariableop_76_adam_output_bias_v:
identity_78��AssignVariableOp�AssignVariableOp_1�AssignVariableOp_10�AssignVariableOp_11�AssignVariableOp_12�AssignVariableOp_13�AssignVariableOp_14�AssignVariableOp_15�AssignVariableOp_16�AssignVariableOp_17�AssignVariableOp_18�AssignVariableOp_19�AssignVariableOp_2�AssignVariableOp_20�AssignVariableOp_21�AssignVariableOp_22�AssignVariableOp_23�AssignVariableOp_24�AssignVariableOp_25�AssignVariableOp_26�AssignVariableOp_27�AssignVariableOp_28�AssignVariableOp_29�AssignVariableOp_3�AssignVariableOp_30�AssignVariableOp_31�AssignVariableOp_32�AssignVariableOp_33�AssignVariableOp_34�AssignVariableOp_35�AssignVariableOp_36�AssignVariableOp_37�AssignVariableOp_38�AssignVariableOp_39�AssignVariableOp_4�AssignVariableOp_40�AssignVariableOp_41�AssignVariableOp_42�AssignVariableOp_43�AssignVariableOp_44�AssignVariableOp_45�AssignVariableOp_46�AssignVariableOp_47�AssignVariableOp_48�AssignVariableOp_49�AssignVariableOp_5�AssignVariableOp_50�AssignVariableOp_51�AssignVariableOp_52�AssignVariableOp_53�AssignVariableOp_54�AssignVariableOp_55�AssignVariableOp_56�AssignVariableOp_57�AssignVariableOp_58�AssignVariableOp_59�AssignVariableOp_6�AssignVariableOp_60�AssignVariableOp_61�AssignVariableOp_62�AssignVariableOp_63�AssignVariableOp_64�AssignVariableOp_65�AssignVariableOp_66�AssignVariableOp_67�AssignVariableOp_68�AssignVariableOp_69�AssignVariableOp_7�AssignVariableOp_70�AssignVariableOp_71�AssignVariableOp_72�AssignVariableOp_73�AssignVariableOp_74�AssignVariableOp_75�AssignVariableOp_76�AssignVariableOp_8�AssignVariableOp_9�*
RestoreV2/tensor_namesConst"/device:CPU:0*
_output_shapes
:N*
dtype0*�*
value�*B�*NB6layer_with_weights-0/kernel/.ATTRIBUTES/VARIABLE_VALUEB4layer_with_weights-0/bias/.ATTRIBUTES/VARIABLE_VALUEB5layer_with_weights-1/gamma/.ATTRIBUTES/VARIABLE_VALUEB4layer_with_weights-1/beta/.ATTRIBUTES/VARIABLE_VALUEB;layer_with_weights-1/moving_mean/.ATTRIBUTES/VARIABLE_VALUEB?layer_with_weights-1/moving_variance/.ATTRIBUTES/VARIABLE_VALUEB6layer_with_weights-2/kernel/.ATTRIBUTES/VARIABLE_VALUEB4layer_with_weights-2/bias/.ATTRIBUTES/VARIABLE_VALUEB5layer_with_weights-3/gamma/.ATTRIBUTES/VARIABLE_VALUEB4layer_with_weights-3/beta/.ATTRIBUTES/VARIABLE_VALUEB;layer_with_weights-3/moving_mean/.ATTRIBUTES/VARIABLE_VALUEB?layer_with_weights-3/moving_variance/.ATTRIBUTES/VARIABLE_VALUEB6layer_with_weights-4/kernel/.ATTRIBUTES/VARIABLE_VALUEB4layer_with_weights-4/bias/.ATTRIBUTES/VARIABLE_VALUEB5layer_with_weights-5/gamma/.ATTRIBUTES/VARIABLE_VALUEB4layer_with_weights-5/beta/.ATTRIBUTES/VARIABLE_VALUEB;layer_with_weights-5/moving_mean/.ATTRIBUTES/VARIABLE_VALUEB?layer_with_weights-5/moving_variance/.ATTRIBUTES/VARIABLE_VALUEB6layer_with_weights-6/kernel/.ATTRIBUTES/VARIABLE_VALUEB4layer_with_weights-6/bias/.ATTRIBUTES/VARIABLE_VALUEB5layer_with_weights-7/gamma/.ATTRIBUTES/VARIABLE_VALUEB4layer_with_weights-7/beta/.ATTRIBUTES/VARIABLE_VALUEB;layer_with_weights-7/moving_mean/.ATTRIBUTES/VARIABLE_VALUEB?layer_with_weights-7/moving_variance/.ATTRIBUTES/VARIABLE_VALUEB6layer_with_weights-8/kernel/.ATTRIBUTES/VARIABLE_VALUEB4layer_with_weights-8/bias/.ATTRIBUTES/VARIABLE_VALUEB6layer_with_weights-9/kernel/.ATTRIBUTES/VARIABLE_VALUEB4layer_with_weights-9/bias/.ATTRIBUTES/VARIABLE_VALUEB)optimizer/iter/.ATTRIBUTES/VARIABLE_VALUEB+optimizer/beta_1/.ATTRIBUTES/VARIABLE_VALUEB+optimizer/beta_2/.ATTRIBUTES/VARIABLE_VALUEB*optimizer/decay/.ATTRIBUTES/VARIABLE_VALUEB2optimizer/learning_rate/.ATTRIBUTES/VARIABLE_VALUEB4keras_api/metrics/0/total/.ATTRIBUTES/VARIABLE_VALUEB4keras_api/metrics/0/count/.ATTRIBUTES/VARIABLE_VALUEB4keras_api/metrics/1/total/.ATTRIBUTES/VARIABLE_VALUEB4keras_api/metrics/1/count/.ATTRIBUTES/VARIABLE_VALUEBRlayer_with_weights-0/kernel/.OPTIMIZER_SLOT/optimizer/m/.ATTRIBUTES/VARIABLE_VALUEBPlayer_with_weights-0/bias/.OPTIMIZER_SLOT/optimizer/m/.ATTRIBUTES/VARIABLE_VALUEBQlayer_with_weights-1/gamma/.OPTIMIZER_SLOT/optimizer/m/.ATTRIBUTES/VARIABLE_VALUEBPlayer_with_weights-1/beta/.OPTIMIZER_SLOT/optimizer/m/.ATTRIBUTES/VARIABLE_VALUEBRlayer_with_weights-2/kernel/.OPTIMIZER_SLOT/optimizer/m/.ATTRIBUTES/VARIABLE_VALUEBPlayer_with_weights-2/bias/.OPTIMIZER_SLOT/optimizer/m/.ATTRIBUTES/VARIABLE_VALUEBQlayer_with_weights-3/gamma/.OPTIMIZER_SLOT/optimizer/m/.ATTRIBUTES/VARIABLE_VALUEBPlayer_with_weights-3/beta/.OPTIMIZER_SLOT/optimizer/m/.ATTRIBUTES/VARIABLE_VALUEBRlayer_with_weights-4/kernel/.OPTIMIZER_SLOT/optimizer/m/.ATTRIBUTES/VARIABLE_VALUEBPlayer_with_weights-4/bias/.OPTIMIZER_SLOT/optimizer/m/.ATTRIBUTES/VARIABLE_VALUEBQlayer_with_weights-5/gamma/.OPTIMIZER_SLOT/optimizer/m/.ATTRIBUTES/VARIABLE_VALUEBPlayer_with_weights-5/beta/.OPTIMIZER_SLOT/optimizer/m/.ATTRIBUTES/VARIABLE_VALUEBRlayer_with_weights-6/kernel/.OPTIMIZER_SLOT/optimizer/m/.ATTRIBUTES/VARIABLE_VALUEBPlayer_with_weights-6/bias/.OPTIMIZER_SLOT/optimizer/m/.ATTRIBUTES/VARIABLE_VALUEBQlayer_with_weights-7/gamma/.OPTIMIZER_SLOT/optimizer/m/.ATTRIBUTES/VARIABLE_VALUEBPlayer_with_weights-7/beta/.OPTIMIZER_SLOT/optimizer/m/.ATTRIBUTES/VARIABLE_VALUEBRlayer_with_weights-8/kernel/.OPTIMIZER_SLOT/optimizer/m/.ATTRIBUTES/VARIABLE_VALUEBPlayer_with_weights-8/bias/.OPTIMIZER_SLOT/optimizer/m/.ATTRIBUTES/VARIABLE_VALUEBRlayer_with_weights-9/kernel/.OPTIMIZER_SLOT/optimizer/m/.ATTRIBUTES/VARIABLE_VALUEBPlayer_with_weights-9/bias/.OPTIMIZER_SLOT/optimizer/m/.ATTRIBUTES/VARIABLE_VALUEBRlayer_with_weights-0/kernel/.OPTIMIZER_SLOT/optimizer/v/.ATTRIBUTES/VARIABLE_VALUEBPlayer_with_weights-0/bias/.OPTIMIZER_SLOT/optimizer/v/.ATTRIBUTES/VARIABLE_VALUEBQlayer_with_weights-1/gamma/.OPTIMIZER_SLOT/optimizer/v/.ATTRIBUTES/VARIABLE_VALUEBPlayer_with_weights-1/beta/.OPTIMIZER_SLOT/optimizer/v/.ATTRIBUTES/VARIABLE_VALUEBRlayer_with_weights-2/kernel/.OPTIMIZER_SLOT/optimizer/v/.ATTRIBUTES/VARIABLE_VALUEBPlayer_with_weights-2/bias/.OPTIMIZER_SLOT/optimizer/v/.ATTRIBUTES/VARIABLE_VALUEBQlayer_with_weights-3/gamma/.OPTIMIZER_SLOT/optimizer/v/.ATTRIBUTES/VARIABLE_VALUEBPlayer_with_weights-3/beta/.OPTIMIZER_SLOT/optimizer/v/.ATTRIBUTES/VARIABLE_VALUEBRlayer_with_weights-4/kernel/.OPTIMIZER_SLOT/optimizer/v/.ATTRIBUTES/VARIABLE_VALUEBPlayer_with_weights-4/bias/.OPTIMIZER_SLOT/optimizer/v/.ATTRIBUTES/VARIABLE_VALUEBQlayer_with_weights-5/gamma/.OPTIMIZER_SLOT/optimizer/v/.ATTRIBUTES/VARIABLE_VALUEBPlayer_with_weights-5/beta/.OPTIMIZER_SLOT/optimizer/v/.ATTRIBUTES/VARIABLE_VALUEBRlayer_with_weights-6/kernel/.OPTIMIZER_SLOT/optimizer/v/.ATTRIBUTES/VARIABLE_VALUEBPlayer_with_weights-6/bias/.OPTIMIZER_SLOT/optimizer/v/.ATTRIBUTES/VARIABLE_VALUEBQlayer_with_weights-7/gamma/.OPTIMIZER_SLOT/optimizer/v/.ATTRIBUTES/VARIABLE_VALUEBPlayer_with_weights-7/beta/.OPTIMIZER_SLOT/optimizer/v/.ATTRIBUTES/VARIABLE_VALUEBRlayer_with_weights-8/kernel/.OPTIMIZER_SLOT/optimizer/v/.ATTRIBUTES/VARIABLE_VALUEBPlayer_with_weights-8/bias/.OPTIMIZER_SLOT/optimizer/v/.ATTRIBUTES/VARIABLE_VALUEBRlayer_with_weights-9/kernel/.OPTIMIZER_SLOT/optimizer/v/.ATTRIBUTES/VARIABLE_VALUEBPlayer_with_weights-9/bias/.OPTIMIZER_SLOT/optimizer/v/.ATTRIBUTES/VARIABLE_VALUEB_CHECKPOINTABLE_OBJECT_GRAPH�
RestoreV2/shape_and_slicesConst"/device:CPU:0*
_output_shapes
:N*
dtype0*�
value�B�NB B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B �
	RestoreV2	RestoreV2file_prefixRestoreV2/tensor_names:output:0#RestoreV2/shape_and_slices:output:0"/device:CPU:0*�
_output_shapes�
�::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::*\
dtypesR
P2N	[
IdentityIdentityRestoreV2:tensors:0"/device:CPU:0*
T0*
_output_shapes
:�
AssignVariableOpAssignVariableOp#assignvariableop_block0_conv_kernelIdentity:output:0"/device:CPU:0*
_output_shapes
 *
dtype0]

Identity_1IdentityRestoreV2:tensors:1"/device:CPU:0*
T0*
_output_shapes
:�
AssignVariableOp_1AssignVariableOp#assignvariableop_1_block0_conv_biasIdentity_1:output:0"/device:CPU:0*
_output_shapes
 *
dtype0]

Identity_2IdentityRestoreV2:tensors:2"/device:CPU:0*
T0*
_output_shapes
:�
AssignVariableOp_2AssignVariableOp$assignvariableop_2_block0_norm_gammaIdentity_2:output:0"/device:CPU:0*
_output_shapes
 *
dtype0]

Identity_3IdentityRestoreV2:tensors:3"/device:CPU:0*
T0*
_output_shapes
:�
AssignVariableOp_3AssignVariableOp#assignvariableop_3_block0_norm_betaIdentity_3:output:0"/device:CPU:0*
_output_shapes
 *
dtype0]

Identity_4IdentityRestoreV2:tensors:4"/device:CPU:0*
T0*
_output_shapes
:�
AssignVariableOp_4AssignVariableOp*assignvariableop_4_block0_norm_moving_meanIdentity_4:output:0"/device:CPU:0*
_output_shapes
 *
dtype0]

Identity_5IdentityRestoreV2:tensors:5"/device:CPU:0*
T0*
_output_shapes
:�
AssignVariableOp_5AssignVariableOp.assignvariableop_5_block0_norm_moving_varianceIdentity_5:output:0"/device:CPU:0*
_output_shapes
 *
dtype0]

Identity_6IdentityRestoreV2:tensors:6"/device:CPU:0*
T0*
_output_shapes
:�
AssignVariableOp_6AssignVariableOp%assignvariableop_6_block1_conv_kernelIdentity_6:output:0"/device:CPU:0*
_output_shapes
 *
dtype0]

Identity_7IdentityRestoreV2:tensors:7"/device:CPU:0*
T0*
_output_shapes
:�
AssignVariableOp_7AssignVariableOp#assignvariableop_7_block1_conv_biasIdentity_7:output:0"/device:CPU:0*
_output_shapes
 *
dtype0]

Identity_8IdentityRestoreV2:tensors:8"/device:CPU:0*
T0*
_output_shapes
:�
AssignVariableOp_8AssignVariableOp$assignvariableop_8_block1_norm_gammaIdentity_8:output:0"/device:CPU:0*
_output_shapes
 *
dtype0]

Identity_9IdentityRestoreV2:tensors:9"/device:CPU:0*
T0*
_output_shapes
:�
AssignVariableOp_9AssignVariableOp#assignvariableop_9_block1_norm_betaIdentity_9:output:0"/device:CPU:0*
_output_shapes
 *
dtype0_
Identity_10IdentityRestoreV2:tensors:10"/device:CPU:0*
T0*
_output_shapes
:�
AssignVariableOp_10AssignVariableOp+assignvariableop_10_block1_norm_moving_meanIdentity_10:output:0"/device:CPU:0*
_output_shapes
 *
dtype0_
Identity_11IdentityRestoreV2:tensors:11"/device:CPU:0*
T0*
_output_shapes
:�
AssignVariableOp_11AssignVariableOp/assignvariableop_11_block1_norm_moving_varianceIdentity_11:output:0"/device:CPU:0*
_output_shapes
 *
dtype0_
Identity_12IdentityRestoreV2:tensors:12"/device:CPU:0*
T0*
_output_shapes
:�
AssignVariableOp_12AssignVariableOp&assignvariableop_12_block2_conv_kernelIdentity_12:output:0"/device:CPU:0*
_output_shapes
 *
dtype0_
Identity_13IdentityRestoreV2:tensors:13"/device:CPU:0*
T0*
_output_shapes
:�
AssignVariableOp_13AssignVariableOp$assignvariableop_13_block2_conv_biasIdentity_13:output:0"/device:CPU:0*
_output_shapes
 *
dtype0_
Identity_14IdentityRestoreV2:tensors:14"/device:CPU:0*
T0*
_output_shapes
:�
AssignVariableOp_14AssignVariableOp%assignvariableop_14_block2_norm_gammaIdentity_14:output:0"/device:CPU:0*
_output_shapes
 *
dtype0_
Identity_15IdentityRestoreV2:tensors:15"/device:CPU:0*
T0*
_output_shapes
:�
AssignVariableOp_15AssignVariableOp$assignvariableop_15_block2_norm_betaIdentity_15:output:0"/device:CPU:0*
_output_shapes
 *
dtype0_
Identity_16IdentityRestoreV2:tensors:16"/device:CPU:0*
T0*
_output_shapes
:�
AssignVariableOp_16AssignVariableOp+assignvariableop_16_block2_norm_moving_meanIdentity_16:output:0"/device:CPU:0*
_output_shapes
 *
dtype0_
Identity_17IdentityRestoreV2:tensors:17"/device:CPU:0*
T0*
_output_shapes
:�
AssignVariableOp_17AssignVariableOp/assignvariableop_17_block2_norm_moving_varianceIdentity_17:output:0"/device:CPU:0*
_output_shapes
 *
dtype0_
Identity_18IdentityRestoreV2:tensors:18"/device:CPU:0*
T0*
_output_shapes
:�
AssignVariableOp_18AssignVariableOp&assignvariableop_18_block3_conv_kernelIdentity_18:output:0"/device:CPU:0*
_output_shapes
 *
dtype0_
Identity_19IdentityRestoreV2:tensors:19"/device:CPU:0*
T0*
_output_shapes
:�
AssignVariableOp_19AssignVariableOp$assignvariableop_19_block3_conv_biasIdentity_19:output:0"/device:CPU:0*
_output_shapes
 *
dtype0_
Identity_20IdentityRestoreV2:tensors:20"/device:CPU:0*
T0*
_output_shapes
:�
AssignVariableOp_20AssignVariableOp%assignvariableop_20_block3_norm_gammaIdentity_20:output:0"/device:CPU:0*
_output_shapes
 *
dtype0_
Identity_21IdentityRestoreV2:tensors:21"/device:CPU:0*
T0*
_output_shapes
:�
AssignVariableOp_21AssignVariableOp$assignvariableop_21_block3_norm_betaIdentity_21:output:0"/device:CPU:0*
_output_shapes
 *
dtype0_
Identity_22IdentityRestoreV2:tensors:22"/device:CPU:0*
T0*
_output_shapes
:�
AssignVariableOp_22AssignVariableOp+assignvariableop_22_block3_norm_moving_meanIdentity_22:output:0"/device:CPU:0*
_output_shapes
 *
dtype0_
Identity_23IdentityRestoreV2:tensors:23"/device:CPU:0*
T0*
_output_shapes
:�
AssignVariableOp_23AssignVariableOp/assignvariableop_23_block3_norm_moving_varianceIdentity_23:output:0"/device:CPU:0*
_output_shapes
 *
dtype0_
Identity_24IdentityRestoreV2:tensors:24"/device:CPU:0*
T0*
_output_shapes
:�
AssignVariableOp_24AssignVariableOp&assignvariableop_24_final_dense_kernelIdentity_24:output:0"/device:CPU:0*
_output_shapes
 *
dtype0_
Identity_25IdentityRestoreV2:tensors:25"/device:CPU:0*
T0*
_output_shapes
:�
AssignVariableOp_25AssignVariableOp$assignvariableop_25_final_dense_biasIdentity_25:output:0"/device:CPU:0*
_output_shapes
 *
dtype0_
Identity_26IdentityRestoreV2:tensors:26"/device:CPU:0*
T0*
_output_shapes
:�
AssignVariableOp_26AssignVariableOp!assignvariableop_26_output_kernelIdentity_26:output:0"/device:CPU:0*
_output_shapes
 *
dtype0_
Identity_27IdentityRestoreV2:tensors:27"/device:CPU:0*
T0*
_output_shapes
:�
AssignVariableOp_27AssignVariableOpassignvariableop_27_output_biasIdentity_27:output:0"/device:CPU:0*
_output_shapes
 *
dtype0_
Identity_28IdentityRestoreV2:tensors:28"/device:CPU:0*
T0	*
_output_shapes
:�
AssignVariableOp_28AssignVariableOpassignvariableop_28_adam_iterIdentity_28:output:0"/device:CPU:0*
_output_shapes
 *
dtype0	_
Identity_29IdentityRestoreV2:tensors:29"/device:CPU:0*
T0*
_output_shapes
:�
AssignVariableOp_29AssignVariableOpassignvariableop_29_adam_beta_1Identity_29:output:0"/device:CPU:0*
_output_shapes
 *
dtype0_
Identity_30IdentityRestoreV2:tensors:30"/device:CPU:0*
T0*
_output_shapes
:�
AssignVariableOp_30AssignVariableOpassignvariableop_30_adam_beta_2Identity_30:output:0"/device:CPU:0*
_output_shapes
 *
dtype0_
Identity_31IdentityRestoreV2:tensors:31"/device:CPU:0*
T0*
_output_shapes
:�
AssignVariableOp_31AssignVariableOpassignvariableop_31_adam_decayIdentity_31:output:0"/device:CPU:0*
_output_shapes
 *
dtype0_
Identity_32IdentityRestoreV2:tensors:32"/device:CPU:0*
T0*
_output_shapes
:�
AssignVariableOp_32AssignVariableOp&assignvariableop_32_adam_learning_rateIdentity_32:output:0"/device:CPU:0*
_output_shapes
 *
dtype0_
Identity_33IdentityRestoreV2:tensors:33"/device:CPU:0*
T0*
_output_shapes
:�
AssignVariableOp_33AssignVariableOpassignvariableop_33_totalIdentity_33:output:0"/device:CPU:0*
_output_shapes
 *
dtype0_
Identity_34IdentityRestoreV2:tensors:34"/device:CPU:0*
T0*
_output_shapes
:�
AssignVariableOp_34AssignVariableOpassignvariableop_34_countIdentity_34:output:0"/device:CPU:0*
_output_shapes
 *
dtype0_
Identity_35IdentityRestoreV2:tensors:35"/device:CPU:0*
T0*
_output_shapes
:�
AssignVariableOp_35AssignVariableOpassignvariableop_35_total_1Identity_35:output:0"/device:CPU:0*
_output_shapes
 *
dtype0_
Identity_36IdentityRestoreV2:tensors:36"/device:CPU:0*
T0*
_output_shapes
:�
AssignVariableOp_36AssignVariableOpassignvariableop_36_count_1Identity_36:output:0"/device:CPU:0*
_output_shapes
 *
dtype0_
Identity_37IdentityRestoreV2:tensors:37"/device:CPU:0*
T0*
_output_shapes
:�
AssignVariableOp_37AssignVariableOp-assignvariableop_37_adam_block0_conv_kernel_mIdentity_37:output:0"/device:CPU:0*
_output_shapes
 *
dtype0_
Identity_38IdentityRestoreV2:tensors:38"/device:CPU:0*
T0*
_output_shapes
:�
AssignVariableOp_38AssignVariableOp+assignvariableop_38_adam_block0_conv_bias_mIdentity_38:output:0"/device:CPU:0*
_output_shapes
 *
dtype0_
Identity_39IdentityRestoreV2:tensors:39"/device:CPU:0*
T0*
_output_shapes
:�
AssignVariableOp_39AssignVariableOp,assignvariableop_39_adam_block0_norm_gamma_mIdentity_39:output:0"/device:CPU:0*
_output_shapes
 *
dtype0_
Identity_40IdentityRestoreV2:tensors:40"/device:CPU:0*
T0*
_output_shapes
:�
AssignVariableOp_40AssignVariableOp+assignvariableop_40_adam_block0_norm_beta_mIdentity_40:output:0"/device:CPU:0*
_output_shapes
 *
dtype0_
Identity_41IdentityRestoreV2:tensors:41"/device:CPU:0*
T0*
_output_shapes
:�
AssignVariableOp_41AssignVariableOp-assignvariableop_41_adam_block1_conv_kernel_mIdentity_41:output:0"/device:CPU:0*
_output_shapes
 *
dtype0_
Identity_42IdentityRestoreV2:tensors:42"/device:CPU:0*
T0*
_output_shapes
:�
AssignVariableOp_42AssignVariableOp+assignvariableop_42_adam_block1_conv_bias_mIdentity_42:output:0"/device:CPU:0*
_output_shapes
 *
dtype0_
Identity_43IdentityRestoreV2:tensors:43"/device:CPU:0*
T0*
_output_shapes
:�
AssignVariableOp_43AssignVariableOp,assignvariableop_43_adam_block1_norm_gamma_mIdentity_43:output:0"/device:CPU:0*
_output_shapes
 *
dtype0_
Identity_44IdentityRestoreV2:tensors:44"/device:CPU:0*
T0*
_output_shapes
:�
AssignVariableOp_44AssignVariableOp+assignvariableop_44_adam_block1_norm_beta_mIdentity_44:output:0"/device:CPU:0*
_output_shapes
 *
dtype0_
Identity_45IdentityRestoreV2:tensors:45"/device:CPU:0*
T0*
_output_shapes
:�
AssignVariableOp_45AssignVariableOp-assignvariableop_45_adam_block2_conv_kernel_mIdentity_45:output:0"/device:CPU:0*
_output_shapes
 *
dtype0_
Identity_46IdentityRestoreV2:tensors:46"/device:CPU:0*
T0*
_output_shapes
:�
AssignVariableOp_46AssignVariableOp+assignvariableop_46_adam_block2_conv_bias_mIdentity_46:output:0"/device:CPU:0*
_output_shapes
 *
dtype0_
Identity_47IdentityRestoreV2:tensors:47"/device:CPU:0*
T0*
_output_shapes
:�
AssignVariableOp_47AssignVariableOp,assignvariableop_47_adam_block2_norm_gamma_mIdentity_47:output:0"/device:CPU:0*
_output_shapes
 *
dtype0_
Identity_48IdentityRestoreV2:tensors:48"/device:CPU:0*
T0*
_output_shapes
:�
AssignVariableOp_48AssignVariableOp+assignvariableop_48_adam_block2_norm_beta_mIdentity_48:output:0"/device:CPU:0*
_output_shapes
 *
dtype0_
Identity_49IdentityRestoreV2:tensors:49"/device:CPU:0*
T0*
_output_shapes
:�
AssignVariableOp_49AssignVariableOp-assignvariableop_49_adam_block3_conv_kernel_mIdentity_49:output:0"/device:CPU:0*
_output_shapes
 *
dtype0_
Identity_50IdentityRestoreV2:tensors:50"/device:CPU:0*
T0*
_output_shapes
:�
AssignVariableOp_50AssignVariableOp+assignvariableop_50_adam_block3_conv_bias_mIdentity_50:output:0"/device:CPU:0*
_output_shapes
 *
dtype0_
Identity_51IdentityRestoreV2:tensors:51"/device:CPU:0*
T0*
_output_shapes
:�
AssignVariableOp_51AssignVariableOp,assignvariableop_51_adam_block3_norm_gamma_mIdentity_51:output:0"/device:CPU:0*
_output_shapes
 *
dtype0_
Identity_52IdentityRestoreV2:tensors:52"/device:CPU:0*
T0*
_output_shapes
:�
AssignVariableOp_52AssignVariableOp+assignvariableop_52_adam_block3_norm_beta_mIdentity_52:output:0"/device:CPU:0*
_output_shapes
 *
dtype0_
Identity_53IdentityRestoreV2:tensors:53"/device:CPU:0*
T0*
_output_shapes
:�
AssignVariableOp_53AssignVariableOp-assignvariableop_53_adam_final_dense_kernel_mIdentity_53:output:0"/device:CPU:0*
_output_shapes
 *
dtype0_
Identity_54IdentityRestoreV2:tensors:54"/device:CPU:0*
T0*
_output_shapes
:�
AssignVariableOp_54AssignVariableOp+assignvariableop_54_adam_final_dense_bias_mIdentity_54:output:0"/device:CPU:0*
_output_shapes
 *
dtype0_
Identity_55IdentityRestoreV2:tensors:55"/device:CPU:0*
T0*
_output_shapes
:�
AssignVariableOp_55AssignVariableOp(assignvariableop_55_adam_output_kernel_mIdentity_55:output:0"/device:CPU:0*
_output_shapes
 *
dtype0_
Identity_56IdentityRestoreV2:tensors:56"/device:CPU:0*
T0*
_output_shapes
:�
AssignVariableOp_56AssignVariableOp&assignvariableop_56_adam_output_bias_mIdentity_56:output:0"/device:CPU:0*
_output_shapes
 *
dtype0_
Identity_57IdentityRestoreV2:tensors:57"/device:CPU:0*
T0*
_output_shapes
:�
AssignVariableOp_57AssignVariableOp-assignvariableop_57_adam_block0_conv_kernel_vIdentity_57:output:0"/device:CPU:0*
_output_shapes
 *
dtype0_
Identity_58IdentityRestoreV2:tensors:58"/device:CPU:0*
T0*
_output_shapes
:�
AssignVariableOp_58AssignVariableOp+assignvariableop_58_adam_block0_conv_bias_vIdentity_58:output:0"/device:CPU:0*
_output_shapes
 *
dtype0_
Identity_59IdentityRestoreV2:tensors:59"/device:CPU:0*
T0*
_output_shapes
:�
AssignVariableOp_59AssignVariableOp,assignvariableop_59_adam_block0_norm_gamma_vIdentity_59:output:0"/device:CPU:0*
_output_shapes
 *
dtype0_
Identity_60IdentityRestoreV2:tensors:60"/device:CPU:0*
T0*
_output_shapes
:�
AssignVariableOp_60AssignVariableOp+assignvariableop_60_adam_block0_norm_beta_vIdentity_60:output:0"/device:CPU:0*
_output_shapes
 *
dtype0_
Identity_61IdentityRestoreV2:tensors:61"/device:CPU:0*
T0*
_output_shapes
:�
AssignVariableOp_61AssignVariableOp-assignvariableop_61_adam_block1_conv_kernel_vIdentity_61:output:0"/device:CPU:0*
_output_shapes
 *
dtype0_
Identity_62IdentityRestoreV2:tensors:62"/device:CPU:0*
T0*
_output_shapes
:�
AssignVariableOp_62AssignVariableOp+assignvariableop_62_adam_block1_conv_bias_vIdentity_62:output:0"/device:CPU:0*
_output_shapes
 *
dtype0_
Identity_63IdentityRestoreV2:tensors:63"/device:CPU:0*
T0*
_output_shapes
:�
AssignVariableOp_63AssignVariableOp,assignvariableop_63_adam_block1_norm_gamma_vIdentity_63:output:0"/device:CPU:0*
_output_shapes
 *
dtype0_
Identity_64IdentityRestoreV2:tensors:64"/device:CPU:0*
T0*
_output_shapes
:�
AssignVariableOp_64AssignVariableOp+assignvariableop_64_adam_block1_norm_beta_vIdentity_64:output:0"/device:CPU:0*
_output_shapes
 *
dtype0_
Identity_65IdentityRestoreV2:tensors:65"/device:CPU:0*
T0*
_output_shapes
:�
AssignVariableOp_65AssignVariableOp-assignvariableop_65_adam_block2_conv_kernel_vIdentity_65:output:0"/device:CPU:0*
_output_shapes
 *
dtype0_
Identity_66IdentityRestoreV2:tensors:66"/device:CPU:0*
T0*
_output_shapes
:�
AssignVariableOp_66AssignVariableOp+assignvariableop_66_adam_block2_conv_bias_vIdentity_66:output:0"/device:CPU:0*
_output_shapes
 *
dtype0_
Identity_67IdentityRestoreV2:tensors:67"/device:CPU:0*
T0*
_output_shapes
:�
AssignVariableOp_67AssignVariableOp,assignvariableop_67_adam_block2_norm_gamma_vIdentity_67:output:0"/device:CPU:0*
_output_shapes
 *
dtype0_
Identity_68IdentityRestoreV2:tensors:68"/device:CPU:0*
T0*
_output_shapes
:�
AssignVariableOp_68AssignVariableOp+assignvariableop_68_adam_block2_norm_beta_vIdentity_68:output:0"/device:CPU:0*
_output_shapes
 *
dtype0_
Identity_69IdentityRestoreV2:tensors:69"/device:CPU:0*
T0*
_output_shapes
:�
AssignVariableOp_69AssignVariableOp-assignvariableop_69_adam_block3_conv_kernel_vIdentity_69:output:0"/device:CPU:0*
_output_shapes
 *
dtype0_
Identity_70IdentityRestoreV2:tensors:70"/device:CPU:0*
T0*
_output_shapes
:�
AssignVariableOp_70AssignVariableOp+assignvariableop_70_adam_block3_conv_bias_vIdentity_70:output:0"/device:CPU:0*
_output_shapes
 *
dtype0_
Identity_71IdentityRestoreV2:tensors:71"/device:CPU:0*
T0*
_output_shapes
:�
AssignVariableOp_71AssignVariableOp,assignvariableop_71_adam_block3_norm_gamma_vIdentity_71:output:0"/device:CPU:0*
_output_shapes
 *
dtype0_
Identity_72IdentityRestoreV2:tensors:72"/device:CPU:0*
T0*
_output_shapes
:�
AssignVariableOp_72AssignVariableOp+assignvariableop_72_adam_block3_norm_beta_vIdentity_72:output:0"/device:CPU:0*
_output_shapes
 *
dtype0_
Identity_73IdentityRestoreV2:tensors:73"/device:CPU:0*
T0*
_output_shapes
:�
AssignVariableOp_73AssignVariableOp-assignvariableop_73_adam_final_dense_kernel_vIdentity_73:output:0"/device:CPU:0*
_output_shapes
 *
dtype0_
Identity_74IdentityRestoreV2:tensors:74"/device:CPU:0*
T0*
_output_shapes
:�
AssignVariableOp_74AssignVariableOp+assignvariableop_74_adam_final_dense_bias_vIdentity_74:output:0"/device:CPU:0*
_output_shapes
 *
dtype0_
Identity_75IdentityRestoreV2:tensors:75"/device:CPU:0*
T0*
_output_shapes
:�
AssignVariableOp_75AssignVariableOp(assignvariableop_75_adam_output_kernel_vIdentity_75:output:0"/device:CPU:0*
_output_shapes
 *
dtype0_
Identity_76IdentityRestoreV2:tensors:76"/device:CPU:0*
T0*
_output_shapes
:�
AssignVariableOp_76AssignVariableOp&assignvariableop_76_adam_output_bias_vIdentity_76:output:0"/device:CPU:0*
_output_shapes
 *
dtype01
NoOpNoOp"/device:CPU:0*
_output_shapes
 �
Identity_77Identityfile_prefix^AssignVariableOp^AssignVariableOp_1^AssignVariableOp_10^AssignVariableOp_11^AssignVariableOp_12^AssignVariableOp_13^AssignVariableOp_14^AssignVariableOp_15^AssignVariableOp_16^AssignVariableOp_17^AssignVariableOp_18^AssignVariableOp_19^AssignVariableOp_2^AssignVariableOp_20^AssignVariableOp_21^AssignVariableOp_22^AssignVariableOp_23^AssignVariableOp_24^AssignVariableOp_25^AssignVariableOp_26^AssignVariableOp_27^AssignVariableOp_28^AssignVariableOp_29^AssignVariableOp_3^AssignVariableOp_30^AssignVariableOp_31^AssignVariableOp_32^AssignVariableOp_33^AssignVariableOp_34^AssignVariableOp_35^AssignVariableOp_36^AssignVariableOp_37^AssignVariableOp_38^AssignVariableOp_39^AssignVariableOp_4^AssignVariableOp_40^AssignVariableOp_41^AssignVariableOp_42^AssignVariableOp_43^AssignVariableOp_44^AssignVariableOp_45^AssignVariableOp_46^AssignVariableOp_47^AssignVariableOp_48^AssignVariableOp_49^AssignVariableOp_5^AssignVariableOp_50^AssignVariableOp_51^AssignVariableOp_52^AssignVariableOp_53^AssignVariableOp_54^AssignVariableOp_55^AssignVariableOp_56^AssignVariableOp_57^AssignVariableOp_58^AssignVariableOp_59^AssignVariableOp_6^AssignVariableOp_60^AssignVariableOp_61^AssignVariableOp_62^AssignVariableOp_63^AssignVariableOp_64^AssignVariableOp_65^AssignVariableOp_66^AssignVariableOp_67^AssignVariableOp_68^AssignVariableOp_69^AssignVariableOp_7^AssignVariableOp_70^AssignVariableOp_71^AssignVariableOp_72^AssignVariableOp_73^AssignVariableOp_74^AssignVariableOp_75^AssignVariableOp_76^AssignVariableOp_8^AssignVariableOp_9^NoOp"/device:CPU:0*
T0*
_output_shapes
: W
Identity_78IdentityIdentity_77:output:0^NoOp_1*
T0*
_output_shapes
: �
NoOp_1NoOp^AssignVariableOp^AssignVariableOp_1^AssignVariableOp_10^AssignVariableOp_11^AssignVariableOp_12^AssignVariableOp_13^AssignVariableOp_14^AssignVariableOp_15^AssignVariableOp_16^AssignVariableOp_17^AssignVariableOp_18^AssignVariableOp_19^AssignVariableOp_2^AssignVariableOp_20^AssignVariableOp_21^AssignVariableOp_22^AssignVariableOp_23^AssignVariableOp_24^AssignVariableOp_25^AssignVariableOp_26^AssignVariableOp_27^AssignVariableOp_28^AssignVariableOp_29^AssignVariableOp_3^AssignVariableOp_30^AssignVariableOp_31^AssignVariableOp_32^AssignVariableOp_33^AssignVariableOp_34^AssignVariableOp_35^AssignVariableOp_36^AssignVariableOp_37^AssignVariableOp_38^AssignVariableOp_39^AssignVariableOp_4^AssignVariableOp_40^AssignVariableOp_41^AssignVariableOp_42^AssignVariableOp_43^AssignVariableOp_44^AssignVariableOp_45^AssignVariableOp_46^AssignVariableOp_47^AssignVariableOp_48^AssignVariableOp_49^AssignVariableOp_5^AssignVariableOp_50^AssignVariableOp_51^AssignVariableOp_52^AssignVariableOp_53^AssignVariableOp_54^AssignVariableOp_55^AssignVariableOp_56^AssignVariableOp_57^AssignVariableOp_58^AssignVariableOp_59^AssignVariableOp_6^AssignVariableOp_60^AssignVariableOp_61^AssignVariableOp_62^AssignVariableOp_63^AssignVariableOp_64^AssignVariableOp_65^AssignVariableOp_66^AssignVariableOp_67^AssignVariableOp_68^AssignVariableOp_69^AssignVariableOp_7^AssignVariableOp_70^AssignVariableOp_71^AssignVariableOp_72^AssignVariableOp_73^AssignVariableOp_74^AssignVariableOp_75^AssignVariableOp_76^AssignVariableOp_8^AssignVariableOp_9*"
_acd_function_control_output(*
_output_shapes
 "#
identity_78Identity_78:output:0*�
_input_shapes�
�: : : : : : : : : : : : : : : : : : : : : : : : : : : : : : : : : : : : : : : : : : : : : : : : : : : : : : : : : : : : : : : : : : : : : : : : : : : : : : 2$
AssignVariableOpAssignVariableOp2(
AssignVariableOp_1AssignVariableOp_12*
AssignVariableOp_10AssignVariableOp_102*
AssignVariableOp_11AssignVariableOp_112*
AssignVariableOp_12AssignVariableOp_122*
AssignVariableOp_13AssignVariableOp_132*
AssignVariableOp_14AssignVariableOp_142*
AssignVariableOp_15AssignVariableOp_152*
AssignVariableOp_16AssignVariableOp_162*
AssignVariableOp_17AssignVariableOp_172*
AssignVariableOp_18AssignVariableOp_182*
AssignVariableOp_19AssignVariableOp_192(
AssignVariableOp_2AssignVariableOp_22*
AssignVariableOp_20AssignVariableOp_202*
AssignVariableOp_21AssignVariableOp_212*
AssignVariableOp_22AssignVariableOp_222*
AssignVariableOp_23AssignVariableOp_232*
AssignVariableOp_24AssignVariableOp_242*
AssignVariableOp_25AssignVariableOp_252*
AssignVariableOp_26AssignVariableOp_262*
AssignVariableOp_27AssignVariableOp_272*
AssignVariableOp_28AssignVariableOp_282*
AssignVariableOp_29AssignVariableOp_292(
AssignVariableOp_3AssignVariableOp_32*
AssignVariableOp_30AssignVariableOp_302*
AssignVariableOp_31AssignVariableOp_312*
AssignVariableOp_32AssignVariableOp_322*
AssignVariableOp_33AssignVariableOp_332*
AssignVariableOp_34AssignVariableOp_342*
AssignVariableOp_35AssignVariableOp_352*
AssignVariableOp_36AssignVariableOp_362*
AssignVariableOp_37AssignVariableOp_372*
AssignVariableOp_38AssignVariableOp_382*
AssignVariableOp_39AssignVariableOp_392(
AssignVariableOp_4AssignVariableOp_42*
AssignVariableOp_40AssignVariableOp_402*
AssignVariableOp_41AssignVariableOp_412*
AssignVariableOp_42AssignVariableOp_422*
AssignVariableOp_43AssignVariableOp_432*
AssignVariableOp_44AssignVariableOp_442*
AssignVariableOp_45AssignVariableOp_452*
AssignVariableOp_46AssignVariableOp_462*
AssignVariableOp_47AssignVariableOp_472*
AssignVariableOp_48AssignVariableOp_482*
AssignVariableOp_49AssignVariableOp_492(
AssignVariableOp_5AssignVariableOp_52*
AssignVariableOp_50AssignVariableOp_502*
AssignVariableOp_51AssignVariableOp_512*
AssignVariableOp_52AssignVariableOp_522*
AssignVariableOp_53AssignVariableOp_532*
AssignVariableOp_54AssignVariableOp_542*
AssignVariableOp_55AssignVariableOp_552*
AssignVariableOp_56AssignVariableOp_562*
AssignVariableOp_57AssignVariableOp_572*
AssignVariableOp_58AssignVariableOp_582*
AssignVariableOp_59AssignVariableOp_592(
AssignVariableOp_6AssignVariableOp_62*
AssignVariableOp_60AssignVariableOp_602*
AssignVariableOp_61AssignVariableOp_612*
AssignVariableOp_62AssignVariableOp_622*
AssignVariableOp_63AssignVariableOp_632*
AssignVariableOp_64AssignVariableOp_642*
AssignVariableOp_65AssignVariableOp_652*
AssignVariableOp_66AssignVariableOp_662*
AssignVariableOp_67AssignVariableOp_672*
AssignVariableOp_68AssignVariableOp_682*
AssignVariableOp_69AssignVariableOp_692(
AssignVariableOp_7AssignVariableOp_72*
AssignVariableOp_70AssignVariableOp_702*
AssignVariableOp_71AssignVariableOp_712*
AssignVariableOp_72AssignVariableOp_722*
AssignVariableOp_73AssignVariableOp_732*
AssignVariableOp_74AssignVariableOp_742*
AssignVariableOp_75AssignVariableOp_752*
AssignVariableOp_76AssignVariableOp_762(
AssignVariableOp_8AssignVariableOp_82(
AssignVariableOp_9AssignVariableOp_9:C ?

_output_shapes
: 
%
_user_specified_namefile_prefix
��
�
__inference__wrapped_model_7156
input_1O
5sequential_block0_conv_conv2d_readvariableop_resource:D
6sequential_block0_conv_biasadd_readvariableop_resource:<
.sequential_block0_norm_readvariableop_resource:>
0sequential_block0_norm_readvariableop_1_resource:M
?sequential_block0_norm_fusedbatchnormv3_readvariableop_resource:O
Asequential_block0_norm_fusedbatchnormv3_readvariableop_1_resource:O
5sequential_block1_conv_conv2d_readvariableop_resource: D
6sequential_block1_conv_biasadd_readvariableop_resource: <
.sequential_block1_norm_readvariableop_resource: >
0sequential_block1_norm_readvariableop_1_resource: M
?sequential_block1_norm_fusedbatchnormv3_readvariableop_resource: O
Asequential_block1_norm_fusedbatchnormv3_readvariableop_1_resource: O
5sequential_block2_conv_conv2d_readvariableop_resource: @D
6sequential_block2_conv_biasadd_readvariableop_resource:@<
.sequential_block2_norm_readvariableop_resource:@>
0sequential_block2_norm_readvariableop_1_resource:@M
?sequential_block2_norm_fusedbatchnormv3_readvariableop_resource:@O
Asequential_block2_norm_fusedbatchnormv3_readvariableop_1_resource:@P
5sequential_block3_conv_conv2d_readvariableop_resource:@�E
6sequential_block3_conv_biasadd_readvariableop_resource:	�=
.sequential_block3_norm_readvariableop_resource:	�?
0sequential_block3_norm_readvariableop_1_resource:	�N
?sequential_block3_norm_fusedbatchnormv3_readvariableop_resource:	�P
Asequential_block3_norm_fusedbatchnormv3_readvariableop_1_resource:	�I
5sequential_final_dense_matmul_readvariableop_resource:
��E
6sequential_final_dense_biasadd_readvariableop_resource:	�C
0sequential_output_matmul_readvariableop_resource:	�?
1sequential_output_biasadd_readvariableop_resource:
identity��-sequential/Block0_Conv/BiasAdd/ReadVariableOp�,sequential/Block0_Conv/Conv2D/ReadVariableOp�6sequential/Block0_Norm/FusedBatchNormV3/ReadVariableOp�8sequential/Block0_Norm/FusedBatchNormV3/ReadVariableOp_1�%sequential/Block0_Norm/ReadVariableOp�'sequential/Block0_Norm/ReadVariableOp_1�-sequential/Block1_Conv/BiasAdd/ReadVariableOp�,sequential/Block1_Conv/Conv2D/ReadVariableOp�6sequential/Block1_Norm/FusedBatchNormV3/ReadVariableOp�8sequential/Block1_Norm/FusedBatchNormV3/ReadVariableOp_1�%sequential/Block1_Norm/ReadVariableOp�'sequential/Block1_Norm/ReadVariableOp_1�-sequential/Block2_Conv/BiasAdd/ReadVariableOp�,sequential/Block2_Conv/Conv2D/ReadVariableOp�6sequential/Block2_Norm/FusedBatchNormV3/ReadVariableOp�8sequential/Block2_Norm/FusedBatchNormV3/ReadVariableOp_1�%sequential/Block2_Norm/ReadVariableOp�'sequential/Block2_Norm/ReadVariableOp_1�-sequential/Block3_Conv/BiasAdd/ReadVariableOp�,sequential/Block3_Conv/Conv2D/ReadVariableOp�6sequential/Block3_Norm/FusedBatchNormV3/ReadVariableOp�8sequential/Block3_Norm/FusedBatchNormV3/ReadVariableOp_1�%sequential/Block3_Norm/ReadVariableOp�'sequential/Block3_Norm/ReadVariableOp_1�-sequential/Final_Dense/BiasAdd/ReadVariableOp�,sequential/Final_Dense/MatMul/ReadVariableOp�(sequential/output/BiasAdd/ReadVariableOp�'sequential/output/MatMul/ReadVariableOp�
,sequential/Block0_Conv/Conv2D/ReadVariableOpReadVariableOp5sequential_block0_conv_conv2d_readvariableop_resource*&
_output_shapes
:*
dtype0�
sequential/Block0_Conv/Conv2DConv2Dinput_14sequential/Block0_Conv/Conv2D/ReadVariableOp:value:0*
T0*1
_output_shapes
:�����������*
paddingSAME*
strides
�
-sequential/Block0_Conv/BiasAdd/ReadVariableOpReadVariableOp6sequential_block0_conv_biasadd_readvariableop_resource*
_output_shapes
:*
dtype0�
sequential/Block0_Conv/BiasAddBiasAdd&sequential/Block0_Conv/Conv2D:output:05sequential/Block0_Conv/BiasAdd/ReadVariableOp:value:0*
T0*1
_output_shapes
:������������
sequential/Block0_Conv/ReluRelu'sequential/Block0_Conv/BiasAdd:output:0*
T0*1
_output_shapes
:������������
sequential/Block0_Pool/MaxPoolMaxPool)sequential/Block0_Conv/Relu:activations:0*/
_output_shapes
:���������@V*
ksize
*
paddingVALID*
strides
�
%sequential/Block0_Norm/ReadVariableOpReadVariableOp.sequential_block0_norm_readvariableop_resource*
_output_shapes
:*
dtype0�
'sequential/Block0_Norm/ReadVariableOp_1ReadVariableOp0sequential_block0_norm_readvariableop_1_resource*
_output_shapes
:*
dtype0�
6sequential/Block0_Norm/FusedBatchNormV3/ReadVariableOpReadVariableOp?sequential_block0_norm_fusedbatchnormv3_readvariableop_resource*
_output_shapes
:*
dtype0�
8sequential/Block0_Norm/FusedBatchNormV3/ReadVariableOp_1ReadVariableOpAsequential_block0_norm_fusedbatchnormv3_readvariableop_1_resource*
_output_shapes
:*
dtype0�
'sequential/Block0_Norm/FusedBatchNormV3FusedBatchNormV3'sequential/Block0_Pool/MaxPool:output:0-sequential/Block0_Norm/ReadVariableOp:value:0/sequential/Block0_Norm/ReadVariableOp_1:value:0>sequential/Block0_Norm/FusedBatchNormV3/ReadVariableOp:value:0@sequential/Block0_Norm/FusedBatchNormV3/ReadVariableOp_1:value:0*
T0*
U0*K
_output_shapes9
7:���������@V:::::*
epsilon%o�:*
is_training( �
,sequential/Block1_Conv/Conv2D/ReadVariableOpReadVariableOp5sequential_block1_conv_conv2d_readvariableop_resource*&
_output_shapes
: *
dtype0�
sequential/Block1_Conv/Conv2DConv2D+sequential/Block0_Norm/FusedBatchNormV3:y:04sequential/Block1_Conv/Conv2D/ReadVariableOp:value:0*
T0*/
_output_shapes
:���������@V *
paddingSAME*
strides
�
-sequential/Block1_Conv/BiasAdd/ReadVariableOpReadVariableOp6sequential_block1_conv_biasadd_readvariableop_resource*
_output_shapes
: *
dtype0�
sequential/Block1_Conv/BiasAddBiasAdd&sequential/Block1_Conv/Conv2D:output:05sequential/Block1_Conv/BiasAdd/ReadVariableOp:value:0*
T0*/
_output_shapes
:���������@V �
sequential/Block1_Conv/ReluRelu'sequential/Block1_Conv/BiasAdd:output:0*
T0*/
_output_shapes
:���������@V �
sequential/Block1_Pool/MaxPoolMaxPool)sequential/Block1_Conv/Relu:activations:0*/
_output_shapes
:��������� + *
ksize
*
paddingVALID*
strides
�
%sequential/Block1_Norm/ReadVariableOpReadVariableOp.sequential_block1_norm_readvariableop_resource*
_output_shapes
: *
dtype0�
'sequential/Block1_Norm/ReadVariableOp_1ReadVariableOp0sequential_block1_norm_readvariableop_1_resource*
_output_shapes
: *
dtype0�
6sequential/Block1_Norm/FusedBatchNormV3/ReadVariableOpReadVariableOp?sequential_block1_norm_fusedbatchnormv3_readvariableop_resource*
_output_shapes
: *
dtype0�
8sequential/Block1_Norm/FusedBatchNormV3/ReadVariableOp_1ReadVariableOpAsequential_block1_norm_fusedbatchnormv3_readvariableop_1_resource*
_output_shapes
: *
dtype0�
'sequential/Block1_Norm/FusedBatchNormV3FusedBatchNormV3'sequential/Block1_Pool/MaxPool:output:0-sequential/Block1_Norm/ReadVariableOp:value:0/sequential/Block1_Norm/ReadVariableOp_1:value:0>sequential/Block1_Norm/FusedBatchNormV3/ReadVariableOp:value:0@sequential/Block1_Norm/FusedBatchNormV3/ReadVariableOp_1:value:0*
T0*
U0*K
_output_shapes9
7:��������� + : : : : :*
epsilon%o�:*
is_training( �
,sequential/Block2_Conv/Conv2D/ReadVariableOpReadVariableOp5sequential_block2_conv_conv2d_readvariableop_resource*&
_output_shapes
: @*
dtype0�
sequential/Block2_Conv/Conv2DConv2D+sequential/Block1_Norm/FusedBatchNormV3:y:04sequential/Block2_Conv/Conv2D/ReadVariableOp:value:0*
T0*/
_output_shapes
:��������� +@*
paddingSAME*
strides
�
-sequential/Block2_Conv/BiasAdd/ReadVariableOpReadVariableOp6sequential_block2_conv_biasadd_readvariableop_resource*
_output_shapes
:@*
dtype0�
sequential/Block2_Conv/BiasAddBiasAdd&sequential/Block2_Conv/Conv2D:output:05sequential/Block2_Conv/BiasAdd/ReadVariableOp:value:0*
T0*/
_output_shapes
:��������� +@�
sequential/Block2_Conv/ReluRelu'sequential/Block2_Conv/BiasAdd:output:0*
T0*/
_output_shapes
:��������� +@�
sequential/Block2_Pool/MaxPoolMaxPool)sequential/Block2_Conv/Relu:activations:0*/
_output_shapes
:���������@*
ksize
*
paddingVALID*
strides
�
%sequential/Block2_Norm/ReadVariableOpReadVariableOp.sequential_block2_norm_readvariableop_resource*
_output_shapes
:@*
dtype0�
'sequential/Block2_Norm/ReadVariableOp_1ReadVariableOp0sequential_block2_norm_readvariableop_1_resource*
_output_shapes
:@*
dtype0�
6sequential/Block2_Norm/FusedBatchNormV3/ReadVariableOpReadVariableOp?sequential_block2_norm_fusedbatchnormv3_readvariableop_resource*
_output_shapes
:@*
dtype0�
8sequential/Block2_Norm/FusedBatchNormV3/ReadVariableOp_1ReadVariableOpAsequential_block2_norm_fusedbatchnormv3_readvariableop_1_resource*
_output_shapes
:@*
dtype0�
'sequential/Block2_Norm/FusedBatchNormV3FusedBatchNormV3'sequential/Block2_Pool/MaxPool:output:0-sequential/Block2_Norm/ReadVariableOp:value:0/sequential/Block2_Norm/ReadVariableOp_1:value:0>sequential/Block2_Norm/FusedBatchNormV3/ReadVariableOp:value:0@sequential/Block2_Norm/FusedBatchNormV3/ReadVariableOp_1:value:0*
T0*
U0*K
_output_shapes9
7:���������@:@:@:@:@:*
epsilon%o�:*
is_training( �
,sequential/Block3_Conv/Conv2D/ReadVariableOpReadVariableOp5sequential_block3_conv_conv2d_readvariableop_resource*'
_output_shapes
:@�*
dtype0�
sequential/Block3_Conv/Conv2DConv2D+sequential/Block2_Norm/FusedBatchNormV3:y:04sequential/Block3_Conv/Conv2D/ReadVariableOp:value:0*
T0*0
_output_shapes
:����������*
paddingSAME*
strides
�
-sequential/Block3_Conv/BiasAdd/ReadVariableOpReadVariableOp6sequential_block3_conv_biasadd_readvariableop_resource*
_output_shapes	
:�*
dtype0�
sequential/Block3_Conv/BiasAddBiasAdd&sequential/Block3_Conv/Conv2D:output:05sequential/Block3_Conv/BiasAdd/ReadVariableOp:value:0*
T0*0
_output_shapes
:�����������
sequential/Block3_Conv/ReluRelu'sequential/Block3_Conv/BiasAdd:output:0*
T0*0
_output_shapes
:�����������
sequential/Block3_Pool/MaxPoolMaxPool)sequential/Block3_Conv/Relu:activations:0*0
_output_shapes
:���������
�*
ksize
*
paddingVALID*
strides
�
%sequential/Block3_Norm/ReadVariableOpReadVariableOp.sequential_block3_norm_readvariableop_resource*
_output_shapes	
:�*
dtype0�
'sequential/Block3_Norm/ReadVariableOp_1ReadVariableOp0sequential_block3_norm_readvariableop_1_resource*
_output_shapes	
:�*
dtype0�
6sequential/Block3_Norm/FusedBatchNormV3/ReadVariableOpReadVariableOp?sequential_block3_norm_fusedbatchnormv3_readvariableop_resource*
_output_shapes	
:�*
dtype0�
8sequential/Block3_Norm/FusedBatchNormV3/ReadVariableOp_1ReadVariableOpAsequential_block3_norm_fusedbatchnormv3_readvariableop_1_resource*
_output_shapes	
:�*
dtype0�
'sequential/Block3_Norm/FusedBatchNormV3FusedBatchNormV3'sequential/Block3_Pool/MaxPool:output:0-sequential/Block3_Norm/ReadVariableOp:value:0/sequential/Block3_Norm/ReadVariableOp_1:value:0>sequential/Block3_Norm/FusedBatchNormV3/ReadVariableOp:value:0@sequential/Block3_Norm/FusedBatchNormV3/ReadVariableOp_1:value:0*
T0*
U0*P
_output_shapes>
<:���������
�:�:�:�:�:*
epsilon%o�:*
is_training( �
1sequential/GlobalMaxPooling/Max/reduction_indicesConst*
_output_shapes
:*
dtype0*
valueB"      �
sequential/GlobalMaxPooling/MaxMax+sequential/Block3_Norm/FusedBatchNormV3:y:0:sequential/GlobalMaxPooling/Max/reduction_indices:output:0*
T0*(
_output_shapes
:�����������
,sequential/Final_Dense/MatMul/ReadVariableOpReadVariableOp5sequential_final_dense_matmul_readvariableop_resource* 
_output_shapes
:
��*
dtype0�
sequential/Final_Dense/MatMulMatMul(sequential/GlobalMaxPooling/Max:output:04sequential/Final_Dense/MatMul/ReadVariableOp:value:0*
T0*(
_output_shapes
:�����������
-sequential/Final_Dense/BiasAdd/ReadVariableOpReadVariableOp6sequential_final_dense_biasadd_readvariableop_resource*
_output_shapes	
:�*
dtype0�
sequential/Final_Dense/BiasAddBiasAdd'sequential/Final_Dense/MatMul:product:05sequential/Final_Dense/BiasAdd/ReadVariableOp:value:0*
T0*(
_output_shapes
:����������
sequential/Final_Dense/ReluRelu'sequential/Final_Dense/BiasAdd:output:0*
T0*(
_output_shapes
:�����������
'sequential/output/MatMul/ReadVariableOpReadVariableOp0sequential_output_matmul_readvariableop_resource*
_output_shapes
:	�*
dtype0�
sequential/output/MatMulMatMul)sequential/Final_Dense/Relu:activations:0/sequential/output/MatMul/ReadVariableOp:value:0*
T0*'
_output_shapes
:����������
(sequential/output/BiasAdd/ReadVariableOpReadVariableOp1sequential_output_biasadd_readvariableop_resource*
_output_shapes
:*
dtype0�
sequential/output/BiasAddBiasAdd"sequential/output/MatMul:product:00sequential/output/BiasAdd/ReadVariableOp:value:0*
T0*'
_output_shapes
:���������z
sequential/output/SigmoidSigmoid"sequential/output/BiasAdd:output:0*
T0*'
_output_shapes
:���������l
IdentityIdentitysequential/output/Sigmoid:y:0^NoOp*
T0*'
_output_shapes
:����������
NoOpNoOp.^sequential/Block0_Conv/BiasAdd/ReadVariableOp-^sequential/Block0_Conv/Conv2D/ReadVariableOp7^sequential/Block0_Norm/FusedBatchNormV3/ReadVariableOp9^sequential/Block0_Norm/FusedBatchNormV3/ReadVariableOp_1&^sequential/Block0_Norm/ReadVariableOp(^sequential/Block0_Norm/ReadVariableOp_1.^sequential/Block1_Conv/BiasAdd/ReadVariableOp-^sequential/Block1_Conv/Conv2D/ReadVariableOp7^sequential/Block1_Norm/FusedBatchNormV3/ReadVariableOp9^sequential/Block1_Norm/FusedBatchNormV3/ReadVariableOp_1&^sequential/Block1_Norm/ReadVariableOp(^sequential/Block1_Norm/ReadVariableOp_1.^sequential/Block2_Conv/BiasAdd/ReadVariableOp-^sequential/Block2_Conv/Conv2D/ReadVariableOp7^sequential/Block2_Norm/FusedBatchNormV3/ReadVariableOp9^sequential/Block2_Norm/FusedBatchNormV3/ReadVariableOp_1&^sequential/Block2_Norm/ReadVariableOp(^sequential/Block2_Norm/ReadVariableOp_1.^sequential/Block3_Conv/BiasAdd/ReadVariableOp-^sequential/Block3_Conv/Conv2D/ReadVariableOp7^sequential/Block3_Norm/FusedBatchNormV3/ReadVariableOp9^sequential/Block3_Norm/FusedBatchNormV3/ReadVariableOp_1&^sequential/Block3_Norm/ReadVariableOp(^sequential/Block3_Norm/ReadVariableOp_1.^sequential/Final_Dense/BiasAdd/ReadVariableOp-^sequential/Final_Dense/MatMul/ReadVariableOp)^sequential/output/BiasAdd/ReadVariableOp(^sequential/output/MatMul/ReadVariableOp*"
_acd_function_control_output(*
_output_shapes
 "
identityIdentity:output:0*(
_construction_contextkEagerRuntime*h
_input_shapesW
U:�����������: : : : : : : : : : : : : : : : : : : : : : : : : : : : 2^
-sequential/Block0_Conv/BiasAdd/ReadVariableOp-sequential/Block0_Conv/BiasAdd/ReadVariableOp2\
,sequential/Block0_Conv/Conv2D/ReadVariableOp,sequential/Block0_Conv/Conv2D/ReadVariableOp2p
6sequential/Block0_Norm/FusedBatchNormV3/ReadVariableOp6sequential/Block0_Norm/FusedBatchNormV3/ReadVariableOp2t
8sequential/Block0_Norm/FusedBatchNormV3/ReadVariableOp_18sequential/Block0_Norm/FusedBatchNormV3/ReadVariableOp_12N
%sequential/Block0_Norm/ReadVariableOp%sequential/Block0_Norm/ReadVariableOp2R
'sequential/Block0_Norm/ReadVariableOp_1'sequential/Block0_Norm/ReadVariableOp_12^
-sequential/Block1_Conv/BiasAdd/ReadVariableOp-sequential/Block1_Conv/BiasAdd/ReadVariableOp2\
,sequential/Block1_Conv/Conv2D/ReadVariableOp,sequential/Block1_Conv/Conv2D/ReadVariableOp2p
6sequential/Block1_Norm/FusedBatchNormV3/ReadVariableOp6sequential/Block1_Norm/FusedBatchNormV3/ReadVariableOp2t
8sequential/Block1_Norm/FusedBatchNormV3/ReadVariableOp_18sequential/Block1_Norm/FusedBatchNormV3/ReadVariableOp_12N
%sequential/Block1_Norm/ReadVariableOp%sequential/Block1_Norm/ReadVariableOp2R
'sequential/Block1_Norm/ReadVariableOp_1'sequential/Block1_Norm/ReadVariableOp_12^
-sequential/Block2_Conv/BiasAdd/ReadVariableOp-sequential/Block2_Conv/BiasAdd/ReadVariableOp2\
,sequential/Block2_Conv/Conv2D/ReadVariableOp,sequential/Block2_Conv/Conv2D/ReadVariableOp2p
6sequential/Block2_Norm/FusedBatchNormV3/ReadVariableOp6sequential/Block2_Norm/FusedBatchNormV3/ReadVariableOp2t
8sequential/Block2_Norm/FusedBatchNormV3/ReadVariableOp_18sequential/Block2_Norm/FusedBatchNormV3/ReadVariableOp_12N
%sequential/Block2_Norm/ReadVariableOp%sequential/Block2_Norm/ReadVariableOp2R
'sequential/Block2_Norm/ReadVariableOp_1'sequential/Block2_Norm/ReadVariableOp_12^
-sequential/Block3_Conv/BiasAdd/ReadVariableOp-sequential/Block3_Conv/BiasAdd/ReadVariableOp2\
,sequential/Block3_Conv/Conv2D/ReadVariableOp,sequential/Block3_Conv/Conv2D/ReadVariableOp2p
6sequential/Block3_Norm/FusedBatchNormV3/ReadVariableOp6sequential/Block3_Norm/FusedBatchNormV3/ReadVariableOp2t
8sequential/Block3_Norm/FusedBatchNormV3/ReadVariableOp_18sequential/Block3_Norm/FusedBatchNormV3/ReadVariableOp_12N
%sequential/Block3_Norm/ReadVariableOp%sequential/Block3_Norm/ReadVariableOp2R
'sequential/Block3_Norm/ReadVariableOp_1'sequential/Block3_Norm/ReadVariableOp_12^
-sequential/Final_Dense/BiasAdd/ReadVariableOp-sequential/Final_Dense/BiasAdd/ReadVariableOp2\
,sequential/Final_Dense/MatMul/ReadVariableOp,sequential/Final_Dense/MatMul/ReadVariableOp2T
(sequential/output/BiasAdd/ReadVariableOp(sequential/output/BiasAdd/ReadVariableOp2R
'sequential/output/MatMul/ReadVariableOp'sequential/output/MatMul/ReadVariableOp:Z V
1
_output_shapes
:�����������
!
_user_specified_name	input_1
�
a
E__inference_Block3_Pool_layer_call_and_return_conditional_losses_7393

inputs
identity�
MaxPoolMaxPoolinputs*J
_output_shapes8
6:4������������������������������������*
ksize
*
paddingVALID*
strides
{
IdentityIdentityMaxPool:output:0*
T0*J
_output_shapes8
6:4������������������������������������"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*I
_input_shapes8
6:4������������������������������������:r n
J
_output_shapes8
6:4������������������������������������
 
_user_specified_nameinputs
�
f
J__inference_GlobalMaxPooling_layer_call_and_return_conditional_losses_8937

inputs
identityf
Max/reduction_indicesConst*
_output_shapes
:*
dtype0*
valueB"      m
MaxMaxinputsMax/reduction_indices:output:0*
T0*0
_output_shapes
:������������������]
IdentityIdentityMax:output:0*
T0*0
_output_shapes
:������������������"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*I
_input_shapes8
6:4������������������������������������:r n
J
_output_shapes8
6:4������������������������������������
 
_user_specified_nameinputs
�
�
%__inference_output_layer_call_fn_8966

inputs
unknown:	�
	unknown_0:
identity��StatefulPartitionedCall�
StatefulPartitionedCallStatefulPartitionedCallinputsunknown	unknown_0*
Tin
2*
Tout
2*
_collective_manager_ids
 *'
_output_shapes
:���������*$
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8� *I
fDRB
@__inference_output_layer_call_and_return_conditional_losses_7617o
IdentityIdentity StatefulPartitionedCall:output:0^NoOp*
T0*'
_output_shapes
:���������`
NoOpNoOp^StatefulPartitionedCall*"
_acd_function_control_output(*
_output_shapes
 "
identityIdentity:output:0*(
_construction_contextkEagerRuntime*+
_input_shapes
:����������: : 22
StatefulPartitionedCallStatefulPartitionedCall:P L
(
_output_shapes
:����������
 
_user_specified_nameinputs
�
�
*__inference_Block0_Norm_layer_call_fn_8601

inputs
unknown:
	unknown_0:
	unknown_1:
	unknown_2:
identity��StatefulPartitionedCall�
StatefulPartitionedCallStatefulPartitionedCallinputsunknown	unknown_0	unknown_1	unknown_2*
Tin	
2*
Tout
2*
_collective_manager_ids
 *A
_output_shapes/
-:+���������������������������*&
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8� *N
fIRG
E__inference_Block0_Norm_layer_call_and_return_conditional_losses_7190�
IdentityIdentity StatefulPartitionedCall:output:0^NoOp*
T0*A
_output_shapes/
-:+���������������������������`
NoOpNoOp^StatefulPartitionedCall*"
_acd_function_control_output(*
_output_shapes
 "
identityIdentity:output:0*(
_construction_contextkEagerRuntime*H
_input_shapes7
5:+���������������������������: : : : 22
StatefulPartitionedCallStatefulPartitionedCall:i e
A
_output_shapes/
-:+���������������������������
 
_user_specified_nameinputs
�
F
*__inference_Block0_Pool_layer_call_fn_8583

inputs
identity�
PartitionedCallPartitionedCallinputs*
Tin
2*
Tout
2*
_collective_manager_ids
 *J
_output_shapes8
6:4������������������������������������* 
_read_only_resource_inputs
 *0
config_proto 

CPU

GPU2*0J 8� *N
fIRG
E__inference_Block0_Pool_layer_call_and_return_conditional_losses_7165�
IdentityIdentityPartitionedCall:output:0*
T0*J
_output_shapes8
6:4������������������������������������"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*I
_input_shapes8
6:4������������������������������������:r n
J
_output_shapes8
6:4������������������������������������
 
_user_specified_nameinputs
�
�
E__inference_Block0_Conv_layer_call_and_return_conditional_losses_7491

inputs8
conv2d_readvariableop_resource:-
biasadd_readvariableop_resource:
identity��BiasAdd/ReadVariableOp�Conv2D/ReadVariableOp|
Conv2D/ReadVariableOpReadVariableOpconv2d_readvariableop_resource*&
_output_shapes
:*
dtype0�
Conv2DConv2DinputsConv2D/ReadVariableOp:value:0*
T0*1
_output_shapes
:�����������*
paddingSAME*
strides
r
BiasAdd/ReadVariableOpReadVariableOpbiasadd_readvariableop_resource*
_output_shapes
:*
dtype0
BiasAddBiasAddConv2D:output:0BiasAdd/ReadVariableOp:value:0*
T0*1
_output_shapes
:�����������Z
ReluReluBiasAdd:output:0*
T0*1
_output_shapes
:�����������k
IdentityIdentityRelu:activations:0^NoOp*
T0*1
_output_shapes
:�����������w
NoOpNoOp^BiasAdd/ReadVariableOp^Conv2D/ReadVariableOp*"
_acd_function_control_output(*
_output_shapes
 "
identityIdentity:output:0*(
_construction_contextkEagerRuntime*4
_input_shapes#
!:�����������: : 20
BiasAdd/ReadVariableOpBiasAdd/ReadVariableOp2.
Conv2D/ReadVariableOpConv2D/ReadVariableOp:Y U
1
_output_shapes
:�����������
 
_user_specified_nameinputs
�
�
E__inference_Block1_Norm_layer_call_and_return_conditional_losses_7297

inputs%
readvariableop_resource: '
readvariableop_1_resource: 6
(fusedbatchnormv3_readvariableop_resource: 8
*fusedbatchnormv3_readvariableop_1_resource: 
identity��AssignNewValue�AssignNewValue_1�FusedBatchNormV3/ReadVariableOp�!FusedBatchNormV3/ReadVariableOp_1�ReadVariableOp�ReadVariableOp_1b
ReadVariableOpReadVariableOpreadvariableop_resource*
_output_shapes
: *
dtype0f
ReadVariableOp_1ReadVariableOpreadvariableop_1_resource*
_output_shapes
: *
dtype0�
FusedBatchNormV3/ReadVariableOpReadVariableOp(fusedbatchnormv3_readvariableop_resource*
_output_shapes
: *
dtype0�
!FusedBatchNormV3/ReadVariableOp_1ReadVariableOp*fusedbatchnormv3_readvariableop_1_resource*
_output_shapes
: *
dtype0�
FusedBatchNormV3FusedBatchNormV3inputsReadVariableOp:value:0ReadVariableOp_1:value:0'FusedBatchNormV3/ReadVariableOp:value:0)FusedBatchNormV3/ReadVariableOp_1:value:0*
T0*
U0*]
_output_shapesK
I:+��������������������������� : : : : :*
epsilon%o�:*
exponential_avg_factor%
�#<�
AssignNewValueAssignVariableOp(fusedbatchnormv3_readvariableop_resourceFusedBatchNormV3:batch_mean:0 ^FusedBatchNormV3/ReadVariableOp*
_output_shapes
 *
dtype0�
AssignNewValue_1AssignVariableOp*fusedbatchnormv3_readvariableop_1_resource!FusedBatchNormV3:batch_variance:0"^FusedBatchNormV3/ReadVariableOp_1*
_output_shapes
 *
dtype0}
IdentityIdentityFusedBatchNormV3:y:0^NoOp*
T0*A
_output_shapes/
-:+��������������������������� �
NoOpNoOp^AssignNewValue^AssignNewValue_1 ^FusedBatchNormV3/ReadVariableOp"^FusedBatchNormV3/ReadVariableOp_1^ReadVariableOp^ReadVariableOp_1*"
_acd_function_control_output(*
_output_shapes
 "
identityIdentity:output:0*(
_construction_contextkEagerRuntime*H
_input_shapes7
5:+��������������������������� : : : : 2 
AssignNewValueAssignNewValue2$
AssignNewValue_1AssignNewValue_12B
FusedBatchNormV3/ReadVariableOpFusedBatchNormV3/ReadVariableOp2F
!FusedBatchNormV3/ReadVariableOp_1!FusedBatchNormV3/ReadVariableOp_12 
ReadVariableOpReadVariableOp2$
ReadVariableOp_1ReadVariableOp_1:i e
A
_output_shapes/
-:+��������������������������� 
 
_user_specified_nameinputs
�{
�
D__inference_sequential_layer_call_and_return_conditional_losses_8387

inputsD
*block0_conv_conv2d_readvariableop_resource:9
+block0_conv_biasadd_readvariableop_resource:1
#block0_norm_readvariableop_resource:3
%block0_norm_readvariableop_1_resource:B
4block0_norm_fusedbatchnormv3_readvariableop_resource:D
6block0_norm_fusedbatchnormv3_readvariableop_1_resource:D
*block1_conv_conv2d_readvariableop_resource: 9
+block1_conv_biasadd_readvariableop_resource: 1
#block1_norm_readvariableop_resource: 3
%block1_norm_readvariableop_1_resource: B
4block1_norm_fusedbatchnormv3_readvariableop_resource: D
6block1_norm_fusedbatchnormv3_readvariableop_1_resource: D
*block2_conv_conv2d_readvariableop_resource: @9
+block2_conv_biasadd_readvariableop_resource:@1
#block2_norm_readvariableop_resource:@3
%block2_norm_readvariableop_1_resource:@B
4block2_norm_fusedbatchnormv3_readvariableop_resource:@D
6block2_norm_fusedbatchnormv3_readvariableop_1_resource:@E
*block3_conv_conv2d_readvariableop_resource:@�:
+block3_conv_biasadd_readvariableop_resource:	�2
#block3_norm_readvariableop_resource:	�4
%block3_norm_readvariableop_1_resource:	�C
4block3_norm_fusedbatchnormv3_readvariableop_resource:	�E
6block3_norm_fusedbatchnormv3_readvariableop_1_resource:	�>
*final_dense_matmul_readvariableop_resource:
��:
+final_dense_biasadd_readvariableop_resource:	�8
%output_matmul_readvariableop_resource:	�4
&output_biasadd_readvariableop_resource:
identity��"Block0_Conv/BiasAdd/ReadVariableOp�!Block0_Conv/Conv2D/ReadVariableOp�+Block0_Norm/FusedBatchNormV3/ReadVariableOp�-Block0_Norm/FusedBatchNormV3/ReadVariableOp_1�Block0_Norm/ReadVariableOp�Block0_Norm/ReadVariableOp_1�"Block1_Conv/BiasAdd/ReadVariableOp�!Block1_Conv/Conv2D/ReadVariableOp�+Block1_Norm/FusedBatchNormV3/ReadVariableOp�-Block1_Norm/FusedBatchNormV3/ReadVariableOp_1�Block1_Norm/ReadVariableOp�Block1_Norm/ReadVariableOp_1�"Block2_Conv/BiasAdd/ReadVariableOp�!Block2_Conv/Conv2D/ReadVariableOp�+Block2_Norm/FusedBatchNormV3/ReadVariableOp�-Block2_Norm/FusedBatchNormV3/ReadVariableOp_1�Block2_Norm/ReadVariableOp�Block2_Norm/ReadVariableOp_1�"Block3_Conv/BiasAdd/ReadVariableOp�!Block3_Conv/Conv2D/ReadVariableOp�+Block3_Norm/FusedBatchNormV3/ReadVariableOp�-Block3_Norm/FusedBatchNormV3/ReadVariableOp_1�Block3_Norm/ReadVariableOp�Block3_Norm/ReadVariableOp_1�"Final_Dense/BiasAdd/ReadVariableOp�!Final_Dense/MatMul/ReadVariableOp�output/BiasAdd/ReadVariableOp�output/MatMul/ReadVariableOp�
!Block0_Conv/Conv2D/ReadVariableOpReadVariableOp*block0_conv_conv2d_readvariableop_resource*&
_output_shapes
:*
dtype0�
Block0_Conv/Conv2DConv2Dinputs)Block0_Conv/Conv2D/ReadVariableOp:value:0*
T0*1
_output_shapes
:�����������*
paddingSAME*
strides
�
"Block0_Conv/BiasAdd/ReadVariableOpReadVariableOp+block0_conv_biasadd_readvariableop_resource*
_output_shapes
:*
dtype0�
Block0_Conv/BiasAddBiasAddBlock0_Conv/Conv2D:output:0*Block0_Conv/BiasAdd/ReadVariableOp:value:0*
T0*1
_output_shapes
:�����������r
Block0_Conv/ReluReluBlock0_Conv/BiasAdd:output:0*
T0*1
_output_shapes
:������������
Block0_Pool/MaxPoolMaxPoolBlock0_Conv/Relu:activations:0*/
_output_shapes
:���������@V*
ksize
*
paddingVALID*
strides
z
Block0_Norm/ReadVariableOpReadVariableOp#block0_norm_readvariableop_resource*
_output_shapes
:*
dtype0~
Block0_Norm/ReadVariableOp_1ReadVariableOp%block0_norm_readvariableop_1_resource*
_output_shapes
:*
dtype0�
+Block0_Norm/FusedBatchNormV3/ReadVariableOpReadVariableOp4block0_norm_fusedbatchnormv3_readvariableop_resource*
_output_shapes
:*
dtype0�
-Block0_Norm/FusedBatchNormV3/ReadVariableOp_1ReadVariableOp6block0_norm_fusedbatchnormv3_readvariableop_1_resource*
_output_shapes
:*
dtype0�
Block0_Norm/FusedBatchNormV3FusedBatchNormV3Block0_Pool/MaxPool:output:0"Block0_Norm/ReadVariableOp:value:0$Block0_Norm/ReadVariableOp_1:value:03Block0_Norm/FusedBatchNormV3/ReadVariableOp:value:05Block0_Norm/FusedBatchNormV3/ReadVariableOp_1:value:0*
T0*
U0*K
_output_shapes9
7:���������@V:::::*
epsilon%o�:*
is_training( �
!Block1_Conv/Conv2D/ReadVariableOpReadVariableOp*block1_conv_conv2d_readvariableop_resource*&
_output_shapes
: *
dtype0�
Block1_Conv/Conv2DConv2D Block0_Norm/FusedBatchNormV3:y:0)Block1_Conv/Conv2D/ReadVariableOp:value:0*
T0*/
_output_shapes
:���������@V *
paddingSAME*
strides
�
"Block1_Conv/BiasAdd/ReadVariableOpReadVariableOp+block1_conv_biasadd_readvariableop_resource*
_output_shapes
: *
dtype0�
Block1_Conv/BiasAddBiasAddBlock1_Conv/Conv2D:output:0*Block1_Conv/BiasAdd/ReadVariableOp:value:0*
T0*/
_output_shapes
:���������@V p
Block1_Conv/ReluReluBlock1_Conv/BiasAdd:output:0*
T0*/
_output_shapes
:���������@V �
Block1_Pool/MaxPoolMaxPoolBlock1_Conv/Relu:activations:0*/
_output_shapes
:��������� + *
ksize
*
paddingVALID*
strides
z
Block1_Norm/ReadVariableOpReadVariableOp#block1_norm_readvariableop_resource*
_output_shapes
: *
dtype0~
Block1_Norm/ReadVariableOp_1ReadVariableOp%block1_norm_readvariableop_1_resource*
_output_shapes
: *
dtype0�
+Block1_Norm/FusedBatchNormV3/ReadVariableOpReadVariableOp4block1_norm_fusedbatchnormv3_readvariableop_resource*
_output_shapes
: *
dtype0�
-Block1_Norm/FusedBatchNormV3/ReadVariableOp_1ReadVariableOp6block1_norm_fusedbatchnormv3_readvariableop_1_resource*
_output_shapes
: *
dtype0�
Block1_Norm/FusedBatchNormV3FusedBatchNormV3Block1_Pool/MaxPool:output:0"Block1_Norm/ReadVariableOp:value:0$Block1_Norm/ReadVariableOp_1:value:03Block1_Norm/FusedBatchNormV3/ReadVariableOp:value:05Block1_Norm/FusedBatchNormV3/ReadVariableOp_1:value:0*
T0*
U0*K
_output_shapes9
7:��������� + : : : : :*
epsilon%o�:*
is_training( �
!Block2_Conv/Conv2D/ReadVariableOpReadVariableOp*block2_conv_conv2d_readvariableop_resource*&
_output_shapes
: @*
dtype0�
Block2_Conv/Conv2DConv2D Block1_Norm/FusedBatchNormV3:y:0)Block2_Conv/Conv2D/ReadVariableOp:value:0*
T0*/
_output_shapes
:��������� +@*
paddingSAME*
strides
�
"Block2_Conv/BiasAdd/ReadVariableOpReadVariableOp+block2_conv_biasadd_readvariableop_resource*
_output_shapes
:@*
dtype0�
Block2_Conv/BiasAddBiasAddBlock2_Conv/Conv2D:output:0*Block2_Conv/BiasAdd/ReadVariableOp:value:0*
T0*/
_output_shapes
:��������� +@p
Block2_Conv/ReluReluBlock2_Conv/BiasAdd:output:0*
T0*/
_output_shapes
:��������� +@�
Block2_Pool/MaxPoolMaxPoolBlock2_Conv/Relu:activations:0*/
_output_shapes
:���������@*
ksize
*
paddingVALID*
strides
z
Block2_Norm/ReadVariableOpReadVariableOp#block2_norm_readvariableop_resource*
_output_shapes
:@*
dtype0~
Block2_Norm/ReadVariableOp_1ReadVariableOp%block2_norm_readvariableop_1_resource*
_output_shapes
:@*
dtype0�
+Block2_Norm/FusedBatchNormV3/ReadVariableOpReadVariableOp4block2_norm_fusedbatchnormv3_readvariableop_resource*
_output_shapes
:@*
dtype0�
-Block2_Norm/FusedBatchNormV3/ReadVariableOp_1ReadVariableOp6block2_norm_fusedbatchnormv3_readvariableop_1_resource*
_output_shapes
:@*
dtype0�
Block2_Norm/FusedBatchNormV3FusedBatchNormV3Block2_Pool/MaxPool:output:0"Block2_Norm/ReadVariableOp:value:0$Block2_Norm/ReadVariableOp_1:value:03Block2_Norm/FusedBatchNormV3/ReadVariableOp:value:05Block2_Norm/FusedBatchNormV3/ReadVariableOp_1:value:0*
T0*
U0*K
_output_shapes9
7:���������@:@:@:@:@:*
epsilon%o�:*
is_training( �
!Block3_Conv/Conv2D/ReadVariableOpReadVariableOp*block3_conv_conv2d_readvariableop_resource*'
_output_shapes
:@�*
dtype0�
Block3_Conv/Conv2DConv2D Block2_Norm/FusedBatchNormV3:y:0)Block3_Conv/Conv2D/ReadVariableOp:value:0*
T0*0
_output_shapes
:����������*
paddingSAME*
strides
�
"Block3_Conv/BiasAdd/ReadVariableOpReadVariableOp+block3_conv_biasadd_readvariableop_resource*
_output_shapes	
:�*
dtype0�
Block3_Conv/BiasAddBiasAddBlock3_Conv/Conv2D:output:0*Block3_Conv/BiasAdd/ReadVariableOp:value:0*
T0*0
_output_shapes
:����������q
Block3_Conv/ReluReluBlock3_Conv/BiasAdd:output:0*
T0*0
_output_shapes
:�����������
Block3_Pool/MaxPoolMaxPoolBlock3_Conv/Relu:activations:0*0
_output_shapes
:���������
�*
ksize
*
paddingVALID*
strides
{
Block3_Norm/ReadVariableOpReadVariableOp#block3_norm_readvariableop_resource*
_output_shapes	
:�*
dtype0
Block3_Norm/ReadVariableOp_1ReadVariableOp%block3_norm_readvariableop_1_resource*
_output_shapes	
:�*
dtype0�
+Block3_Norm/FusedBatchNormV3/ReadVariableOpReadVariableOp4block3_norm_fusedbatchnormv3_readvariableop_resource*
_output_shapes	
:�*
dtype0�
-Block3_Norm/FusedBatchNormV3/ReadVariableOp_1ReadVariableOp6block3_norm_fusedbatchnormv3_readvariableop_1_resource*
_output_shapes	
:�*
dtype0�
Block3_Norm/FusedBatchNormV3FusedBatchNormV3Block3_Pool/MaxPool:output:0"Block3_Norm/ReadVariableOp:value:0$Block3_Norm/ReadVariableOp_1:value:03Block3_Norm/FusedBatchNormV3/ReadVariableOp:value:05Block3_Norm/FusedBatchNormV3/ReadVariableOp_1:value:0*
T0*
U0*P
_output_shapes>
<:���������
�:�:�:�:�:*
epsilon%o�:*
is_training( w
&GlobalMaxPooling/Max/reduction_indicesConst*
_output_shapes
:*
dtype0*
valueB"      �
GlobalMaxPooling/MaxMax Block3_Norm/FusedBatchNormV3:y:0/GlobalMaxPooling/Max/reduction_indices:output:0*
T0*(
_output_shapes
:�����������
!Final_Dense/MatMul/ReadVariableOpReadVariableOp*final_dense_matmul_readvariableop_resource* 
_output_shapes
:
��*
dtype0�
Final_Dense/MatMulMatMulGlobalMaxPooling/Max:output:0)Final_Dense/MatMul/ReadVariableOp:value:0*
T0*(
_output_shapes
:�����������
"Final_Dense/BiasAdd/ReadVariableOpReadVariableOp+final_dense_biasadd_readvariableop_resource*
_output_shapes	
:�*
dtype0�
Final_Dense/BiasAddBiasAddFinal_Dense/MatMul:product:0*Final_Dense/BiasAdd/ReadVariableOp:value:0*
T0*(
_output_shapes
:����������i
Final_Dense/ReluReluFinal_Dense/BiasAdd:output:0*
T0*(
_output_shapes
:�����������
output/MatMul/ReadVariableOpReadVariableOp%output_matmul_readvariableop_resource*
_output_shapes
:	�*
dtype0�
output/MatMulMatMulFinal_Dense/Relu:activations:0$output/MatMul/ReadVariableOp:value:0*
T0*'
_output_shapes
:����������
output/BiasAdd/ReadVariableOpReadVariableOp&output_biasadd_readvariableop_resource*
_output_shapes
:*
dtype0�
output/BiasAddBiasAddoutput/MatMul:product:0%output/BiasAdd/ReadVariableOp:value:0*
T0*'
_output_shapes
:���������d
output/SigmoidSigmoidoutput/BiasAdd:output:0*
T0*'
_output_shapes
:���������a
IdentityIdentityoutput/Sigmoid:y:0^NoOp*
T0*'
_output_shapes
:����������
NoOpNoOp#^Block0_Conv/BiasAdd/ReadVariableOp"^Block0_Conv/Conv2D/ReadVariableOp,^Block0_Norm/FusedBatchNormV3/ReadVariableOp.^Block0_Norm/FusedBatchNormV3/ReadVariableOp_1^Block0_Norm/ReadVariableOp^Block0_Norm/ReadVariableOp_1#^Block1_Conv/BiasAdd/ReadVariableOp"^Block1_Conv/Conv2D/ReadVariableOp,^Block1_Norm/FusedBatchNormV3/ReadVariableOp.^Block1_Norm/FusedBatchNormV3/ReadVariableOp_1^Block1_Norm/ReadVariableOp^Block1_Norm/ReadVariableOp_1#^Block2_Conv/BiasAdd/ReadVariableOp"^Block2_Conv/Conv2D/ReadVariableOp,^Block2_Norm/FusedBatchNormV3/ReadVariableOp.^Block2_Norm/FusedBatchNormV3/ReadVariableOp_1^Block2_Norm/ReadVariableOp^Block2_Norm/ReadVariableOp_1#^Block3_Conv/BiasAdd/ReadVariableOp"^Block3_Conv/Conv2D/ReadVariableOp,^Block3_Norm/FusedBatchNormV3/ReadVariableOp.^Block3_Norm/FusedBatchNormV3/ReadVariableOp_1^Block3_Norm/ReadVariableOp^Block3_Norm/ReadVariableOp_1#^Final_Dense/BiasAdd/ReadVariableOp"^Final_Dense/MatMul/ReadVariableOp^output/BiasAdd/ReadVariableOp^output/MatMul/ReadVariableOp*"
_acd_function_control_output(*
_output_shapes
 "
identityIdentity:output:0*(
_construction_contextkEagerRuntime*h
_input_shapesW
U:�����������: : : : : : : : : : : : : : : : : : : : : : : : : : : : 2H
"Block0_Conv/BiasAdd/ReadVariableOp"Block0_Conv/BiasAdd/ReadVariableOp2F
!Block0_Conv/Conv2D/ReadVariableOp!Block0_Conv/Conv2D/ReadVariableOp2Z
+Block0_Norm/FusedBatchNormV3/ReadVariableOp+Block0_Norm/FusedBatchNormV3/ReadVariableOp2^
-Block0_Norm/FusedBatchNormV3/ReadVariableOp_1-Block0_Norm/FusedBatchNormV3/ReadVariableOp_128
Block0_Norm/ReadVariableOpBlock0_Norm/ReadVariableOp2<
Block0_Norm/ReadVariableOp_1Block0_Norm/ReadVariableOp_12H
"Block1_Conv/BiasAdd/ReadVariableOp"Block1_Conv/BiasAdd/ReadVariableOp2F
!Block1_Conv/Conv2D/ReadVariableOp!Block1_Conv/Conv2D/ReadVariableOp2Z
+Block1_Norm/FusedBatchNormV3/ReadVariableOp+Block1_Norm/FusedBatchNormV3/ReadVariableOp2^
-Block1_Norm/FusedBatchNormV3/ReadVariableOp_1-Block1_Norm/FusedBatchNormV3/ReadVariableOp_128
Block1_Norm/ReadVariableOpBlock1_Norm/ReadVariableOp2<
Block1_Norm/ReadVariableOp_1Block1_Norm/ReadVariableOp_12H
"Block2_Conv/BiasAdd/ReadVariableOp"Block2_Conv/BiasAdd/ReadVariableOp2F
!Block2_Conv/Conv2D/ReadVariableOp!Block2_Conv/Conv2D/ReadVariableOp2Z
+Block2_Norm/FusedBatchNormV3/ReadVariableOp+Block2_Norm/FusedBatchNormV3/ReadVariableOp2^
-Block2_Norm/FusedBatchNormV3/ReadVariableOp_1-Block2_Norm/FusedBatchNormV3/ReadVariableOp_128
Block2_Norm/ReadVariableOpBlock2_Norm/ReadVariableOp2<
Block2_Norm/ReadVariableOp_1Block2_Norm/ReadVariableOp_12H
"Block3_Conv/BiasAdd/ReadVariableOp"Block3_Conv/BiasAdd/ReadVariableOp2F
!Block3_Conv/Conv2D/ReadVariableOp!Block3_Conv/Conv2D/ReadVariableOp2Z
+Block3_Norm/FusedBatchNormV3/ReadVariableOp+Block3_Norm/FusedBatchNormV3/ReadVariableOp2^
-Block3_Norm/FusedBatchNormV3/ReadVariableOp_1-Block3_Norm/FusedBatchNormV3/ReadVariableOp_128
Block3_Norm/ReadVariableOpBlock3_Norm/ReadVariableOp2<
Block3_Norm/ReadVariableOp_1Block3_Norm/ReadVariableOp_12H
"Final_Dense/BiasAdd/ReadVariableOp"Final_Dense/BiasAdd/ReadVariableOp2F
!Final_Dense/MatMul/ReadVariableOp!Final_Dense/MatMul/ReadVariableOp2>
output/BiasAdd/ReadVariableOpoutput/BiasAdd/ReadVariableOp2<
output/MatMul/ReadVariableOpoutput/MatMul/ReadVariableOp:Y U
1
_output_shapes
:�����������
 
_user_specified_nameinputs
�
a
E__inference_Block1_Pool_layer_call_and_return_conditional_losses_7241

inputs
identity�
MaxPoolMaxPoolinputs*J
_output_shapes8
6:4������������������������������������*
ksize
*
paddingVALID*
strides
{
IdentityIdentityMaxPool:output:0*
T0*J
_output_shapes8
6:4������������������������������������"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*I
_input_shapes8
6:4������������������������������������:r n
J
_output_shapes8
6:4������������������������������������
 
_user_specified_nameinputs
�
F
*__inference_Block3_Pool_layer_call_fn_8859

inputs
identity�
PartitionedCallPartitionedCallinputs*
Tin
2*
Tout
2*
_collective_manager_ids
 *J
_output_shapes8
6:4������������������������������������* 
_read_only_resource_inputs
 *0
config_proto 

CPU

GPU2*0J 8� *N
fIRG
E__inference_Block3_Pool_layer_call_and_return_conditional_losses_7393�
IdentityIdentityPartitionedCall:output:0*
T0*J
_output_shapes8
6:4������������������������������������"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*I
_input_shapes8
6:4������������������������������������:r n
J
_output_shapes8
6:4������������������������������������
 
_user_specified_nameinputs
�
�
"__inference_signature_wrapper_8558
input_1!
unknown:
	unknown_0:
	unknown_1:
	unknown_2:
	unknown_3:
	unknown_4:#
	unknown_5: 
	unknown_6: 
	unknown_7: 
	unknown_8: 
	unknown_9: 

unknown_10: $

unknown_11: @

unknown_12:@

unknown_13:@

unknown_14:@

unknown_15:@

unknown_16:@%

unknown_17:@�

unknown_18:	�

unknown_19:	�

unknown_20:	�

unknown_21:	�

unknown_22:	�

unknown_23:
��

unknown_24:	�

unknown_25:	�

unknown_26:
identity��StatefulPartitionedCall�
StatefulPartitionedCallStatefulPartitionedCallinput_1unknown	unknown_0	unknown_1	unknown_2	unknown_3	unknown_4	unknown_5	unknown_6	unknown_7	unknown_8	unknown_9
unknown_10
unknown_11
unknown_12
unknown_13
unknown_14
unknown_15
unknown_16
unknown_17
unknown_18
unknown_19
unknown_20
unknown_21
unknown_22
unknown_23
unknown_24
unknown_25
unknown_26*(
Tin!
2*
Tout
2*
_collective_manager_ids
 *'
_output_shapes
:���������*>
_read_only_resource_inputs 
	
*0
config_proto 

CPU

GPU2*0J 8� *(
f#R!
__inference__wrapped_model_7156o
IdentityIdentity StatefulPartitionedCall:output:0^NoOp*
T0*'
_output_shapes
:���������`
NoOpNoOp^StatefulPartitionedCall*"
_acd_function_control_output(*
_output_shapes
 "
identityIdentity:output:0*(
_construction_contextkEagerRuntime*h
_input_shapesW
U:�����������: : : : : : : : : : : : : : : : : : : : : : : : : : : : 22
StatefulPartitionedCallStatefulPartitionedCall:Z V
1
_output_shapes
:�����������
!
_user_specified_name	input_1
�
F
*__inference_Block2_Pool_layer_call_fn_8767

inputs
identity�
PartitionedCallPartitionedCallinputs*
Tin
2*
Tout
2*
_collective_manager_ids
 *J
_output_shapes8
6:4������������������������������������* 
_read_only_resource_inputs
 *0
config_proto 

CPU

GPU2*0J 8� *N
fIRG
E__inference_Block2_Pool_layer_call_and_return_conditional_losses_7317�
IdentityIdentityPartitionedCall:output:0*
T0*J
_output_shapes8
6:4������������������������������������"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*I
_input_shapes8
6:4������������������������������������:r n
J
_output_shapes8
6:4������������������������������������
 
_user_specified_nameinputs
�
�
*__inference_Block2_Norm_layer_call_fn_8798

inputs
unknown:@
	unknown_0:@
	unknown_1:@
	unknown_2:@
identity��StatefulPartitionedCall�
StatefulPartitionedCallStatefulPartitionedCallinputsunknown	unknown_0	unknown_1	unknown_2*
Tin	
2*
Tout
2*
_collective_manager_ids
 *A
_output_shapes/
-:+���������������������������@*$
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8� *N
fIRG
E__inference_Block2_Norm_layer_call_and_return_conditional_losses_7373�
IdentityIdentity StatefulPartitionedCall:output:0^NoOp*
T0*A
_output_shapes/
-:+���������������������������@`
NoOpNoOp^StatefulPartitionedCall*"
_acd_function_control_output(*
_output_shapes
 "
identityIdentity:output:0*(
_construction_contextkEagerRuntime*H
_input_shapes7
5:+���������������������������@: : : : 22
StatefulPartitionedCallStatefulPartitionedCall:i e
A
_output_shapes/
-:+���������������������������@
 
_user_specified_nameinputs
�
�
*__inference_Block3_Conv_layer_call_fn_8843

inputs"
unknown:@�
	unknown_0:	�
identity��StatefulPartitionedCall�
StatefulPartitionedCallStatefulPartitionedCallinputsunknown	unknown_0*
Tin
2*
Tout
2*
_collective_manager_ids
 *0
_output_shapes
:����������*$
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8� *N
fIRG
E__inference_Block3_Conv_layer_call_and_return_conditional_losses_7572x
IdentityIdentity StatefulPartitionedCall:output:0^NoOp*
T0*0
_output_shapes
:����������`
NoOpNoOp^StatefulPartitionedCall*"
_acd_function_control_output(*
_output_shapes
 "
identityIdentity:output:0*(
_construction_contextkEagerRuntime*2
_input_shapes!
:���������@: : 22
StatefulPartitionedCallStatefulPartitionedCall:W S
/
_output_shapes
:���������@
 
_user_specified_nameinputs
�
�
E__inference_Block3_Norm_layer_call_and_return_conditional_losses_8926

inputs&
readvariableop_resource:	�(
readvariableop_1_resource:	�7
(fusedbatchnormv3_readvariableop_resource:	�9
*fusedbatchnormv3_readvariableop_1_resource:	�
identity��AssignNewValue�AssignNewValue_1�FusedBatchNormV3/ReadVariableOp�!FusedBatchNormV3/ReadVariableOp_1�ReadVariableOp�ReadVariableOp_1c
ReadVariableOpReadVariableOpreadvariableop_resource*
_output_shapes	
:�*
dtype0g
ReadVariableOp_1ReadVariableOpreadvariableop_1_resource*
_output_shapes	
:�*
dtype0�
FusedBatchNormV3/ReadVariableOpReadVariableOp(fusedbatchnormv3_readvariableop_resource*
_output_shapes	
:�*
dtype0�
!FusedBatchNormV3/ReadVariableOp_1ReadVariableOp*fusedbatchnormv3_readvariableop_1_resource*
_output_shapes	
:�*
dtype0�
FusedBatchNormV3FusedBatchNormV3inputsReadVariableOp:value:0ReadVariableOp_1:value:0'FusedBatchNormV3/ReadVariableOp:value:0)FusedBatchNormV3/ReadVariableOp_1:value:0*
T0*
U0*b
_output_shapesP
N:,����������������������������:�:�:�:�:*
epsilon%o�:*
exponential_avg_factor%
�#<�
AssignNewValueAssignVariableOp(fusedbatchnormv3_readvariableop_resourceFusedBatchNormV3:batch_mean:0 ^FusedBatchNormV3/ReadVariableOp*
_output_shapes
 *
dtype0�
AssignNewValue_1AssignVariableOp*fusedbatchnormv3_readvariableop_1_resource!FusedBatchNormV3:batch_variance:0"^FusedBatchNormV3/ReadVariableOp_1*
_output_shapes
 *
dtype0~
IdentityIdentityFusedBatchNormV3:y:0^NoOp*
T0*B
_output_shapes0
.:,�����������������������������
NoOpNoOp^AssignNewValue^AssignNewValue_1 ^FusedBatchNormV3/ReadVariableOp"^FusedBatchNormV3/ReadVariableOp_1^ReadVariableOp^ReadVariableOp_1*"
_acd_function_control_output(*
_output_shapes
 "
identityIdentity:output:0*(
_construction_contextkEagerRuntime*I
_input_shapes8
6:,����������������������������: : : : 2 
AssignNewValueAssignNewValue2$
AssignNewValue_1AssignNewValue_12B
FusedBatchNormV3/ReadVariableOpFusedBatchNormV3/ReadVariableOp2F
!FusedBatchNormV3/ReadVariableOp_1!FusedBatchNormV3/ReadVariableOp_12 
ReadVariableOpReadVariableOp2$
ReadVariableOp_1ReadVariableOp_1:j f
B
_output_shapes0
.:,����������������������������
 
_user_specified_nameinputs
�
�
E__inference_Block3_Conv_layer_call_and_return_conditional_losses_7572

inputs9
conv2d_readvariableop_resource:@�.
biasadd_readvariableop_resource:	�
identity��BiasAdd/ReadVariableOp�Conv2D/ReadVariableOp}
Conv2D/ReadVariableOpReadVariableOpconv2d_readvariableop_resource*'
_output_shapes
:@�*
dtype0�
Conv2DConv2DinputsConv2D/ReadVariableOp:value:0*
T0*0
_output_shapes
:����������*
paddingSAME*
strides
s
BiasAdd/ReadVariableOpReadVariableOpbiasadd_readvariableop_resource*
_output_shapes	
:�*
dtype0~
BiasAddBiasAddConv2D:output:0BiasAdd/ReadVariableOp:value:0*
T0*0
_output_shapes
:����������Y
ReluReluBiasAdd:output:0*
T0*0
_output_shapes
:����������j
IdentityIdentityRelu:activations:0^NoOp*
T0*0
_output_shapes
:����������w
NoOpNoOp^BiasAdd/ReadVariableOp^Conv2D/ReadVariableOp*"
_acd_function_control_output(*
_output_shapes
 "
identityIdentity:output:0*(
_construction_contextkEagerRuntime*2
_input_shapes!
:���������@: : 20
BiasAdd/ReadVariableOpBiasAdd/ReadVariableOp2.
Conv2D/ReadVariableOpConv2D/ReadVariableOp:W S
/
_output_shapes
:���������@
 
_user_specified_nameinputs
�
�
E__inference_Block3_Norm_layer_call_and_return_conditional_losses_7449

inputs&
readvariableop_resource:	�(
readvariableop_1_resource:	�7
(fusedbatchnormv3_readvariableop_resource:	�9
*fusedbatchnormv3_readvariableop_1_resource:	�
identity��AssignNewValue�AssignNewValue_1�FusedBatchNormV3/ReadVariableOp�!FusedBatchNormV3/ReadVariableOp_1�ReadVariableOp�ReadVariableOp_1c
ReadVariableOpReadVariableOpreadvariableop_resource*
_output_shapes	
:�*
dtype0g
ReadVariableOp_1ReadVariableOpreadvariableop_1_resource*
_output_shapes	
:�*
dtype0�
FusedBatchNormV3/ReadVariableOpReadVariableOp(fusedbatchnormv3_readvariableop_resource*
_output_shapes	
:�*
dtype0�
!FusedBatchNormV3/ReadVariableOp_1ReadVariableOp*fusedbatchnormv3_readvariableop_1_resource*
_output_shapes	
:�*
dtype0�
FusedBatchNormV3FusedBatchNormV3inputsReadVariableOp:value:0ReadVariableOp_1:value:0'FusedBatchNormV3/ReadVariableOp:value:0)FusedBatchNormV3/ReadVariableOp_1:value:0*
T0*
U0*b
_output_shapesP
N:,����������������������������:�:�:�:�:*
epsilon%o�:*
exponential_avg_factor%
�#<�
AssignNewValueAssignVariableOp(fusedbatchnormv3_readvariableop_resourceFusedBatchNormV3:batch_mean:0 ^FusedBatchNormV3/ReadVariableOp*
_output_shapes
 *
dtype0�
AssignNewValue_1AssignVariableOp*fusedbatchnormv3_readvariableop_1_resource!FusedBatchNormV3:batch_variance:0"^FusedBatchNormV3/ReadVariableOp_1*
_output_shapes
 *
dtype0~
IdentityIdentityFusedBatchNormV3:y:0^NoOp*
T0*B
_output_shapes0
.:,�����������������������������
NoOpNoOp^AssignNewValue^AssignNewValue_1 ^FusedBatchNormV3/ReadVariableOp"^FusedBatchNormV3/ReadVariableOp_1^ReadVariableOp^ReadVariableOp_1*"
_acd_function_control_output(*
_output_shapes
 "
identityIdentity:output:0*(
_construction_contextkEagerRuntime*I
_input_shapes8
6:,����������������������������: : : : 2 
AssignNewValueAssignNewValue2$
AssignNewValue_1AssignNewValue_12B
FusedBatchNormV3/ReadVariableOpFusedBatchNormV3/ReadVariableOp2F
!FusedBatchNormV3/ReadVariableOp_1!FusedBatchNormV3/ReadVariableOp_12 
ReadVariableOpReadVariableOp2$
ReadVariableOp_1ReadVariableOp_1:j f
B
_output_shapes0
.:,����������������������������
 
_user_specified_nameinputs
�
�
E__inference_Block0_Norm_layer_call_and_return_conditional_losses_7221

inputs%
readvariableop_resource:'
readvariableop_1_resource:6
(fusedbatchnormv3_readvariableop_resource:8
*fusedbatchnormv3_readvariableop_1_resource:
identity��AssignNewValue�AssignNewValue_1�FusedBatchNormV3/ReadVariableOp�!FusedBatchNormV3/ReadVariableOp_1�ReadVariableOp�ReadVariableOp_1b
ReadVariableOpReadVariableOpreadvariableop_resource*
_output_shapes
:*
dtype0f
ReadVariableOp_1ReadVariableOpreadvariableop_1_resource*
_output_shapes
:*
dtype0�
FusedBatchNormV3/ReadVariableOpReadVariableOp(fusedbatchnormv3_readvariableop_resource*
_output_shapes
:*
dtype0�
!FusedBatchNormV3/ReadVariableOp_1ReadVariableOp*fusedbatchnormv3_readvariableop_1_resource*
_output_shapes
:*
dtype0�
FusedBatchNormV3FusedBatchNormV3inputsReadVariableOp:value:0ReadVariableOp_1:value:0'FusedBatchNormV3/ReadVariableOp:value:0)FusedBatchNormV3/ReadVariableOp_1:value:0*
T0*
U0*]
_output_shapesK
I:+���������������������������:::::*
epsilon%o�:*
exponential_avg_factor%
�#<�
AssignNewValueAssignVariableOp(fusedbatchnormv3_readvariableop_resourceFusedBatchNormV3:batch_mean:0 ^FusedBatchNormV3/ReadVariableOp*
_output_shapes
 *
dtype0�
AssignNewValue_1AssignVariableOp*fusedbatchnormv3_readvariableop_1_resource!FusedBatchNormV3:batch_variance:0"^FusedBatchNormV3/ReadVariableOp_1*
_output_shapes
 *
dtype0}
IdentityIdentityFusedBatchNormV3:y:0^NoOp*
T0*A
_output_shapes/
-:+����������������������������
NoOpNoOp^AssignNewValue^AssignNewValue_1 ^FusedBatchNormV3/ReadVariableOp"^FusedBatchNormV3/ReadVariableOp_1^ReadVariableOp^ReadVariableOp_1*"
_acd_function_control_output(*
_output_shapes
 "
identityIdentity:output:0*(
_construction_contextkEagerRuntime*H
_input_shapes7
5:+���������������������������: : : : 2 
AssignNewValueAssignNewValue2$
AssignNewValue_1AssignNewValue_12B
FusedBatchNormV3/ReadVariableOpFusedBatchNormV3/ReadVariableOp2F
!FusedBatchNormV3/ReadVariableOp_1!FusedBatchNormV3/ReadVariableOp_12 
ReadVariableOpReadVariableOp2$
ReadVariableOp_1ReadVariableOp_1:i e
A
_output_shapes/
-:+���������������������������
 
_user_specified_nameinputs
�G
�
D__inference_sequential_layer_call_and_return_conditional_losses_8151
input_1*
block0_conv_8079:
block0_conv_8081:
block0_norm_8085:
block0_norm_8087:
block0_norm_8089:
block0_norm_8091:*
block1_conv_8094: 
block1_conv_8096: 
block1_norm_8100: 
block1_norm_8102: 
block1_norm_8104: 
block1_norm_8106: *
block2_conv_8109: @
block2_conv_8111:@
block2_norm_8115:@
block2_norm_8117:@
block2_norm_8119:@
block2_norm_8121:@+
block3_conv_8124:@�
block3_conv_8126:	�
block3_norm_8130:	�
block3_norm_8132:	�
block3_norm_8134:	�
block3_norm_8136:	�$
final_dense_8140:
��
final_dense_8142:	�
output_8145:	�
output_8147:
identity��#Block0_Conv/StatefulPartitionedCall�#Block0_Norm/StatefulPartitionedCall�#Block1_Conv/StatefulPartitionedCall�#Block1_Norm/StatefulPartitionedCall�#Block2_Conv/StatefulPartitionedCall�#Block2_Norm/StatefulPartitionedCall�#Block3_Conv/StatefulPartitionedCall�#Block3_Norm/StatefulPartitionedCall�#Final_Dense/StatefulPartitionedCall�output/StatefulPartitionedCall�
#Block0_Conv/StatefulPartitionedCallStatefulPartitionedCallinput_1block0_conv_8079block0_conv_8081*
Tin
2*
Tout
2*
_collective_manager_ids
 *1
_output_shapes
:�����������*$
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8� *N
fIRG
E__inference_Block0_Conv_layer_call_and_return_conditional_losses_7491�
Block0_Pool/PartitionedCallPartitionedCall,Block0_Conv/StatefulPartitionedCall:output:0*
Tin
2*
Tout
2*
_collective_manager_ids
 */
_output_shapes
:���������@V* 
_read_only_resource_inputs
 *0
config_proto 

CPU

GPU2*0J 8� *N
fIRG
E__inference_Block0_Pool_layer_call_and_return_conditional_losses_7165�
#Block0_Norm/StatefulPartitionedCallStatefulPartitionedCall$Block0_Pool/PartitionedCall:output:0block0_norm_8085block0_norm_8087block0_norm_8089block0_norm_8091*
Tin	
2*
Tout
2*
_collective_manager_ids
 */
_output_shapes
:���������@V*$
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8� *N
fIRG
E__inference_Block0_Norm_layer_call_and_return_conditional_losses_7221�
#Block1_Conv/StatefulPartitionedCallStatefulPartitionedCall,Block0_Norm/StatefulPartitionedCall:output:0block1_conv_8094block1_conv_8096*
Tin
2*
Tout
2*
_collective_manager_ids
 */
_output_shapes
:���������@V *$
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8� *N
fIRG
E__inference_Block1_Conv_layer_call_and_return_conditional_losses_7518�
Block1_Pool/PartitionedCallPartitionedCall,Block1_Conv/StatefulPartitionedCall:output:0*
Tin
2*
Tout
2*
_collective_manager_ids
 */
_output_shapes
:��������� + * 
_read_only_resource_inputs
 *0
config_proto 

CPU

GPU2*0J 8� *N
fIRG
E__inference_Block1_Pool_layer_call_and_return_conditional_losses_7241�
#Block1_Norm/StatefulPartitionedCallStatefulPartitionedCall$Block1_Pool/PartitionedCall:output:0block1_norm_8100block1_norm_8102block1_norm_8104block1_norm_8106*
Tin	
2*
Tout
2*
_collective_manager_ids
 */
_output_shapes
:��������� + *$
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8� *N
fIRG
E__inference_Block1_Norm_layer_call_and_return_conditional_losses_7297�
#Block2_Conv/StatefulPartitionedCallStatefulPartitionedCall,Block1_Norm/StatefulPartitionedCall:output:0block2_conv_8109block2_conv_8111*
Tin
2*
Tout
2*
_collective_manager_ids
 */
_output_shapes
:��������� +@*$
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8� *N
fIRG
E__inference_Block2_Conv_layer_call_and_return_conditional_losses_7545�
Block2_Pool/PartitionedCallPartitionedCall,Block2_Conv/StatefulPartitionedCall:output:0*
Tin
2*
Tout
2*
_collective_manager_ids
 */
_output_shapes
:���������@* 
_read_only_resource_inputs
 *0
config_proto 

CPU

GPU2*0J 8� *N
fIRG
E__inference_Block2_Pool_layer_call_and_return_conditional_losses_7317�
#Block2_Norm/StatefulPartitionedCallStatefulPartitionedCall$Block2_Pool/PartitionedCall:output:0block2_norm_8115block2_norm_8117block2_norm_8119block2_norm_8121*
Tin	
2*
Tout
2*
_collective_manager_ids
 */
_output_shapes
:���������@*$
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8� *N
fIRG
E__inference_Block2_Norm_layer_call_and_return_conditional_losses_7373�
#Block3_Conv/StatefulPartitionedCallStatefulPartitionedCall,Block2_Norm/StatefulPartitionedCall:output:0block3_conv_8124block3_conv_8126*
Tin
2*
Tout
2*
_collective_manager_ids
 *0
_output_shapes
:����������*$
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8� *N
fIRG
E__inference_Block3_Conv_layer_call_and_return_conditional_losses_7572�
Block3_Pool/PartitionedCallPartitionedCall,Block3_Conv/StatefulPartitionedCall:output:0*
Tin
2*
Tout
2*
_collective_manager_ids
 *0
_output_shapes
:���������
�* 
_read_only_resource_inputs
 *0
config_proto 

CPU

GPU2*0J 8� *N
fIRG
E__inference_Block3_Pool_layer_call_and_return_conditional_losses_7393�
#Block3_Norm/StatefulPartitionedCallStatefulPartitionedCall$Block3_Pool/PartitionedCall:output:0block3_norm_8130block3_norm_8132block3_norm_8134block3_norm_8136*
Tin	
2*
Tout
2*
_collective_manager_ids
 *0
_output_shapes
:���������
�*$
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8� *N
fIRG
E__inference_Block3_Norm_layer_call_and_return_conditional_losses_7449�
 GlobalMaxPooling/PartitionedCallPartitionedCall,Block3_Norm/StatefulPartitionedCall:output:0*
Tin
2*
Tout
2*
_collective_manager_ids
 *(
_output_shapes
:����������* 
_read_only_resource_inputs
 *0
config_proto 

CPU

GPU2*0J 8� *S
fNRL
J__inference_GlobalMaxPooling_layer_call_and_return_conditional_losses_7470�
#Final_Dense/StatefulPartitionedCallStatefulPartitionedCall)GlobalMaxPooling/PartitionedCall:output:0final_dense_8140final_dense_8142*
Tin
2*
Tout
2*
_collective_manager_ids
 *(
_output_shapes
:����������*$
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8� *N
fIRG
E__inference_Final_Dense_layer_call_and_return_conditional_losses_7600�
output/StatefulPartitionedCallStatefulPartitionedCall,Final_Dense/StatefulPartitionedCall:output:0output_8145output_8147*
Tin
2*
Tout
2*
_collective_manager_ids
 *'
_output_shapes
:���������*$
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8� *I
fDRB
@__inference_output_layer_call_and_return_conditional_losses_7617v
IdentityIdentity'output/StatefulPartitionedCall:output:0^NoOp*
T0*'
_output_shapes
:����������
NoOpNoOp$^Block0_Conv/StatefulPartitionedCall$^Block0_Norm/StatefulPartitionedCall$^Block1_Conv/StatefulPartitionedCall$^Block1_Norm/StatefulPartitionedCall$^Block2_Conv/StatefulPartitionedCall$^Block2_Norm/StatefulPartitionedCall$^Block3_Conv/StatefulPartitionedCall$^Block3_Norm/StatefulPartitionedCall$^Final_Dense/StatefulPartitionedCall^output/StatefulPartitionedCall*"
_acd_function_control_output(*
_output_shapes
 "
identityIdentity:output:0*(
_construction_contextkEagerRuntime*h
_input_shapesW
U:�����������: : : : : : : : : : : : : : : : : : : : : : : : : : : : 2J
#Block0_Conv/StatefulPartitionedCall#Block0_Conv/StatefulPartitionedCall2J
#Block0_Norm/StatefulPartitionedCall#Block0_Norm/StatefulPartitionedCall2J
#Block1_Conv/StatefulPartitionedCall#Block1_Conv/StatefulPartitionedCall2J
#Block1_Norm/StatefulPartitionedCall#Block1_Norm/StatefulPartitionedCall2J
#Block2_Conv/StatefulPartitionedCall#Block2_Conv/StatefulPartitionedCall2J
#Block2_Norm/StatefulPartitionedCall#Block2_Norm/StatefulPartitionedCall2J
#Block3_Conv/StatefulPartitionedCall#Block3_Conv/StatefulPartitionedCall2J
#Block3_Norm/StatefulPartitionedCall#Block3_Norm/StatefulPartitionedCall2J
#Final_Dense/StatefulPartitionedCall#Final_Dense/StatefulPartitionedCall2@
output/StatefulPartitionedCalloutput/StatefulPartitionedCall:Z V
1
_output_shapes
:�����������
!
_user_specified_name	input_1
�
�
E__inference_Block0_Norm_layer_call_and_return_conditional_losses_8632

inputs%
readvariableop_resource:'
readvariableop_1_resource:6
(fusedbatchnormv3_readvariableop_resource:8
*fusedbatchnormv3_readvariableop_1_resource:
identity��FusedBatchNormV3/ReadVariableOp�!FusedBatchNormV3/ReadVariableOp_1�ReadVariableOp�ReadVariableOp_1b
ReadVariableOpReadVariableOpreadvariableop_resource*
_output_shapes
:*
dtype0f
ReadVariableOp_1ReadVariableOpreadvariableop_1_resource*
_output_shapes
:*
dtype0�
FusedBatchNormV3/ReadVariableOpReadVariableOp(fusedbatchnormv3_readvariableop_resource*
_output_shapes
:*
dtype0�
!FusedBatchNormV3/ReadVariableOp_1ReadVariableOp*fusedbatchnormv3_readvariableop_1_resource*
_output_shapes
:*
dtype0�
FusedBatchNormV3FusedBatchNormV3inputsReadVariableOp:value:0ReadVariableOp_1:value:0'FusedBatchNormV3/ReadVariableOp:value:0)FusedBatchNormV3/ReadVariableOp_1:value:0*
T0*
U0*]
_output_shapesK
I:+���������������������������:::::*
epsilon%o�:*
is_training( }
IdentityIdentityFusedBatchNormV3:y:0^NoOp*
T0*A
_output_shapes/
-:+����������������������������
NoOpNoOp ^FusedBatchNormV3/ReadVariableOp"^FusedBatchNormV3/ReadVariableOp_1^ReadVariableOp^ReadVariableOp_1*"
_acd_function_control_output(*
_output_shapes
 "
identityIdentity:output:0*(
_construction_contextkEagerRuntime*H
_input_shapes7
5:+���������������������������: : : : 2B
FusedBatchNormV3/ReadVariableOpFusedBatchNormV3/ReadVariableOp2F
!FusedBatchNormV3/ReadVariableOp_1!FusedBatchNormV3/ReadVariableOp_12 
ReadVariableOpReadVariableOp2$
ReadVariableOp_1ReadVariableOp_1:i e
A
_output_shapes/
-:+���������������������������
 
_user_specified_nameinputs
�G
�
D__inference_sequential_layer_call_and_return_conditional_losses_7881

inputs*
block0_conv_7809:
block0_conv_7811:
block0_norm_7815:
block0_norm_7817:
block0_norm_7819:
block0_norm_7821:*
block1_conv_7824: 
block1_conv_7826: 
block1_norm_7830: 
block1_norm_7832: 
block1_norm_7834: 
block1_norm_7836: *
block2_conv_7839: @
block2_conv_7841:@
block2_norm_7845:@
block2_norm_7847:@
block2_norm_7849:@
block2_norm_7851:@+
block3_conv_7854:@�
block3_conv_7856:	�
block3_norm_7860:	�
block3_norm_7862:	�
block3_norm_7864:	�
block3_norm_7866:	�$
final_dense_7870:
��
final_dense_7872:	�
output_7875:	�
output_7877:
identity��#Block0_Conv/StatefulPartitionedCall�#Block0_Norm/StatefulPartitionedCall�#Block1_Conv/StatefulPartitionedCall�#Block1_Norm/StatefulPartitionedCall�#Block2_Conv/StatefulPartitionedCall�#Block2_Norm/StatefulPartitionedCall�#Block3_Conv/StatefulPartitionedCall�#Block3_Norm/StatefulPartitionedCall�#Final_Dense/StatefulPartitionedCall�output/StatefulPartitionedCall�
#Block0_Conv/StatefulPartitionedCallStatefulPartitionedCallinputsblock0_conv_7809block0_conv_7811*
Tin
2*
Tout
2*
_collective_manager_ids
 *1
_output_shapes
:�����������*$
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8� *N
fIRG
E__inference_Block0_Conv_layer_call_and_return_conditional_losses_7491�
Block0_Pool/PartitionedCallPartitionedCall,Block0_Conv/StatefulPartitionedCall:output:0*
Tin
2*
Tout
2*
_collective_manager_ids
 */
_output_shapes
:���������@V* 
_read_only_resource_inputs
 *0
config_proto 

CPU

GPU2*0J 8� *N
fIRG
E__inference_Block0_Pool_layer_call_and_return_conditional_losses_7165�
#Block0_Norm/StatefulPartitionedCallStatefulPartitionedCall$Block0_Pool/PartitionedCall:output:0block0_norm_7815block0_norm_7817block0_norm_7819block0_norm_7821*
Tin	
2*
Tout
2*
_collective_manager_ids
 */
_output_shapes
:���������@V*$
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8� *N
fIRG
E__inference_Block0_Norm_layer_call_and_return_conditional_losses_7221�
#Block1_Conv/StatefulPartitionedCallStatefulPartitionedCall,Block0_Norm/StatefulPartitionedCall:output:0block1_conv_7824block1_conv_7826*
Tin
2*
Tout
2*
_collective_manager_ids
 */
_output_shapes
:���������@V *$
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8� *N
fIRG
E__inference_Block1_Conv_layer_call_and_return_conditional_losses_7518�
Block1_Pool/PartitionedCallPartitionedCall,Block1_Conv/StatefulPartitionedCall:output:0*
Tin
2*
Tout
2*
_collective_manager_ids
 */
_output_shapes
:��������� + * 
_read_only_resource_inputs
 *0
config_proto 

CPU

GPU2*0J 8� *N
fIRG
E__inference_Block1_Pool_layer_call_and_return_conditional_losses_7241�
#Block1_Norm/StatefulPartitionedCallStatefulPartitionedCall$Block1_Pool/PartitionedCall:output:0block1_norm_7830block1_norm_7832block1_norm_7834block1_norm_7836*
Tin	
2*
Tout
2*
_collective_manager_ids
 */
_output_shapes
:��������� + *$
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8� *N
fIRG
E__inference_Block1_Norm_layer_call_and_return_conditional_losses_7297�
#Block2_Conv/StatefulPartitionedCallStatefulPartitionedCall,Block1_Norm/StatefulPartitionedCall:output:0block2_conv_7839block2_conv_7841*
Tin
2*
Tout
2*
_collective_manager_ids
 */
_output_shapes
:��������� +@*$
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8� *N
fIRG
E__inference_Block2_Conv_layer_call_and_return_conditional_losses_7545�
Block2_Pool/PartitionedCallPartitionedCall,Block2_Conv/StatefulPartitionedCall:output:0*
Tin
2*
Tout
2*
_collective_manager_ids
 */
_output_shapes
:���������@* 
_read_only_resource_inputs
 *0
config_proto 

CPU

GPU2*0J 8� *N
fIRG
E__inference_Block2_Pool_layer_call_and_return_conditional_losses_7317�
#Block2_Norm/StatefulPartitionedCallStatefulPartitionedCall$Block2_Pool/PartitionedCall:output:0block2_norm_7845block2_norm_7847block2_norm_7849block2_norm_7851*
Tin	
2*
Tout
2*
_collective_manager_ids
 */
_output_shapes
:���������@*$
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8� *N
fIRG
E__inference_Block2_Norm_layer_call_and_return_conditional_losses_7373�
#Block3_Conv/StatefulPartitionedCallStatefulPartitionedCall,Block2_Norm/StatefulPartitionedCall:output:0block3_conv_7854block3_conv_7856*
Tin
2*
Tout
2*
_collective_manager_ids
 *0
_output_shapes
:����������*$
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8� *N
fIRG
E__inference_Block3_Conv_layer_call_and_return_conditional_losses_7572�
Block3_Pool/PartitionedCallPartitionedCall,Block3_Conv/StatefulPartitionedCall:output:0*
Tin
2*
Tout
2*
_collective_manager_ids
 *0
_output_shapes
:���������
�* 
_read_only_resource_inputs
 *0
config_proto 

CPU

GPU2*0J 8� *N
fIRG
E__inference_Block3_Pool_layer_call_and_return_conditional_losses_7393�
#Block3_Norm/StatefulPartitionedCallStatefulPartitionedCall$Block3_Pool/PartitionedCall:output:0block3_norm_7860block3_norm_7862block3_norm_7864block3_norm_7866*
Tin	
2*
Tout
2*
_collective_manager_ids
 *0
_output_shapes
:���������
�*$
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8� *N
fIRG
E__inference_Block3_Norm_layer_call_and_return_conditional_losses_7449�
 GlobalMaxPooling/PartitionedCallPartitionedCall,Block3_Norm/StatefulPartitionedCall:output:0*
Tin
2*
Tout
2*
_collective_manager_ids
 *(
_output_shapes
:����������* 
_read_only_resource_inputs
 *0
config_proto 

CPU

GPU2*0J 8� *S
fNRL
J__inference_GlobalMaxPooling_layer_call_and_return_conditional_losses_7470�
#Final_Dense/StatefulPartitionedCallStatefulPartitionedCall)GlobalMaxPooling/PartitionedCall:output:0final_dense_7870final_dense_7872*
Tin
2*
Tout
2*
_collective_manager_ids
 *(
_output_shapes
:����������*$
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8� *N
fIRG
E__inference_Final_Dense_layer_call_and_return_conditional_losses_7600�
output/StatefulPartitionedCallStatefulPartitionedCall,Final_Dense/StatefulPartitionedCall:output:0output_7875output_7877*
Tin
2*
Tout
2*
_collective_manager_ids
 *'
_output_shapes
:���������*$
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8� *I
fDRB
@__inference_output_layer_call_and_return_conditional_losses_7617v
IdentityIdentity'output/StatefulPartitionedCall:output:0^NoOp*
T0*'
_output_shapes
:����������
NoOpNoOp$^Block0_Conv/StatefulPartitionedCall$^Block0_Norm/StatefulPartitionedCall$^Block1_Conv/StatefulPartitionedCall$^Block1_Norm/StatefulPartitionedCall$^Block2_Conv/StatefulPartitionedCall$^Block2_Norm/StatefulPartitionedCall$^Block3_Conv/StatefulPartitionedCall$^Block3_Norm/StatefulPartitionedCall$^Final_Dense/StatefulPartitionedCall^output/StatefulPartitionedCall*"
_acd_function_control_output(*
_output_shapes
 "
identityIdentity:output:0*(
_construction_contextkEagerRuntime*h
_input_shapesW
U:�����������: : : : : : : : : : : : : : : : : : : : : : : : : : : : 2J
#Block0_Conv/StatefulPartitionedCall#Block0_Conv/StatefulPartitionedCall2J
#Block0_Norm/StatefulPartitionedCall#Block0_Norm/StatefulPartitionedCall2J
#Block1_Conv/StatefulPartitionedCall#Block1_Conv/StatefulPartitionedCall2J
#Block1_Norm/StatefulPartitionedCall#Block1_Norm/StatefulPartitionedCall2J
#Block2_Conv/StatefulPartitionedCall#Block2_Conv/StatefulPartitionedCall2J
#Block2_Norm/StatefulPartitionedCall#Block2_Norm/StatefulPartitionedCall2J
#Block3_Conv/StatefulPartitionedCall#Block3_Conv/StatefulPartitionedCall2J
#Block3_Norm/StatefulPartitionedCall#Block3_Norm/StatefulPartitionedCall2J
#Final_Dense/StatefulPartitionedCall#Final_Dense/StatefulPartitionedCall2@
output/StatefulPartitionedCalloutput/StatefulPartitionedCall:Y U
1
_output_shapes
:�����������
 
_user_specified_nameinputs
�
a
E__inference_Block2_Pool_layer_call_and_return_conditional_losses_7317

inputs
identity�
MaxPoolMaxPoolinputs*J
_output_shapes8
6:4������������������������������������*
ksize
*
paddingVALID*
strides
{
IdentityIdentityMaxPool:output:0*
T0*J
_output_shapes8
6:4������������������������������������"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*I
_input_shapes8
6:4������������������������������������:r n
J
_output_shapes8
6:4������������������������������������
 
_user_specified_nameinputs
�
�
)__inference_sequential_layer_call_fn_8218

inputs!
unknown:
	unknown_0:
	unknown_1:
	unknown_2:
	unknown_3:
	unknown_4:#
	unknown_5: 
	unknown_6: 
	unknown_7: 
	unknown_8: 
	unknown_9: 

unknown_10: $

unknown_11: @

unknown_12:@

unknown_13:@

unknown_14:@

unknown_15:@

unknown_16:@%

unknown_17:@�

unknown_18:	�

unknown_19:	�

unknown_20:	�

unknown_21:	�

unknown_22:	�

unknown_23:
��

unknown_24:	�

unknown_25:	�

unknown_26:
identity��StatefulPartitionedCall�
StatefulPartitionedCallStatefulPartitionedCallinputsunknown	unknown_0	unknown_1	unknown_2	unknown_3	unknown_4	unknown_5	unknown_6	unknown_7	unknown_8	unknown_9
unknown_10
unknown_11
unknown_12
unknown_13
unknown_14
unknown_15
unknown_16
unknown_17
unknown_18
unknown_19
unknown_20
unknown_21
unknown_22
unknown_23
unknown_24
unknown_25
unknown_26*(
Tin!
2*
Tout
2*
_collective_manager_ids
 *'
_output_shapes
:���������*>
_read_only_resource_inputs 
	
*0
config_proto 

CPU

GPU2*0J 8� *M
fHRF
D__inference_sequential_layer_call_and_return_conditional_losses_7624o
IdentityIdentity StatefulPartitionedCall:output:0^NoOp*
T0*'
_output_shapes
:���������`
NoOpNoOp^StatefulPartitionedCall*"
_acd_function_control_output(*
_output_shapes
 "
identityIdentity:output:0*(
_construction_contextkEagerRuntime*h
_input_shapesW
U:�����������: : : : : : : : : : : : : : : : : : : : : : : : : : : : 22
StatefulPartitionedCallStatefulPartitionedCall:Y U
1
_output_shapes
:�����������
 
_user_specified_nameinputs
�
�
E__inference_Block0_Norm_layer_call_and_return_conditional_losses_8650

inputs%
readvariableop_resource:'
readvariableop_1_resource:6
(fusedbatchnormv3_readvariableop_resource:8
*fusedbatchnormv3_readvariableop_1_resource:
identity��AssignNewValue�AssignNewValue_1�FusedBatchNormV3/ReadVariableOp�!FusedBatchNormV3/ReadVariableOp_1�ReadVariableOp�ReadVariableOp_1b
ReadVariableOpReadVariableOpreadvariableop_resource*
_output_shapes
:*
dtype0f
ReadVariableOp_1ReadVariableOpreadvariableop_1_resource*
_output_shapes
:*
dtype0�
FusedBatchNormV3/ReadVariableOpReadVariableOp(fusedbatchnormv3_readvariableop_resource*
_output_shapes
:*
dtype0�
!FusedBatchNormV3/ReadVariableOp_1ReadVariableOp*fusedbatchnormv3_readvariableop_1_resource*
_output_shapes
:*
dtype0�
FusedBatchNormV3FusedBatchNormV3inputsReadVariableOp:value:0ReadVariableOp_1:value:0'FusedBatchNormV3/ReadVariableOp:value:0)FusedBatchNormV3/ReadVariableOp_1:value:0*
T0*
U0*]
_output_shapesK
I:+���������������������������:::::*
epsilon%o�:*
exponential_avg_factor%
�#<�
AssignNewValueAssignVariableOp(fusedbatchnormv3_readvariableop_resourceFusedBatchNormV3:batch_mean:0 ^FusedBatchNormV3/ReadVariableOp*
_output_shapes
 *
dtype0�
AssignNewValue_1AssignVariableOp*fusedbatchnormv3_readvariableop_1_resource!FusedBatchNormV3:batch_variance:0"^FusedBatchNormV3/ReadVariableOp_1*
_output_shapes
 *
dtype0}
IdentityIdentityFusedBatchNormV3:y:0^NoOp*
T0*A
_output_shapes/
-:+����������������������������
NoOpNoOp^AssignNewValue^AssignNewValue_1 ^FusedBatchNormV3/ReadVariableOp"^FusedBatchNormV3/ReadVariableOp_1^ReadVariableOp^ReadVariableOp_1*"
_acd_function_control_output(*
_output_shapes
 "
identityIdentity:output:0*(
_construction_contextkEagerRuntime*H
_input_shapes7
5:+���������������������������: : : : 2 
AssignNewValueAssignNewValue2$
AssignNewValue_1AssignNewValue_12B
FusedBatchNormV3/ReadVariableOpFusedBatchNormV3/ReadVariableOp2F
!FusedBatchNormV3/ReadVariableOp_1!FusedBatchNormV3/ReadVariableOp_12 
ReadVariableOpReadVariableOp2$
ReadVariableOp_1ReadVariableOp_1:i e
A
_output_shapes/
-:+���������������������������
 
_user_specified_nameinputs
�
�
)__inference_sequential_layer_call_fn_8001
input_1!
unknown:
	unknown_0:
	unknown_1:
	unknown_2:
	unknown_3:
	unknown_4:#
	unknown_5: 
	unknown_6: 
	unknown_7: 
	unknown_8: 
	unknown_9: 

unknown_10: $

unknown_11: @

unknown_12:@

unknown_13:@

unknown_14:@

unknown_15:@

unknown_16:@%

unknown_17:@�

unknown_18:	�

unknown_19:	�

unknown_20:	�

unknown_21:	�

unknown_22:	�

unknown_23:
��

unknown_24:	�

unknown_25:	�

unknown_26:
identity��StatefulPartitionedCall�
StatefulPartitionedCallStatefulPartitionedCallinput_1unknown	unknown_0	unknown_1	unknown_2	unknown_3	unknown_4	unknown_5	unknown_6	unknown_7	unknown_8	unknown_9
unknown_10
unknown_11
unknown_12
unknown_13
unknown_14
unknown_15
unknown_16
unknown_17
unknown_18
unknown_19
unknown_20
unknown_21
unknown_22
unknown_23
unknown_24
unknown_25
unknown_26*(
Tin!
2*
Tout
2*
_collective_manager_ids
 *'
_output_shapes
:���������*6
_read_only_resource_inputs
	
*0
config_proto 

CPU

GPU2*0J 8� *M
fHRF
D__inference_sequential_layer_call_and_return_conditional_losses_7881o
IdentityIdentity StatefulPartitionedCall:output:0^NoOp*
T0*'
_output_shapes
:���������`
NoOpNoOp^StatefulPartitionedCall*"
_acd_function_control_output(*
_output_shapes
 "
identityIdentity:output:0*(
_construction_contextkEagerRuntime*h
_input_shapesW
U:�����������: : : : : : : : : : : : : : : : : : : : : : : : : : : : 22
StatefulPartitionedCallStatefulPartitionedCall:Z V
1
_output_shapes
:�����������
!
_user_specified_name	input_1
�
�
E__inference_Block1_Norm_layer_call_and_return_conditional_losses_8724

inputs%
readvariableop_resource: '
readvariableop_1_resource: 6
(fusedbatchnormv3_readvariableop_resource: 8
*fusedbatchnormv3_readvariableop_1_resource: 
identity��FusedBatchNormV3/ReadVariableOp�!FusedBatchNormV3/ReadVariableOp_1�ReadVariableOp�ReadVariableOp_1b
ReadVariableOpReadVariableOpreadvariableop_resource*
_output_shapes
: *
dtype0f
ReadVariableOp_1ReadVariableOpreadvariableop_1_resource*
_output_shapes
: *
dtype0�
FusedBatchNormV3/ReadVariableOpReadVariableOp(fusedbatchnormv3_readvariableop_resource*
_output_shapes
: *
dtype0�
!FusedBatchNormV3/ReadVariableOp_1ReadVariableOp*fusedbatchnormv3_readvariableop_1_resource*
_output_shapes
: *
dtype0�
FusedBatchNormV3FusedBatchNormV3inputsReadVariableOp:value:0ReadVariableOp_1:value:0'FusedBatchNormV3/ReadVariableOp:value:0)FusedBatchNormV3/ReadVariableOp_1:value:0*
T0*
U0*]
_output_shapesK
I:+��������������������������� : : : : :*
epsilon%o�:*
is_training( }
IdentityIdentityFusedBatchNormV3:y:0^NoOp*
T0*A
_output_shapes/
-:+��������������������������� �
NoOpNoOp ^FusedBatchNormV3/ReadVariableOp"^FusedBatchNormV3/ReadVariableOp_1^ReadVariableOp^ReadVariableOp_1*"
_acd_function_control_output(*
_output_shapes
 "
identityIdentity:output:0*(
_construction_contextkEagerRuntime*H
_input_shapes7
5:+��������������������������� : : : : 2B
FusedBatchNormV3/ReadVariableOpFusedBatchNormV3/ReadVariableOp2F
!FusedBatchNormV3/ReadVariableOp_1!FusedBatchNormV3/ReadVariableOp_12 
ReadVariableOpReadVariableOp2$
ReadVariableOp_1ReadVariableOp_1:i e
A
_output_shapes/
-:+��������������������������� 
 
_user_specified_nameinputs
�
�
E__inference_Block2_Conv_layer_call_and_return_conditional_losses_7545

inputs8
conv2d_readvariableop_resource: @-
biasadd_readvariableop_resource:@
identity��BiasAdd/ReadVariableOp�Conv2D/ReadVariableOp|
Conv2D/ReadVariableOpReadVariableOpconv2d_readvariableop_resource*&
_output_shapes
: @*
dtype0�
Conv2DConv2DinputsConv2D/ReadVariableOp:value:0*
T0*/
_output_shapes
:��������� +@*
paddingSAME*
strides
r
BiasAdd/ReadVariableOpReadVariableOpbiasadd_readvariableop_resource*
_output_shapes
:@*
dtype0}
BiasAddBiasAddConv2D:output:0BiasAdd/ReadVariableOp:value:0*
T0*/
_output_shapes
:��������� +@X
ReluReluBiasAdd:output:0*
T0*/
_output_shapes
:��������� +@i
IdentityIdentityRelu:activations:0^NoOp*
T0*/
_output_shapes
:��������� +@w
NoOpNoOp^BiasAdd/ReadVariableOp^Conv2D/ReadVariableOp*"
_acd_function_control_output(*
_output_shapes
 "
identityIdentity:output:0*(
_construction_contextkEagerRuntime*2
_input_shapes!
:��������� + : : 20
BiasAdd/ReadVariableOpBiasAdd/ReadVariableOp2.
Conv2D/ReadVariableOpConv2D/ReadVariableOp:W S
/
_output_shapes
:��������� + 
 
_user_specified_nameinputs
�
�
E__inference_Block3_Conv_layer_call_and_return_conditional_losses_8854

inputs9
conv2d_readvariableop_resource:@�.
biasadd_readvariableop_resource:	�
identity��BiasAdd/ReadVariableOp�Conv2D/ReadVariableOp}
Conv2D/ReadVariableOpReadVariableOpconv2d_readvariableop_resource*'
_output_shapes
:@�*
dtype0�
Conv2DConv2DinputsConv2D/ReadVariableOp:value:0*
T0*0
_output_shapes
:����������*
paddingSAME*
strides
s
BiasAdd/ReadVariableOpReadVariableOpbiasadd_readvariableop_resource*
_output_shapes	
:�*
dtype0~
BiasAddBiasAddConv2D:output:0BiasAdd/ReadVariableOp:value:0*
T0*0
_output_shapes
:����������Y
ReluReluBiasAdd:output:0*
T0*0
_output_shapes
:����������j
IdentityIdentityRelu:activations:0^NoOp*
T0*0
_output_shapes
:����������w
NoOpNoOp^BiasAdd/ReadVariableOp^Conv2D/ReadVariableOp*"
_acd_function_control_output(*
_output_shapes
 "
identityIdentity:output:0*(
_construction_contextkEagerRuntime*2
_input_shapes!
:���������@: : 20
BiasAdd/ReadVariableOpBiasAdd/ReadVariableOp2.
Conv2D/ReadVariableOpConv2D/ReadVariableOp:W S
/
_output_shapes
:���������@
 
_user_specified_nameinputs
�

�
@__inference_output_layer_call_and_return_conditional_losses_7617

inputs1
matmul_readvariableop_resource:	�-
biasadd_readvariableop_resource:
identity��BiasAdd/ReadVariableOp�MatMul/ReadVariableOpu
MatMul/ReadVariableOpReadVariableOpmatmul_readvariableop_resource*
_output_shapes
:	�*
dtype0i
MatMulMatMulinputsMatMul/ReadVariableOp:value:0*
T0*'
_output_shapes
:���������r
BiasAdd/ReadVariableOpReadVariableOpbiasadd_readvariableop_resource*
_output_shapes
:*
dtype0v
BiasAddBiasAddMatMul:product:0BiasAdd/ReadVariableOp:value:0*
T0*'
_output_shapes
:���������V
SigmoidSigmoidBiasAdd:output:0*
T0*'
_output_shapes
:���������Z
IdentityIdentitySigmoid:y:0^NoOp*
T0*'
_output_shapes
:���������w
NoOpNoOp^BiasAdd/ReadVariableOp^MatMul/ReadVariableOp*"
_acd_function_control_output(*
_output_shapes
 "
identityIdentity:output:0*(
_construction_contextkEagerRuntime*+
_input_shapes
:����������: : 20
BiasAdd/ReadVariableOpBiasAdd/ReadVariableOp2.
MatMul/ReadVariableOpMatMul/ReadVariableOp:P L
(
_output_shapes
:����������
 
_user_specified_nameinputs
�
F
*__inference_Block1_Pool_layer_call_fn_8675

inputs
identity�
PartitionedCallPartitionedCallinputs*
Tin
2*
Tout
2*
_collective_manager_ids
 *J
_output_shapes8
6:4������������������������������������* 
_read_only_resource_inputs
 *0
config_proto 

CPU

GPU2*0J 8� *N
fIRG
E__inference_Block1_Pool_layer_call_and_return_conditional_losses_7241�
IdentityIdentityPartitionedCall:output:0*
T0*J
_output_shapes8
6:4������������������������������������"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*I
_input_shapes8
6:4������������������������������������:r n
J
_output_shapes8
6:4������������������������������������
 
_user_specified_nameinputs
�
�
E__inference_Block2_Norm_layer_call_and_return_conditional_losses_8816

inputs%
readvariableop_resource:@'
readvariableop_1_resource:@6
(fusedbatchnormv3_readvariableop_resource:@8
*fusedbatchnormv3_readvariableop_1_resource:@
identity��FusedBatchNormV3/ReadVariableOp�!FusedBatchNormV3/ReadVariableOp_1�ReadVariableOp�ReadVariableOp_1b
ReadVariableOpReadVariableOpreadvariableop_resource*
_output_shapes
:@*
dtype0f
ReadVariableOp_1ReadVariableOpreadvariableop_1_resource*
_output_shapes
:@*
dtype0�
FusedBatchNormV3/ReadVariableOpReadVariableOp(fusedbatchnormv3_readvariableop_resource*
_output_shapes
:@*
dtype0�
!FusedBatchNormV3/ReadVariableOp_1ReadVariableOp*fusedbatchnormv3_readvariableop_1_resource*
_output_shapes
:@*
dtype0�
FusedBatchNormV3FusedBatchNormV3inputsReadVariableOp:value:0ReadVariableOp_1:value:0'FusedBatchNormV3/ReadVariableOp:value:0)FusedBatchNormV3/ReadVariableOp_1:value:0*
T0*
U0*]
_output_shapesK
I:+���������������������������@:@:@:@:@:*
epsilon%o�:*
is_training( }
IdentityIdentityFusedBatchNormV3:y:0^NoOp*
T0*A
_output_shapes/
-:+���������������������������@�
NoOpNoOp ^FusedBatchNormV3/ReadVariableOp"^FusedBatchNormV3/ReadVariableOp_1^ReadVariableOp^ReadVariableOp_1*"
_acd_function_control_output(*
_output_shapes
 "
identityIdentity:output:0*(
_construction_contextkEagerRuntime*H
_input_shapes7
5:+���������������������������@: : : : 2B
FusedBatchNormV3/ReadVariableOpFusedBatchNormV3/ReadVariableOp2F
!FusedBatchNormV3/ReadVariableOp_1!FusedBatchNormV3/ReadVariableOp_12 
ReadVariableOpReadVariableOp2$
ReadVariableOp_1ReadVariableOp_1:i e
A
_output_shapes/
-:+���������������������������@
 
_user_specified_nameinputs
�

�
E__inference_Final_Dense_layer_call_and_return_conditional_losses_8957

inputs2
matmul_readvariableop_resource:
��.
biasadd_readvariableop_resource:	�
identity��BiasAdd/ReadVariableOp�MatMul/ReadVariableOpv
MatMul/ReadVariableOpReadVariableOpmatmul_readvariableop_resource* 
_output_shapes
:
��*
dtype0j
MatMulMatMulinputsMatMul/ReadVariableOp:value:0*
T0*(
_output_shapes
:����������s
BiasAdd/ReadVariableOpReadVariableOpbiasadd_readvariableop_resource*
_output_shapes	
:�*
dtype0w
BiasAddBiasAddMatMul:product:0BiasAdd/ReadVariableOp:value:0*
T0*(
_output_shapes
:����������Q
ReluReluBiasAdd:output:0*
T0*(
_output_shapes
:����������b
IdentityIdentityRelu:activations:0^NoOp*
T0*(
_output_shapes
:����������w
NoOpNoOp^BiasAdd/ReadVariableOp^MatMul/ReadVariableOp*"
_acd_function_control_output(*
_output_shapes
 "
identityIdentity:output:0*(
_construction_contextkEagerRuntime*+
_input_shapes
:����������: : 20
BiasAdd/ReadVariableOpBiasAdd/ReadVariableOp2.
MatMul/ReadVariableOpMatMul/ReadVariableOp:P L
(
_output_shapes
:����������
 
_user_specified_nameinputs
�
K
/__inference_GlobalMaxPooling_layer_call_fn_8931

inputs
identity�
PartitionedCallPartitionedCallinputs*
Tin
2*
Tout
2*
_collective_manager_ids
 *0
_output_shapes
:������������������* 
_read_only_resource_inputs
 *0
config_proto 

CPU

GPU2*0J 8� *S
fNRL
J__inference_GlobalMaxPooling_layer_call_and_return_conditional_losses_7470i
IdentityIdentityPartitionedCall:output:0*
T0*0
_output_shapes
:������������������"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*I
_input_shapes8
6:4������������������������������������:r n
J
_output_shapes8
6:4������������������������������������
 
_user_specified_nameinputs
�
�
E__inference_Block1_Conv_layer_call_and_return_conditional_losses_7518

inputs8
conv2d_readvariableop_resource: -
biasadd_readvariableop_resource: 
identity��BiasAdd/ReadVariableOp�Conv2D/ReadVariableOp|
Conv2D/ReadVariableOpReadVariableOpconv2d_readvariableop_resource*&
_output_shapes
: *
dtype0�
Conv2DConv2DinputsConv2D/ReadVariableOp:value:0*
T0*/
_output_shapes
:���������@V *
paddingSAME*
strides
r
BiasAdd/ReadVariableOpReadVariableOpbiasadd_readvariableop_resource*
_output_shapes
: *
dtype0}
BiasAddBiasAddConv2D:output:0BiasAdd/ReadVariableOp:value:0*
T0*/
_output_shapes
:���������@V X
ReluReluBiasAdd:output:0*
T0*/
_output_shapes
:���������@V i
IdentityIdentityRelu:activations:0^NoOp*
T0*/
_output_shapes
:���������@V w
NoOpNoOp^BiasAdd/ReadVariableOp^Conv2D/ReadVariableOp*"
_acd_function_control_output(*
_output_shapes
 "
identityIdentity:output:0*(
_construction_contextkEagerRuntime*2
_input_shapes!
:���������@V: : 20
BiasAdd/ReadVariableOpBiasAdd/ReadVariableOp2.
Conv2D/ReadVariableOpConv2D/ReadVariableOp:W S
/
_output_shapes
:���������@V
 
_user_specified_nameinputs
�
�
E__inference_Block0_Norm_layer_call_and_return_conditional_losses_7190

inputs%
readvariableop_resource:'
readvariableop_1_resource:6
(fusedbatchnormv3_readvariableop_resource:8
*fusedbatchnormv3_readvariableop_1_resource:
identity��FusedBatchNormV3/ReadVariableOp�!FusedBatchNormV3/ReadVariableOp_1�ReadVariableOp�ReadVariableOp_1b
ReadVariableOpReadVariableOpreadvariableop_resource*
_output_shapes
:*
dtype0f
ReadVariableOp_1ReadVariableOpreadvariableop_1_resource*
_output_shapes
:*
dtype0�
FusedBatchNormV3/ReadVariableOpReadVariableOp(fusedbatchnormv3_readvariableop_resource*
_output_shapes
:*
dtype0�
!FusedBatchNormV3/ReadVariableOp_1ReadVariableOp*fusedbatchnormv3_readvariableop_1_resource*
_output_shapes
:*
dtype0�
FusedBatchNormV3FusedBatchNormV3inputsReadVariableOp:value:0ReadVariableOp_1:value:0'FusedBatchNormV3/ReadVariableOp:value:0)FusedBatchNormV3/ReadVariableOp_1:value:0*
T0*
U0*]
_output_shapesK
I:+���������������������������:::::*
epsilon%o�:*
is_training( }
IdentityIdentityFusedBatchNormV3:y:0^NoOp*
T0*A
_output_shapes/
-:+����������������������������
NoOpNoOp ^FusedBatchNormV3/ReadVariableOp"^FusedBatchNormV3/ReadVariableOp_1^ReadVariableOp^ReadVariableOp_1*"
_acd_function_control_output(*
_output_shapes
 "
identityIdentity:output:0*(
_construction_contextkEagerRuntime*H
_input_shapes7
5:+���������������������������: : : : 2B
FusedBatchNormV3/ReadVariableOpFusedBatchNormV3/ReadVariableOp2F
!FusedBatchNormV3/ReadVariableOp_1!FusedBatchNormV3/ReadVariableOp_12 
ReadVariableOpReadVariableOp2$
ReadVariableOp_1ReadVariableOp_1:i e
A
_output_shapes/
-:+���������������������������
 
_user_specified_nameinputs
�
�
E__inference_Block1_Conv_layer_call_and_return_conditional_losses_8670

inputs8
conv2d_readvariableop_resource: -
biasadd_readvariableop_resource: 
identity��BiasAdd/ReadVariableOp�Conv2D/ReadVariableOp|
Conv2D/ReadVariableOpReadVariableOpconv2d_readvariableop_resource*&
_output_shapes
: *
dtype0�
Conv2DConv2DinputsConv2D/ReadVariableOp:value:0*
T0*/
_output_shapes
:���������@V *
paddingSAME*
strides
r
BiasAdd/ReadVariableOpReadVariableOpbiasadd_readvariableop_resource*
_output_shapes
: *
dtype0}
BiasAddBiasAddConv2D:output:0BiasAdd/ReadVariableOp:value:0*
T0*/
_output_shapes
:���������@V X
ReluReluBiasAdd:output:0*
T0*/
_output_shapes
:���������@V i
IdentityIdentityRelu:activations:0^NoOp*
T0*/
_output_shapes
:���������@V w
NoOpNoOp^BiasAdd/ReadVariableOp^Conv2D/ReadVariableOp*"
_acd_function_control_output(*
_output_shapes
 "
identityIdentity:output:0*(
_construction_contextkEagerRuntime*2
_input_shapes!
:���������@V: : 20
BiasAdd/ReadVariableOpBiasAdd/ReadVariableOp2.
Conv2D/ReadVariableOpConv2D/ReadVariableOp:W S
/
_output_shapes
:���������@V
 
_user_specified_nameinputs
�
�
*__inference_Block1_Norm_layer_call_fn_8693

inputs
unknown: 
	unknown_0: 
	unknown_1: 
	unknown_2: 
identity��StatefulPartitionedCall�
StatefulPartitionedCallStatefulPartitionedCallinputsunknown	unknown_0	unknown_1	unknown_2*
Tin	
2*
Tout
2*
_collective_manager_ids
 *A
_output_shapes/
-:+��������������������������� *&
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8� *N
fIRG
E__inference_Block1_Norm_layer_call_and_return_conditional_losses_7266�
IdentityIdentity StatefulPartitionedCall:output:0^NoOp*
T0*A
_output_shapes/
-:+��������������������������� `
NoOpNoOp^StatefulPartitionedCall*"
_acd_function_control_output(*
_output_shapes
 "
identityIdentity:output:0*(
_construction_contextkEagerRuntime*H
_input_shapes7
5:+��������������������������� : : : : 22
StatefulPartitionedCallStatefulPartitionedCall:i e
A
_output_shapes/
-:+��������������������������� 
 
_user_specified_nameinputs
�
f
J__inference_GlobalMaxPooling_layer_call_and_return_conditional_losses_7470

inputs
identityf
Max/reduction_indicesConst*
_output_shapes
:*
dtype0*
valueB"      m
MaxMaxinputsMax/reduction_indices:output:0*
T0*0
_output_shapes
:������������������]
IdentityIdentityMax:output:0*
T0*0
_output_shapes
:������������������"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*I
_input_shapes8
6:4������������������������������������:r n
J
_output_shapes8
6:4������������������������������������
 
_user_specified_nameinputs
�
�
E__inference_Block2_Norm_layer_call_and_return_conditional_losses_7373

inputs%
readvariableop_resource:@'
readvariableop_1_resource:@6
(fusedbatchnormv3_readvariableop_resource:@8
*fusedbatchnormv3_readvariableop_1_resource:@
identity��AssignNewValue�AssignNewValue_1�FusedBatchNormV3/ReadVariableOp�!FusedBatchNormV3/ReadVariableOp_1�ReadVariableOp�ReadVariableOp_1b
ReadVariableOpReadVariableOpreadvariableop_resource*
_output_shapes
:@*
dtype0f
ReadVariableOp_1ReadVariableOpreadvariableop_1_resource*
_output_shapes
:@*
dtype0�
FusedBatchNormV3/ReadVariableOpReadVariableOp(fusedbatchnormv3_readvariableop_resource*
_output_shapes
:@*
dtype0�
!FusedBatchNormV3/ReadVariableOp_1ReadVariableOp*fusedbatchnormv3_readvariableop_1_resource*
_output_shapes
:@*
dtype0�
FusedBatchNormV3FusedBatchNormV3inputsReadVariableOp:value:0ReadVariableOp_1:value:0'FusedBatchNormV3/ReadVariableOp:value:0)FusedBatchNormV3/ReadVariableOp_1:value:0*
T0*
U0*]
_output_shapesK
I:+���������������������������@:@:@:@:@:*
epsilon%o�:*
exponential_avg_factor%
�#<�
AssignNewValueAssignVariableOp(fusedbatchnormv3_readvariableop_resourceFusedBatchNormV3:batch_mean:0 ^FusedBatchNormV3/ReadVariableOp*
_output_shapes
 *
dtype0�
AssignNewValue_1AssignVariableOp*fusedbatchnormv3_readvariableop_1_resource!FusedBatchNormV3:batch_variance:0"^FusedBatchNormV3/ReadVariableOp_1*
_output_shapes
 *
dtype0}
IdentityIdentityFusedBatchNormV3:y:0^NoOp*
T0*A
_output_shapes/
-:+���������������������������@�
NoOpNoOp^AssignNewValue^AssignNewValue_1 ^FusedBatchNormV3/ReadVariableOp"^FusedBatchNormV3/ReadVariableOp_1^ReadVariableOp^ReadVariableOp_1*"
_acd_function_control_output(*
_output_shapes
 "
identityIdentity:output:0*(
_construction_contextkEagerRuntime*H
_input_shapes7
5:+���������������������������@: : : : 2 
AssignNewValueAssignNewValue2$
AssignNewValue_1AssignNewValue_12B
FusedBatchNormV3/ReadVariableOpFusedBatchNormV3/ReadVariableOp2F
!FusedBatchNormV3/ReadVariableOp_1!FusedBatchNormV3/ReadVariableOp_12 
ReadVariableOpReadVariableOp2$
ReadVariableOp_1ReadVariableOp_1:i e
A
_output_shapes/
-:+���������������������������@
 
_user_specified_nameinputs
�
�
*__inference_Block2_Conv_layer_call_fn_8751

inputs!
unknown: @
	unknown_0:@
identity��StatefulPartitionedCall�
StatefulPartitionedCallStatefulPartitionedCallinputsunknown	unknown_0*
Tin
2*
Tout
2*
_collective_manager_ids
 */
_output_shapes
:��������� +@*$
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8� *N
fIRG
E__inference_Block2_Conv_layer_call_and_return_conditional_losses_7545w
IdentityIdentity StatefulPartitionedCall:output:0^NoOp*
T0*/
_output_shapes
:��������� +@`
NoOpNoOp^StatefulPartitionedCall*"
_acd_function_control_output(*
_output_shapes
 "
identityIdentity:output:0*(
_construction_contextkEagerRuntime*2
_input_shapes!
:��������� + : : 22
StatefulPartitionedCallStatefulPartitionedCall:W S
/
_output_shapes
:��������� + 
 
_user_specified_nameinputs
�
�
)__inference_sequential_layer_call_fn_8279

inputs!
unknown:
	unknown_0:
	unknown_1:
	unknown_2:
	unknown_3:
	unknown_4:#
	unknown_5: 
	unknown_6: 
	unknown_7: 
	unknown_8: 
	unknown_9: 

unknown_10: $

unknown_11: @

unknown_12:@

unknown_13:@

unknown_14:@

unknown_15:@

unknown_16:@%

unknown_17:@�

unknown_18:	�

unknown_19:	�

unknown_20:	�

unknown_21:	�

unknown_22:	�

unknown_23:
��

unknown_24:	�

unknown_25:	�

unknown_26:
identity��StatefulPartitionedCall�
StatefulPartitionedCallStatefulPartitionedCallinputsunknown	unknown_0	unknown_1	unknown_2	unknown_3	unknown_4	unknown_5	unknown_6	unknown_7	unknown_8	unknown_9
unknown_10
unknown_11
unknown_12
unknown_13
unknown_14
unknown_15
unknown_16
unknown_17
unknown_18
unknown_19
unknown_20
unknown_21
unknown_22
unknown_23
unknown_24
unknown_25
unknown_26*(
Tin!
2*
Tout
2*
_collective_manager_ids
 *'
_output_shapes
:���������*6
_read_only_resource_inputs
	
*0
config_proto 

CPU

GPU2*0J 8� *M
fHRF
D__inference_sequential_layer_call_and_return_conditional_losses_7881o
IdentityIdentity StatefulPartitionedCall:output:0^NoOp*
T0*'
_output_shapes
:���������`
NoOpNoOp^StatefulPartitionedCall*"
_acd_function_control_output(*
_output_shapes
 "
identityIdentity:output:0*(
_construction_contextkEagerRuntime*h
_input_shapesW
U:�����������: : : : : : : : : : : : : : : : : : : : : : : : : : : : 22
StatefulPartitionedCallStatefulPartitionedCall:Y U
1
_output_shapes
:�����������
 
_user_specified_nameinputs
�
�
*__inference_Block1_Norm_layer_call_fn_8706

inputs
unknown: 
	unknown_0: 
	unknown_1: 
	unknown_2: 
identity��StatefulPartitionedCall�
StatefulPartitionedCallStatefulPartitionedCallinputsunknown	unknown_0	unknown_1	unknown_2*
Tin	
2*
Tout
2*
_collective_manager_ids
 *A
_output_shapes/
-:+��������������������������� *$
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8� *N
fIRG
E__inference_Block1_Norm_layer_call_and_return_conditional_losses_7297�
IdentityIdentity StatefulPartitionedCall:output:0^NoOp*
T0*A
_output_shapes/
-:+��������������������������� `
NoOpNoOp^StatefulPartitionedCall*"
_acd_function_control_output(*
_output_shapes
 "
identityIdentity:output:0*(
_construction_contextkEagerRuntime*H
_input_shapes7
5:+��������������������������� : : : : 22
StatefulPartitionedCallStatefulPartitionedCall:i e
A
_output_shapes/
-:+��������������������������� 
 
_user_specified_nameinputs
�
�
*__inference_Final_Dense_layer_call_fn_8946

inputs
unknown:
��
	unknown_0:	�
identity��StatefulPartitionedCall�
StatefulPartitionedCallStatefulPartitionedCallinputsunknown	unknown_0*
Tin
2*
Tout
2*
_collective_manager_ids
 *(
_output_shapes
:����������*$
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8� *N
fIRG
E__inference_Final_Dense_layer_call_and_return_conditional_losses_7600p
IdentityIdentity StatefulPartitionedCall:output:0^NoOp*
T0*(
_output_shapes
:����������`
NoOpNoOp^StatefulPartitionedCall*"
_acd_function_control_output(*
_output_shapes
 "
identityIdentity:output:0*(
_construction_contextkEagerRuntime*+
_input_shapes
:����������: : 22
StatefulPartitionedCallStatefulPartitionedCall:P L
(
_output_shapes
:����������
 
_user_specified_nameinputs
�G
�
D__inference_sequential_layer_call_and_return_conditional_losses_8076
input_1*
block0_conv_8004:
block0_conv_8006:
block0_norm_8010:
block0_norm_8012:
block0_norm_8014:
block0_norm_8016:*
block1_conv_8019: 
block1_conv_8021: 
block1_norm_8025: 
block1_norm_8027: 
block1_norm_8029: 
block1_norm_8031: *
block2_conv_8034: @
block2_conv_8036:@
block2_norm_8040:@
block2_norm_8042:@
block2_norm_8044:@
block2_norm_8046:@+
block3_conv_8049:@�
block3_conv_8051:	�
block3_norm_8055:	�
block3_norm_8057:	�
block3_norm_8059:	�
block3_norm_8061:	�$
final_dense_8065:
��
final_dense_8067:	�
output_8070:	�
output_8072:
identity��#Block0_Conv/StatefulPartitionedCall�#Block0_Norm/StatefulPartitionedCall�#Block1_Conv/StatefulPartitionedCall�#Block1_Norm/StatefulPartitionedCall�#Block2_Conv/StatefulPartitionedCall�#Block2_Norm/StatefulPartitionedCall�#Block3_Conv/StatefulPartitionedCall�#Block3_Norm/StatefulPartitionedCall�#Final_Dense/StatefulPartitionedCall�output/StatefulPartitionedCall�
#Block0_Conv/StatefulPartitionedCallStatefulPartitionedCallinput_1block0_conv_8004block0_conv_8006*
Tin
2*
Tout
2*
_collective_manager_ids
 *1
_output_shapes
:�����������*$
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8� *N
fIRG
E__inference_Block0_Conv_layer_call_and_return_conditional_losses_7491�
Block0_Pool/PartitionedCallPartitionedCall,Block0_Conv/StatefulPartitionedCall:output:0*
Tin
2*
Tout
2*
_collective_manager_ids
 */
_output_shapes
:���������@V* 
_read_only_resource_inputs
 *0
config_proto 

CPU

GPU2*0J 8� *N
fIRG
E__inference_Block0_Pool_layer_call_and_return_conditional_losses_7165�
#Block0_Norm/StatefulPartitionedCallStatefulPartitionedCall$Block0_Pool/PartitionedCall:output:0block0_norm_8010block0_norm_8012block0_norm_8014block0_norm_8016*
Tin	
2*
Tout
2*
_collective_manager_ids
 */
_output_shapes
:���������@V*&
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8� *N
fIRG
E__inference_Block0_Norm_layer_call_and_return_conditional_losses_7190�
#Block1_Conv/StatefulPartitionedCallStatefulPartitionedCall,Block0_Norm/StatefulPartitionedCall:output:0block1_conv_8019block1_conv_8021*
Tin
2*
Tout
2*
_collective_manager_ids
 */
_output_shapes
:���������@V *$
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8� *N
fIRG
E__inference_Block1_Conv_layer_call_and_return_conditional_losses_7518�
Block1_Pool/PartitionedCallPartitionedCall,Block1_Conv/StatefulPartitionedCall:output:0*
Tin
2*
Tout
2*
_collective_manager_ids
 */
_output_shapes
:��������� + * 
_read_only_resource_inputs
 *0
config_proto 

CPU

GPU2*0J 8� *N
fIRG
E__inference_Block1_Pool_layer_call_and_return_conditional_losses_7241�
#Block1_Norm/StatefulPartitionedCallStatefulPartitionedCall$Block1_Pool/PartitionedCall:output:0block1_norm_8025block1_norm_8027block1_norm_8029block1_norm_8031*
Tin	
2*
Tout
2*
_collective_manager_ids
 */
_output_shapes
:��������� + *&
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8� *N
fIRG
E__inference_Block1_Norm_layer_call_and_return_conditional_losses_7266�
#Block2_Conv/StatefulPartitionedCallStatefulPartitionedCall,Block1_Norm/StatefulPartitionedCall:output:0block2_conv_8034block2_conv_8036*
Tin
2*
Tout
2*
_collective_manager_ids
 */
_output_shapes
:��������� +@*$
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8� *N
fIRG
E__inference_Block2_Conv_layer_call_and_return_conditional_losses_7545�
Block2_Pool/PartitionedCallPartitionedCall,Block2_Conv/StatefulPartitionedCall:output:0*
Tin
2*
Tout
2*
_collective_manager_ids
 */
_output_shapes
:���������@* 
_read_only_resource_inputs
 *0
config_proto 

CPU

GPU2*0J 8� *N
fIRG
E__inference_Block2_Pool_layer_call_and_return_conditional_losses_7317�
#Block2_Norm/StatefulPartitionedCallStatefulPartitionedCall$Block2_Pool/PartitionedCall:output:0block2_norm_8040block2_norm_8042block2_norm_8044block2_norm_8046*
Tin	
2*
Tout
2*
_collective_manager_ids
 */
_output_shapes
:���������@*&
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8� *N
fIRG
E__inference_Block2_Norm_layer_call_and_return_conditional_losses_7342�
#Block3_Conv/StatefulPartitionedCallStatefulPartitionedCall,Block2_Norm/StatefulPartitionedCall:output:0block3_conv_8049block3_conv_8051*
Tin
2*
Tout
2*
_collective_manager_ids
 *0
_output_shapes
:����������*$
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8� *N
fIRG
E__inference_Block3_Conv_layer_call_and_return_conditional_losses_7572�
Block3_Pool/PartitionedCallPartitionedCall,Block3_Conv/StatefulPartitionedCall:output:0*
Tin
2*
Tout
2*
_collective_manager_ids
 *0
_output_shapes
:���������
�* 
_read_only_resource_inputs
 *0
config_proto 

CPU

GPU2*0J 8� *N
fIRG
E__inference_Block3_Pool_layer_call_and_return_conditional_losses_7393�
#Block3_Norm/StatefulPartitionedCallStatefulPartitionedCall$Block3_Pool/PartitionedCall:output:0block3_norm_8055block3_norm_8057block3_norm_8059block3_norm_8061*
Tin	
2*
Tout
2*
_collective_manager_ids
 *0
_output_shapes
:���������
�*&
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8� *N
fIRG
E__inference_Block3_Norm_layer_call_and_return_conditional_losses_7418�
 GlobalMaxPooling/PartitionedCallPartitionedCall,Block3_Norm/StatefulPartitionedCall:output:0*
Tin
2*
Tout
2*
_collective_manager_ids
 *(
_output_shapes
:����������* 
_read_only_resource_inputs
 *0
config_proto 

CPU

GPU2*0J 8� *S
fNRL
J__inference_GlobalMaxPooling_layer_call_and_return_conditional_losses_7470�
#Final_Dense/StatefulPartitionedCallStatefulPartitionedCall)GlobalMaxPooling/PartitionedCall:output:0final_dense_8065final_dense_8067*
Tin
2*
Tout
2*
_collective_manager_ids
 *(
_output_shapes
:����������*$
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8� *N
fIRG
E__inference_Final_Dense_layer_call_and_return_conditional_losses_7600�
output/StatefulPartitionedCallStatefulPartitionedCall,Final_Dense/StatefulPartitionedCall:output:0output_8070output_8072*
Tin
2*
Tout
2*
_collective_manager_ids
 *'
_output_shapes
:���������*$
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8� *I
fDRB
@__inference_output_layer_call_and_return_conditional_losses_7617v
IdentityIdentity'output/StatefulPartitionedCall:output:0^NoOp*
T0*'
_output_shapes
:����������
NoOpNoOp$^Block0_Conv/StatefulPartitionedCall$^Block0_Norm/StatefulPartitionedCall$^Block1_Conv/StatefulPartitionedCall$^Block1_Norm/StatefulPartitionedCall$^Block2_Conv/StatefulPartitionedCall$^Block2_Norm/StatefulPartitionedCall$^Block3_Conv/StatefulPartitionedCall$^Block3_Norm/StatefulPartitionedCall$^Final_Dense/StatefulPartitionedCall^output/StatefulPartitionedCall*"
_acd_function_control_output(*
_output_shapes
 "
identityIdentity:output:0*(
_construction_contextkEagerRuntime*h
_input_shapesW
U:�����������: : : : : : : : : : : : : : : : : : : : : : : : : : : : 2J
#Block0_Conv/StatefulPartitionedCall#Block0_Conv/StatefulPartitionedCall2J
#Block0_Norm/StatefulPartitionedCall#Block0_Norm/StatefulPartitionedCall2J
#Block1_Conv/StatefulPartitionedCall#Block1_Conv/StatefulPartitionedCall2J
#Block1_Norm/StatefulPartitionedCall#Block1_Norm/StatefulPartitionedCall2J
#Block2_Conv/StatefulPartitionedCall#Block2_Conv/StatefulPartitionedCall2J
#Block2_Norm/StatefulPartitionedCall#Block2_Norm/StatefulPartitionedCall2J
#Block3_Conv/StatefulPartitionedCall#Block3_Conv/StatefulPartitionedCall2J
#Block3_Norm/StatefulPartitionedCall#Block3_Norm/StatefulPartitionedCall2J
#Final_Dense/StatefulPartitionedCall#Final_Dense/StatefulPartitionedCall2@
output/StatefulPartitionedCalloutput/StatefulPartitionedCall:Z V
1
_output_shapes
:�����������
!
_user_specified_name	input_1
�
�
E__inference_Block1_Norm_layer_call_and_return_conditional_losses_8742

inputs%
readvariableop_resource: '
readvariableop_1_resource: 6
(fusedbatchnormv3_readvariableop_resource: 8
*fusedbatchnormv3_readvariableop_1_resource: 
identity��AssignNewValue�AssignNewValue_1�FusedBatchNormV3/ReadVariableOp�!FusedBatchNormV3/ReadVariableOp_1�ReadVariableOp�ReadVariableOp_1b
ReadVariableOpReadVariableOpreadvariableop_resource*
_output_shapes
: *
dtype0f
ReadVariableOp_1ReadVariableOpreadvariableop_1_resource*
_output_shapes
: *
dtype0�
FusedBatchNormV3/ReadVariableOpReadVariableOp(fusedbatchnormv3_readvariableop_resource*
_output_shapes
: *
dtype0�
!FusedBatchNormV3/ReadVariableOp_1ReadVariableOp*fusedbatchnormv3_readvariableop_1_resource*
_output_shapes
: *
dtype0�
FusedBatchNormV3FusedBatchNormV3inputsReadVariableOp:value:0ReadVariableOp_1:value:0'FusedBatchNormV3/ReadVariableOp:value:0)FusedBatchNormV3/ReadVariableOp_1:value:0*
T0*
U0*]
_output_shapesK
I:+��������������������������� : : : : :*
epsilon%o�:*
exponential_avg_factor%
�#<�
AssignNewValueAssignVariableOp(fusedbatchnormv3_readvariableop_resourceFusedBatchNormV3:batch_mean:0 ^FusedBatchNormV3/ReadVariableOp*
_output_shapes
 *
dtype0�
AssignNewValue_1AssignVariableOp*fusedbatchnormv3_readvariableop_1_resource!FusedBatchNormV3:batch_variance:0"^FusedBatchNormV3/ReadVariableOp_1*
_output_shapes
 *
dtype0}
IdentityIdentityFusedBatchNormV3:y:0^NoOp*
T0*A
_output_shapes/
-:+��������������������������� �
NoOpNoOp^AssignNewValue^AssignNewValue_1 ^FusedBatchNormV3/ReadVariableOp"^FusedBatchNormV3/ReadVariableOp_1^ReadVariableOp^ReadVariableOp_1*"
_acd_function_control_output(*
_output_shapes
 "
identityIdentity:output:0*(
_construction_contextkEagerRuntime*H
_input_shapes7
5:+��������������������������� : : : : 2 
AssignNewValueAssignNewValue2$
AssignNewValue_1AssignNewValue_12B
FusedBatchNormV3/ReadVariableOpFusedBatchNormV3/ReadVariableOp2F
!FusedBatchNormV3/ReadVariableOp_1!FusedBatchNormV3/ReadVariableOp_12 
ReadVariableOpReadVariableOp2$
ReadVariableOp_1ReadVariableOp_1:i e
A
_output_shapes/
-:+��������������������������� 
 
_user_specified_nameinputs
�
�
)__inference_sequential_layer_call_fn_7683
input_1!
unknown:
	unknown_0:
	unknown_1:
	unknown_2:
	unknown_3:
	unknown_4:#
	unknown_5: 
	unknown_6: 
	unknown_7: 
	unknown_8: 
	unknown_9: 

unknown_10: $

unknown_11: @

unknown_12:@

unknown_13:@

unknown_14:@

unknown_15:@

unknown_16:@%

unknown_17:@�

unknown_18:	�

unknown_19:	�

unknown_20:	�

unknown_21:	�

unknown_22:	�

unknown_23:
��

unknown_24:	�

unknown_25:	�

unknown_26:
identity��StatefulPartitionedCall�
StatefulPartitionedCallStatefulPartitionedCallinput_1unknown	unknown_0	unknown_1	unknown_2	unknown_3	unknown_4	unknown_5	unknown_6	unknown_7	unknown_8	unknown_9
unknown_10
unknown_11
unknown_12
unknown_13
unknown_14
unknown_15
unknown_16
unknown_17
unknown_18
unknown_19
unknown_20
unknown_21
unknown_22
unknown_23
unknown_24
unknown_25
unknown_26*(
Tin!
2*
Tout
2*
_collective_manager_ids
 *'
_output_shapes
:���������*>
_read_only_resource_inputs 
	
*0
config_proto 

CPU

GPU2*0J 8� *M
fHRF
D__inference_sequential_layer_call_and_return_conditional_losses_7624o
IdentityIdentity StatefulPartitionedCall:output:0^NoOp*
T0*'
_output_shapes
:���������`
NoOpNoOp^StatefulPartitionedCall*"
_acd_function_control_output(*
_output_shapes
 "
identityIdentity:output:0*(
_construction_contextkEagerRuntime*h
_input_shapesW
U:�����������: : : : : : : : : : : : : : : : : : : : : : : : : : : : 22
StatefulPartitionedCallStatefulPartitionedCall:Z V
1
_output_shapes
:�����������
!
_user_specified_name	input_1
�
�
*__inference_Block0_Conv_layer_call_fn_8567

inputs!
unknown:
	unknown_0:
identity��StatefulPartitionedCall�
StatefulPartitionedCallStatefulPartitionedCallinputsunknown	unknown_0*
Tin
2*
Tout
2*
_collective_manager_ids
 *1
_output_shapes
:�����������*$
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8� *N
fIRG
E__inference_Block0_Conv_layer_call_and_return_conditional_losses_7491y
IdentityIdentity StatefulPartitionedCall:output:0^NoOp*
T0*1
_output_shapes
:�����������`
NoOpNoOp^StatefulPartitionedCall*"
_acd_function_control_output(*
_output_shapes
 "
identityIdentity:output:0*(
_construction_contextkEagerRuntime*4
_input_shapes#
!:�����������: : 22
StatefulPartitionedCallStatefulPartitionedCall:Y U
1
_output_shapes
:�����������
 
_user_specified_nameinputs
�
�
E__inference_Block2_Conv_layer_call_and_return_conditional_losses_8762

inputs8
conv2d_readvariableop_resource: @-
biasadd_readvariableop_resource:@
identity��BiasAdd/ReadVariableOp�Conv2D/ReadVariableOp|
Conv2D/ReadVariableOpReadVariableOpconv2d_readvariableop_resource*&
_output_shapes
: @*
dtype0�
Conv2DConv2DinputsConv2D/ReadVariableOp:value:0*
T0*/
_output_shapes
:��������� +@*
paddingSAME*
strides
r
BiasAdd/ReadVariableOpReadVariableOpbiasadd_readvariableop_resource*
_output_shapes
:@*
dtype0}
BiasAddBiasAddConv2D:output:0BiasAdd/ReadVariableOp:value:0*
T0*/
_output_shapes
:��������� +@X
ReluReluBiasAdd:output:0*
T0*/
_output_shapes
:��������� +@i
IdentityIdentityRelu:activations:0^NoOp*
T0*/
_output_shapes
:��������� +@w
NoOpNoOp^BiasAdd/ReadVariableOp^Conv2D/ReadVariableOp*"
_acd_function_control_output(*
_output_shapes
 "
identityIdentity:output:0*(
_construction_contextkEagerRuntime*2
_input_shapes!
:��������� + : : 20
BiasAdd/ReadVariableOpBiasAdd/ReadVariableOp2.
Conv2D/ReadVariableOpConv2D/ReadVariableOp:W S
/
_output_shapes
:��������� + 
 
_user_specified_nameinputs
�
�
E__inference_Block0_Conv_layer_call_and_return_conditional_losses_8578

inputs8
conv2d_readvariableop_resource:-
biasadd_readvariableop_resource:
identity��BiasAdd/ReadVariableOp�Conv2D/ReadVariableOp|
Conv2D/ReadVariableOpReadVariableOpconv2d_readvariableop_resource*&
_output_shapes
:*
dtype0�
Conv2DConv2DinputsConv2D/ReadVariableOp:value:0*
T0*1
_output_shapes
:�����������*
paddingSAME*
strides
r
BiasAdd/ReadVariableOpReadVariableOpbiasadd_readvariableop_resource*
_output_shapes
:*
dtype0
BiasAddBiasAddConv2D:output:0BiasAdd/ReadVariableOp:value:0*
T0*1
_output_shapes
:�����������Z
ReluReluBiasAdd:output:0*
T0*1
_output_shapes
:�����������k
IdentityIdentityRelu:activations:0^NoOp*
T0*1
_output_shapes
:�����������w
NoOpNoOp^BiasAdd/ReadVariableOp^Conv2D/ReadVariableOp*"
_acd_function_control_output(*
_output_shapes
 "
identityIdentity:output:0*(
_construction_contextkEagerRuntime*4
_input_shapes#
!:�����������: : 20
BiasAdd/ReadVariableOpBiasAdd/ReadVariableOp2.
Conv2D/ReadVariableOpConv2D/ReadVariableOp:Y U
1
_output_shapes
:�����������
 
_user_specified_nameinputs
�
a
E__inference_Block0_Pool_layer_call_and_return_conditional_losses_8588

inputs
identity�
MaxPoolMaxPoolinputs*J
_output_shapes8
6:4������������������������������������*
ksize
*
paddingVALID*
strides
{
IdentityIdentityMaxPool:output:0*
T0*J
_output_shapes8
6:4������������������������������������"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*I
_input_shapes8
6:4������������������������������������:r n
J
_output_shapes8
6:4������������������������������������
 
_user_specified_nameinputs
�
�
E__inference_Block1_Norm_layer_call_and_return_conditional_losses_7266

inputs%
readvariableop_resource: '
readvariableop_1_resource: 6
(fusedbatchnormv3_readvariableop_resource: 8
*fusedbatchnormv3_readvariableop_1_resource: 
identity��FusedBatchNormV3/ReadVariableOp�!FusedBatchNormV3/ReadVariableOp_1�ReadVariableOp�ReadVariableOp_1b
ReadVariableOpReadVariableOpreadvariableop_resource*
_output_shapes
: *
dtype0f
ReadVariableOp_1ReadVariableOpreadvariableop_1_resource*
_output_shapes
: *
dtype0�
FusedBatchNormV3/ReadVariableOpReadVariableOp(fusedbatchnormv3_readvariableop_resource*
_output_shapes
: *
dtype0�
!FusedBatchNormV3/ReadVariableOp_1ReadVariableOp*fusedbatchnormv3_readvariableop_1_resource*
_output_shapes
: *
dtype0�
FusedBatchNormV3FusedBatchNormV3inputsReadVariableOp:value:0ReadVariableOp_1:value:0'FusedBatchNormV3/ReadVariableOp:value:0)FusedBatchNormV3/ReadVariableOp_1:value:0*
T0*
U0*]
_output_shapesK
I:+��������������������������� : : : : :*
epsilon%o�:*
is_training( }
IdentityIdentityFusedBatchNormV3:y:0^NoOp*
T0*A
_output_shapes/
-:+��������������������������� �
NoOpNoOp ^FusedBatchNormV3/ReadVariableOp"^FusedBatchNormV3/ReadVariableOp_1^ReadVariableOp^ReadVariableOp_1*"
_acd_function_control_output(*
_output_shapes
 "
identityIdentity:output:0*(
_construction_contextkEagerRuntime*H
_input_shapes7
5:+��������������������������� : : : : 2B
FusedBatchNormV3/ReadVariableOpFusedBatchNormV3/ReadVariableOp2F
!FusedBatchNormV3/ReadVariableOp_1!FusedBatchNormV3/ReadVariableOp_12 
ReadVariableOpReadVariableOp2$
ReadVariableOp_1ReadVariableOp_1:i e
A
_output_shapes/
-:+��������������������������� 
 
_user_specified_nameinputs
�	
�
*__inference_Block3_Norm_layer_call_fn_8877

inputs
unknown:	�
	unknown_0:	�
	unknown_1:	�
	unknown_2:	�
identity��StatefulPartitionedCall�
StatefulPartitionedCallStatefulPartitionedCallinputsunknown	unknown_0	unknown_1	unknown_2*
Tin	
2*
Tout
2*
_collective_manager_ids
 *B
_output_shapes0
.:,����������������������������*&
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8� *N
fIRG
E__inference_Block3_Norm_layer_call_and_return_conditional_losses_7418�
IdentityIdentity StatefulPartitionedCall:output:0^NoOp*
T0*B
_output_shapes0
.:,����������������������������`
NoOpNoOp^StatefulPartitionedCall*"
_acd_function_control_output(*
_output_shapes
 "
identityIdentity:output:0*(
_construction_contextkEagerRuntime*I
_input_shapes8
6:,����������������������������: : : : 22
StatefulPartitionedCallStatefulPartitionedCall:j f
B
_output_shapes0
.:,����������������������������
 
_user_specified_nameinputs
�
a
E__inference_Block1_Pool_layer_call_and_return_conditional_losses_8680

inputs
identity�
MaxPoolMaxPoolinputs*J
_output_shapes8
6:4������������������������������������*
ksize
*
paddingVALID*
strides
{
IdentityIdentityMaxPool:output:0*
T0*J
_output_shapes8
6:4������������������������������������"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*I
_input_shapes8
6:4������������������������������������:r n
J
_output_shapes8
6:4������������������������������������
 
_user_specified_nameinputs
�
a
E__inference_Block0_Pool_layer_call_and_return_conditional_losses_7165

inputs
identity�
MaxPoolMaxPoolinputs*J
_output_shapes8
6:4������������������������������������*
ksize
*
paddingVALID*
strides
{
IdentityIdentityMaxPool:output:0*
T0*J
_output_shapes8
6:4������������������������������������"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*I
_input_shapes8
6:4������������������������������������:r n
J
_output_shapes8
6:4������������������������������������
 
_user_specified_nameinputs
�

�
@__inference_output_layer_call_and_return_conditional_losses_8977

inputs1
matmul_readvariableop_resource:	�-
biasadd_readvariableop_resource:
identity��BiasAdd/ReadVariableOp�MatMul/ReadVariableOpu
MatMul/ReadVariableOpReadVariableOpmatmul_readvariableop_resource*
_output_shapes
:	�*
dtype0i
MatMulMatMulinputsMatMul/ReadVariableOp:value:0*
T0*'
_output_shapes
:���������r
BiasAdd/ReadVariableOpReadVariableOpbiasadd_readvariableop_resource*
_output_shapes
:*
dtype0v
BiasAddBiasAddMatMul:product:0BiasAdd/ReadVariableOp:value:0*
T0*'
_output_shapes
:���������V
SigmoidSigmoidBiasAdd:output:0*
T0*'
_output_shapes
:���������Z
IdentityIdentitySigmoid:y:0^NoOp*
T0*'
_output_shapes
:���������w
NoOpNoOp^BiasAdd/ReadVariableOp^MatMul/ReadVariableOp*"
_acd_function_control_output(*
_output_shapes
 "
identityIdentity:output:0*(
_construction_contextkEagerRuntime*+
_input_shapes
:����������: : 20
BiasAdd/ReadVariableOpBiasAdd/ReadVariableOp2.
MatMul/ReadVariableOpMatMul/ReadVariableOp:P L
(
_output_shapes
:����������
 
_user_specified_nameinputs
�
�
E__inference_Block3_Norm_layer_call_and_return_conditional_losses_8908

inputs&
readvariableop_resource:	�(
readvariableop_1_resource:	�7
(fusedbatchnormv3_readvariableop_resource:	�9
*fusedbatchnormv3_readvariableop_1_resource:	�
identity��FusedBatchNormV3/ReadVariableOp�!FusedBatchNormV3/ReadVariableOp_1�ReadVariableOp�ReadVariableOp_1c
ReadVariableOpReadVariableOpreadvariableop_resource*
_output_shapes	
:�*
dtype0g
ReadVariableOp_1ReadVariableOpreadvariableop_1_resource*
_output_shapes	
:�*
dtype0�
FusedBatchNormV3/ReadVariableOpReadVariableOp(fusedbatchnormv3_readvariableop_resource*
_output_shapes	
:�*
dtype0�
!FusedBatchNormV3/ReadVariableOp_1ReadVariableOp*fusedbatchnormv3_readvariableop_1_resource*
_output_shapes	
:�*
dtype0�
FusedBatchNormV3FusedBatchNormV3inputsReadVariableOp:value:0ReadVariableOp_1:value:0'FusedBatchNormV3/ReadVariableOp:value:0)FusedBatchNormV3/ReadVariableOp_1:value:0*
T0*
U0*b
_output_shapesP
N:,����������������������������:�:�:�:�:*
epsilon%o�:*
is_training( ~
IdentityIdentityFusedBatchNormV3:y:0^NoOp*
T0*B
_output_shapes0
.:,�����������������������������
NoOpNoOp ^FusedBatchNormV3/ReadVariableOp"^FusedBatchNormV3/ReadVariableOp_1^ReadVariableOp^ReadVariableOp_1*"
_acd_function_control_output(*
_output_shapes
 "
identityIdentity:output:0*(
_construction_contextkEagerRuntime*I
_input_shapes8
6:,����������������������������: : : : 2B
FusedBatchNormV3/ReadVariableOpFusedBatchNormV3/ReadVariableOp2F
!FusedBatchNormV3/ReadVariableOp_1!FusedBatchNormV3/ReadVariableOp_12 
ReadVariableOpReadVariableOp2$
ReadVariableOp_1ReadVariableOp_1:j f
B
_output_shapes0
.:,����������������������������
 
_user_specified_nameinputs"�L
saver_filename:0StatefulPartitionedCall_1:0StatefulPartitionedCall_28"
saved_model_main_op

NoOp*>
__saved_model_init_op%#
__saved_model_init_op

NoOp*�
serving_default�
E
input_1:
serving_default_input_1:0�����������:
output0
StatefulPartitionedCall:0���������tensorflow/serving/predict:Ϙ
�
layer_with_weights-0
layer-0
layer-1
layer_with_weights-1
layer-2
layer_with_weights-2
layer-3
layer-4
layer_with_weights-3
layer-5
layer_with_weights-4
layer-6
layer-7
	layer_with_weights-5
	layer-8

layer_with_weights-6

layer-9
layer-10
layer_with_weights-7
layer-11
layer-12
layer_with_weights-8
layer-13
layer_with_weights-9
layer-14
	optimizer
	variables
trainable_variables
regularization_losses
	keras_api
__call__
*&call_and_return_all_conditional_losses
_default_save_signature

signatures"
_tf_keras_sequential
�

kernel
bias
	variables
trainable_variables
regularization_losses
	keras_api
__call__
* &call_and_return_all_conditional_losses"
_tf_keras_layer
�
!	variables
"trainable_variables
#regularization_losses
$	keras_api
%__call__
*&&call_and_return_all_conditional_losses"
_tf_keras_layer
�
'axis
	(gamma
)beta
*moving_mean
+moving_variance
,	variables
-trainable_variables
.regularization_losses
/	keras_api
0__call__
*1&call_and_return_all_conditional_losses"
_tf_keras_layer
�

2kernel
3bias
4	variables
5trainable_variables
6regularization_losses
7	keras_api
8__call__
*9&call_and_return_all_conditional_losses"
_tf_keras_layer
�
:	variables
;trainable_variables
<regularization_losses
=	keras_api
>__call__
*?&call_and_return_all_conditional_losses"
_tf_keras_layer
�
@axis
	Agamma
Bbeta
Cmoving_mean
Dmoving_variance
E	variables
Ftrainable_variables
Gregularization_losses
H	keras_api
I__call__
*J&call_and_return_all_conditional_losses"
_tf_keras_layer
�

Kkernel
Lbias
M	variables
Ntrainable_variables
Oregularization_losses
P	keras_api
Q__call__
*R&call_and_return_all_conditional_losses"
_tf_keras_layer
�
S	variables
Ttrainable_variables
Uregularization_losses
V	keras_api
W__call__
*X&call_and_return_all_conditional_losses"
_tf_keras_layer
�
Yaxis
	Zgamma
[beta
\moving_mean
]moving_variance
^	variables
_trainable_variables
`regularization_losses
a	keras_api
b__call__
*c&call_and_return_all_conditional_losses"
_tf_keras_layer
�

dkernel
ebias
f	variables
gtrainable_variables
hregularization_losses
i	keras_api
j__call__
*k&call_and_return_all_conditional_losses"
_tf_keras_layer
�
l	variables
mtrainable_variables
nregularization_losses
o	keras_api
p__call__
*q&call_and_return_all_conditional_losses"
_tf_keras_layer
�
raxis
	sgamma
tbeta
umoving_mean
vmoving_variance
w	variables
xtrainable_variables
yregularization_losses
z	keras_api
{__call__
*|&call_and_return_all_conditional_losses"
_tf_keras_layer
�
}	variables
~trainable_variables
regularization_losses
�	keras_api
�__call__
+�&call_and_return_all_conditional_losses"
_tf_keras_layer
�
�kernel
	�bias
�	variables
�trainable_variables
�regularization_losses
�	keras_api
�__call__
+�&call_and_return_all_conditional_losses"
_tf_keras_layer
�
�kernel
	�bias
�	variables
�trainable_variables
�regularization_losses
�	keras_api
�__call__
+�&call_and_return_all_conditional_losses"
_tf_keras_layer
�
	�iter
�beta_1
�beta_2

�decay
�learning_ratem�m�(m�)m�2m�3m�Am�Bm�Km�Lm�Zm�[m�dm�em�sm�tm�	�m�	�m�	�m�	�m�v�v�(v�)v�2v�3v�Av�Bv�Kv�Lv�Zv�[v�dv�ev�sv�tv�	�v�	�v�	�v�	�v�"
	optimizer
�
0
1
(2
)3
*4
+5
26
37
A8
B9
C10
D11
K12
L13
Z14
[15
\16
]17
d18
e19
s20
t21
u22
v23
�24
�25
�26
�27"
trackable_list_wrapper
�
0
1
(2
)3
24
35
A6
B7
K8
L9
Z10
[11
d12
e13
s14
t15
�16
�17
�18
�19"
trackable_list_wrapper
 "
trackable_list_wrapper
�
�non_trainable_variables
�layers
�metrics
 �layer_regularization_losses
�layer_metrics
	variables
trainable_variables
regularization_losses
__call__
_default_save_signature
*&call_and_return_all_conditional_losses
&"call_and_return_conditional_losses"
_generic_user_object
�2�
)__inference_sequential_layer_call_fn_7683
)__inference_sequential_layer_call_fn_8218
)__inference_sequential_layer_call_fn_8279
)__inference_sequential_layer_call_fn_8001�
���
FullArgSpec1
args)�&
jself
jinputs

jtraining
jmask
varargs
 
varkw
 
defaults�
p 

 

kwonlyargs� 
kwonlydefaults� 
annotations� *
 
�2�
D__inference_sequential_layer_call_and_return_conditional_losses_8387
D__inference_sequential_layer_call_and_return_conditional_losses_8495
D__inference_sequential_layer_call_and_return_conditional_losses_8076
D__inference_sequential_layer_call_and_return_conditional_losses_8151�
���
FullArgSpec1
args)�&
jself
jinputs

jtraining
jmask
varargs
 
varkw
 
defaults�
p 

 

kwonlyargs� 
kwonlydefaults� 
annotations� *
 
�B�
__inference__wrapped_model_7156input_1"�
���
FullArgSpec
args� 
varargsjargs
varkwjkwargs
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 
-
�serving_default"
signature_map
,:*2Block0_Conv/kernel
:2Block0_Conv/bias
.
0
1"
trackable_list_wrapper
.
0
1"
trackable_list_wrapper
 "
trackable_list_wrapper
�
�non_trainable_variables
�layers
�metrics
 �layer_regularization_losses
�layer_metrics
	variables
trainable_variables
regularization_losses
__call__
* &call_and_return_all_conditional_losses
& "call_and_return_conditional_losses"
_generic_user_object
�2�
*__inference_Block0_Conv_layer_call_fn_8567�
���
FullArgSpec
args�
jself
jinputs
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 
�2�
E__inference_Block0_Conv_layer_call_and_return_conditional_losses_8578�
���
FullArgSpec
args�
jself
jinputs
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
�
�non_trainable_variables
�layers
�metrics
 �layer_regularization_losses
�layer_metrics
!	variables
"trainable_variables
#regularization_losses
%__call__
*&&call_and_return_all_conditional_losses
&&"call_and_return_conditional_losses"
_generic_user_object
�2�
*__inference_Block0_Pool_layer_call_fn_8583�
���
FullArgSpec
args�
jself
jinputs
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 
�2�
E__inference_Block0_Pool_layer_call_and_return_conditional_losses_8588�
���
FullArgSpec
args�
jself
jinputs
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 
 "
trackable_list_wrapper
:2Block0_Norm/gamma
:2Block0_Norm/beta
':% (2Block0_Norm/moving_mean
+:) (2Block0_Norm/moving_variance
<
(0
)1
*2
+3"
trackable_list_wrapper
.
(0
)1"
trackable_list_wrapper
 "
trackable_list_wrapper
�
�non_trainable_variables
�layers
�metrics
 �layer_regularization_losses
�layer_metrics
,	variables
-trainable_variables
.regularization_losses
0__call__
*1&call_and_return_all_conditional_losses
&1"call_and_return_conditional_losses"
_generic_user_object
�2�
*__inference_Block0_Norm_layer_call_fn_8601
*__inference_Block0_Norm_layer_call_fn_8614�
���
FullArgSpec)
args!�
jself
jinputs

jtraining
varargs
 
varkw
 
defaults�
p 

kwonlyargs� 
kwonlydefaults� 
annotations� *
 
�2�
E__inference_Block0_Norm_layer_call_and_return_conditional_losses_8632
E__inference_Block0_Norm_layer_call_and_return_conditional_losses_8650�
���
FullArgSpec)
args!�
jself
jinputs

jtraining
varargs
 
varkw
 
defaults�
p 

kwonlyargs� 
kwonlydefaults� 
annotations� *
 
,:* 2Block1_Conv/kernel
: 2Block1_Conv/bias
.
20
31"
trackable_list_wrapper
.
20
31"
trackable_list_wrapper
 "
trackable_list_wrapper
�
�non_trainable_variables
�layers
�metrics
 �layer_regularization_losses
�layer_metrics
4	variables
5trainable_variables
6regularization_losses
8__call__
*9&call_and_return_all_conditional_losses
&9"call_and_return_conditional_losses"
_generic_user_object
�2�
*__inference_Block1_Conv_layer_call_fn_8659�
���
FullArgSpec
args�
jself
jinputs
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 
�2�
E__inference_Block1_Conv_layer_call_and_return_conditional_losses_8670�
���
FullArgSpec
args�
jself
jinputs
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
�
�non_trainable_variables
�layers
�metrics
 �layer_regularization_losses
�layer_metrics
:	variables
;trainable_variables
<regularization_losses
>__call__
*?&call_and_return_all_conditional_losses
&?"call_and_return_conditional_losses"
_generic_user_object
�2�
*__inference_Block1_Pool_layer_call_fn_8675�
���
FullArgSpec
args�
jself
jinputs
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 
�2�
E__inference_Block1_Pool_layer_call_and_return_conditional_losses_8680�
���
FullArgSpec
args�
jself
jinputs
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 
 "
trackable_list_wrapper
: 2Block1_Norm/gamma
: 2Block1_Norm/beta
':%  (2Block1_Norm/moving_mean
+:)  (2Block1_Norm/moving_variance
<
A0
B1
C2
D3"
trackable_list_wrapper
.
A0
B1"
trackable_list_wrapper
 "
trackable_list_wrapper
�
�non_trainable_variables
�layers
�metrics
 �layer_regularization_losses
�layer_metrics
E	variables
Ftrainable_variables
Gregularization_losses
I__call__
*J&call_and_return_all_conditional_losses
&J"call_and_return_conditional_losses"
_generic_user_object
�2�
*__inference_Block1_Norm_layer_call_fn_8693
*__inference_Block1_Norm_layer_call_fn_8706�
���
FullArgSpec)
args!�
jself
jinputs

jtraining
varargs
 
varkw
 
defaults�
p 

kwonlyargs� 
kwonlydefaults� 
annotations� *
 
�2�
E__inference_Block1_Norm_layer_call_and_return_conditional_losses_8724
E__inference_Block1_Norm_layer_call_and_return_conditional_losses_8742�
���
FullArgSpec)
args!�
jself
jinputs

jtraining
varargs
 
varkw
 
defaults�
p 

kwonlyargs� 
kwonlydefaults� 
annotations� *
 
,:* @2Block2_Conv/kernel
:@2Block2_Conv/bias
.
K0
L1"
trackable_list_wrapper
.
K0
L1"
trackable_list_wrapper
 "
trackable_list_wrapper
�
�non_trainable_variables
�layers
�metrics
 �layer_regularization_losses
�layer_metrics
M	variables
Ntrainable_variables
Oregularization_losses
Q__call__
*R&call_and_return_all_conditional_losses
&R"call_and_return_conditional_losses"
_generic_user_object
�2�
*__inference_Block2_Conv_layer_call_fn_8751�
���
FullArgSpec
args�
jself
jinputs
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 
�2�
E__inference_Block2_Conv_layer_call_and_return_conditional_losses_8762�
���
FullArgSpec
args�
jself
jinputs
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
�
�non_trainable_variables
�layers
�metrics
 �layer_regularization_losses
�layer_metrics
S	variables
Ttrainable_variables
Uregularization_losses
W__call__
*X&call_and_return_all_conditional_losses
&X"call_and_return_conditional_losses"
_generic_user_object
�2�
*__inference_Block2_Pool_layer_call_fn_8767�
���
FullArgSpec
args�
jself
jinputs
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 
�2�
E__inference_Block2_Pool_layer_call_and_return_conditional_losses_8772�
���
FullArgSpec
args�
jself
jinputs
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 
 "
trackable_list_wrapper
:@2Block2_Norm/gamma
:@2Block2_Norm/beta
':%@ (2Block2_Norm/moving_mean
+:)@ (2Block2_Norm/moving_variance
<
Z0
[1
\2
]3"
trackable_list_wrapper
.
Z0
[1"
trackable_list_wrapper
 "
trackable_list_wrapper
�
�non_trainable_variables
�layers
�metrics
 �layer_regularization_losses
�layer_metrics
^	variables
_trainable_variables
`regularization_losses
b__call__
*c&call_and_return_all_conditional_losses
&c"call_and_return_conditional_losses"
_generic_user_object
�2�
*__inference_Block2_Norm_layer_call_fn_8785
*__inference_Block2_Norm_layer_call_fn_8798�
���
FullArgSpec)
args!�
jself
jinputs

jtraining
varargs
 
varkw
 
defaults�
p 

kwonlyargs� 
kwonlydefaults� 
annotations� *
 
�2�
E__inference_Block2_Norm_layer_call_and_return_conditional_losses_8816
E__inference_Block2_Norm_layer_call_and_return_conditional_losses_8834�
���
FullArgSpec)
args!�
jself
jinputs

jtraining
varargs
 
varkw
 
defaults�
p 

kwonlyargs� 
kwonlydefaults� 
annotations� *
 
-:+@�2Block3_Conv/kernel
:�2Block3_Conv/bias
.
d0
e1"
trackable_list_wrapper
.
d0
e1"
trackable_list_wrapper
 "
trackable_list_wrapper
�
�non_trainable_variables
�layers
�metrics
 �layer_regularization_losses
�layer_metrics
f	variables
gtrainable_variables
hregularization_losses
j__call__
*k&call_and_return_all_conditional_losses
&k"call_and_return_conditional_losses"
_generic_user_object
�2�
*__inference_Block3_Conv_layer_call_fn_8843�
���
FullArgSpec
args�
jself
jinputs
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 
�2�
E__inference_Block3_Conv_layer_call_and_return_conditional_losses_8854�
���
FullArgSpec
args�
jself
jinputs
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
�
�non_trainable_variables
�layers
�metrics
 �layer_regularization_losses
�layer_metrics
l	variables
mtrainable_variables
nregularization_losses
p__call__
*q&call_and_return_all_conditional_losses
&q"call_and_return_conditional_losses"
_generic_user_object
�2�
*__inference_Block3_Pool_layer_call_fn_8859�
���
FullArgSpec
args�
jself
jinputs
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 
�2�
E__inference_Block3_Pool_layer_call_and_return_conditional_losses_8864�
���
FullArgSpec
args�
jself
jinputs
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 
 "
trackable_list_wrapper
 :�2Block3_Norm/gamma
:�2Block3_Norm/beta
(:&� (2Block3_Norm/moving_mean
,:*� (2Block3_Norm/moving_variance
<
s0
t1
u2
v3"
trackable_list_wrapper
.
s0
t1"
trackable_list_wrapper
 "
trackable_list_wrapper
�
�non_trainable_variables
�layers
�metrics
 �layer_regularization_losses
�layer_metrics
w	variables
xtrainable_variables
yregularization_losses
{__call__
*|&call_and_return_all_conditional_losses
&|"call_and_return_conditional_losses"
_generic_user_object
�2�
*__inference_Block3_Norm_layer_call_fn_8877
*__inference_Block3_Norm_layer_call_fn_8890�
���
FullArgSpec)
args!�
jself
jinputs

jtraining
varargs
 
varkw
 
defaults�
p 

kwonlyargs� 
kwonlydefaults� 
annotations� *
 
�2�
E__inference_Block3_Norm_layer_call_and_return_conditional_losses_8908
E__inference_Block3_Norm_layer_call_and_return_conditional_losses_8926�
���
FullArgSpec)
args!�
jself
jinputs

jtraining
varargs
 
varkw
 
defaults�
p 

kwonlyargs� 
kwonlydefaults� 
annotations� *
 
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
�
�non_trainable_variables
�layers
�metrics
 �layer_regularization_losses
�layer_metrics
}	variables
~trainable_variables
regularization_losses
�__call__
+�&call_and_return_all_conditional_losses
'�"call_and_return_conditional_losses"
_generic_user_object
�2�
/__inference_GlobalMaxPooling_layer_call_fn_8931�
���
FullArgSpec
args�
jself
jinputs
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 
�2�
J__inference_GlobalMaxPooling_layer_call_and_return_conditional_losses_8937�
���
FullArgSpec
args�
jself
jinputs
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 
&:$
��2Final_Dense/kernel
:�2Final_Dense/bias
0
�0
�1"
trackable_list_wrapper
0
�0
�1"
trackable_list_wrapper
 "
trackable_list_wrapper
�
�non_trainable_variables
�layers
�metrics
 �layer_regularization_losses
�layer_metrics
�	variables
�trainable_variables
�regularization_losses
�__call__
+�&call_and_return_all_conditional_losses
'�"call_and_return_conditional_losses"
_generic_user_object
�2�
*__inference_Final_Dense_layer_call_fn_8946�
���
FullArgSpec
args�
jself
jinputs
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 
�2�
E__inference_Final_Dense_layer_call_and_return_conditional_losses_8957�
���
FullArgSpec
args�
jself
jinputs
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 
 :	�2output/kernel
:2output/bias
0
�0
�1"
trackable_list_wrapper
0
�0
�1"
trackable_list_wrapper
 "
trackable_list_wrapper
�
�non_trainable_variables
�layers
�metrics
 �layer_regularization_losses
�layer_metrics
�	variables
�trainable_variables
�regularization_losses
�__call__
+�&call_and_return_all_conditional_losses
'�"call_and_return_conditional_losses"
_generic_user_object
�2�
%__inference_output_layer_call_fn_8966�
���
FullArgSpec
args�
jself
jinputs
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 
�2�
@__inference_output_layer_call_and_return_conditional_losses_8977�
���
FullArgSpec
args�
jself
jinputs
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 
:	 (2	Adam/iter
: (2Adam/beta_1
: (2Adam/beta_2
: (2
Adam/decay
: (2Adam/learning_rate
X
*0
+1
C2
D3
\4
]5
u6
v7"
trackable_list_wrapper
�
0
1
2
3
4
5
6
7
	8

9
10
11
12
13
14"
trackable_list_wrapper
0
�0
�1"
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_dict_wrapper
�B�
"__inference_signature_wrapper_8558input_1"�
���
FullArgSpec
args� 
varargs
 
varkwjkwargs
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_dict_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_dict_wrapper
.
*0
+1"
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_dict_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_dict_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_dict_wrapper
.
C0
D1"
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_dict_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_dict_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_dict_wrapper
.
\0
]1"
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_dict_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_dict_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_dict_wrapper
.
u0
v1"
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_dict_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_dict_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_dict_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_dict_wrapper
R

�total

�count
�	variables
�	keras_api"
_tf_keras_metric
c

�total

�count
�
_fn_kwargs
�	variables
�	keras_api"
_tf_keras_metric
:  (2total
:  (2count
0
�0
�1"
trackable_list_wrapper
.
�	variables"
_generic_user_object
:  (2total
:  (2count
 "
trackable_dict_wrapper
0
�0
�1"
trackable_list_wrapper
.
�	variables"
_generic_user_object
1:/2Adam/Block0_Conv/kernel/m
#:!2Adam/Block0_Conv/bias/m
$:"2Adam/Block0_Norm/gamma/m
#:!2Adam/Block0_Norm/beta/m
1:/ 2Adam/Block1_Conv/kernel/m
#:! 2Adam/Block1_Conv/bias/m
$:" 2Adam/Block1_Norm/gamma/m
#:! 2Adam/Block1_Norm/beta/m
1:/ @2Adam/Block2_Conv/kernel/m
#:!@2Adam/Block2_Conv/bias/m
$:"@2Adam/Block2_Norm/gamma/m
#:!@2Adam/Block2_Norm/beta/m
2:0@�2Adam/Block3_Conv/kernel/m
$:"�2Adam/Block3_Conv/bias/m
%:#�2Adam/Block3_Norm/gamma/m
$:"�2Adam/Block3_Norm/beta/m
+:)
��2Adam/Final_Dense/kernel/m
$:"�2Adam/Final_Dense/bias/m
%:#	�2Adam/output/kernel/m
:2Adam/output/bias/m
1:/2Adam/Block0_Conv/kernel/v
#:!2Adam/Block0_Conv/bias/v
$:"2Adam/Block0_Norm/gamma/v
#:!2Adam/Block0_Norm/beta/v
1:/ 2Adam/Block1_Conv/kernel/v
#:! 2Adam/Block1_Conv/bias/v
$:" 2Adam/Block1_Norm/gamma/v
#:! 2Adam/Block1_Norm/beta/v
1:/ @2Adam/Block2_Conv/kernel/v
#:!@2Adam/Block2_Conv/bias/v
$:"@2Adam/Block2_Norm/gamma/v
#:!@2Adam/Block2_Norm/beta/v
2:0@�2Adam/Block3_Conv/kernel/v
$:"�2Adam/Block3_Conv/bias/v
%:#�2Adam/Block3_Norm/gamma/v
$:"�2Adam/Block3_Norm/beta/v
+:)
��2Adam/Final_Dense/kernel/v
$:"�2Adam/Final_Dense/bias/v
%:#	�2Adam/output/kernel/v
:2Adam/output/bias/v�
E__inference_Block0_Conv_layer_call_and_return_conditional_losses_8578p9�6
/�,
*�'
inputs�����������
� "/�,
%�"
0�����������
� �
*__inference_Block0_Conv_layer_call_fn_8567c9�6
/�,
*�'
inputs�����������
� ""�������������
E__inference_Block0_Norm_layer_call_and_return_conditional_losses_8632�()*+M�J
C�@
:�7
inputs+���������������������������
p 
� "?�<
5�2
0+���������������������������
� �
E__inference_Block0_Norm_layer_call_and_return_conditional_losses_8650�()*+M�J
C�@
:�7
inputs+���������������������������
p
� "?�<
5�2
0+���������������������������
� �
*__inference_Block0_Norm_layer_call_fn_8601�()*+M�J
C�@
:�7
inputs+���������������������������
p 
� "2�/+����������������������������
*__inference_Block0_Norm_layer_call_fn_8614�()*+M�J
C�@
:�7
inputs+���������������������������
p
� "2�/+����������������������������
E__inference_Block0_Pool_layer_call_and_return_conditional_losses_8588�R�O
H�E
C�@
inputs4������������������������������������
� "H�E
>�;
04������������������������������������
� �
*__inference_Block0_Pool_layer_call_fn_8583�R�O
H�E
C�@
inputs4������������������������������������
� ";�84�������������������������������������
E__inference_Block1_Conv_layer_call_and_return_conditional_losses_8670l237�4
-�*
(�%
inputs���������@V
� "-�*
#� 
0���������@V 
� �
*__inference_Block1_Conv_layer_call_fn_8659_237�4
-�*
(�%
inputs���������@V
� " ����������@V �
E__inference_Block1_Norm_layer_call_and_return_conditional_losses_8724�ABCDM�J
C�@
:�7
inputs+��������������������������� 
p 
� "?�<
5�2
0+��������������������������� 
� �
E__inference_Block1_Norm_layer_call_and_return_conditional_losses_8742�ABCDM�J
C�@
:�7
inputs+��������������������������� 
p
� "?�<
5�2
0+��������������������������� 
� �
*__inference_Block1_Norm_layer_call_fn_8693�ABCDM�J
C�@
:�7
inputs+��������������������������� 
p 
� "2�/+��������������������������� �
*__inference_Block1_Norm_layer_call_fn_8706�ABCDM�J
C�@
:�7
inputs+��������������������������� 
p
� "2�/+��������������������������� �
E__inference_Block1_Pool_layer_call_and_return_conditional_losses_8680�R�O
H�E
C�@
inputs4������������������������������������
� "H�E
>�;
04������������������������������������
� �
*__inference_Block1_Pool_layer_call_fn_8675�R�O
H�E
C�@
inputs4������������������������������������
� ";�84�������������������������������������
E__inference_Block2_Conv_layer_call_and_return_conditional_losses_8762lKL7�4
-�*
(�%
inputs��������� + 
� "-�*
#� 
0��������� +@
� �
*__inference_Block2_Conv_layer_call_fn_8751_KL7�4
-�*
(�%
inputs��������� + 
� " ���������� +@�
E__inference_Block2_Norm_layer_call_and_return_conditional_losses_8816�Z[\]M�J
C�@
:�7
inputs+���������������������������@
p 
� "?�<
5�2
0+���������������������������@
� �
E__inference_Block2_Norm_layer_call_and_return_conditional_losses_8834�Z[\]M�J
C�@
:�7
inputs+���������������������������@
p
� "?�<
5�2
0+���������������������������@
� �
*__inference_Block2_Norm_layer_call_fn_8785�Z[\]M�J
C�@
:�7
inputs+���������������������������@
p 
� "2�/+���������������������������@�
*__inference_Block2_Norm_layer_call_fn_8798�Z[\]M�J
C�@
:�7
inputs+���������������������������@
p
� "2�/+���������������������������@�
E__inference_Block2_Pool_layer_call_and_return_conditional_losses_8772�R�O
H�E
C�@
inputs4������������������������������������
� "H�E
>�;
04������������������������������������
� �
*__inference_Block2_Pool_layer_call_fn_8767�R�O
H�E
C�@
inputs4������������������������������������
� ";�84�������������������������������������
E__inference_Block3_Conv_layer_call_and_return_conditional_losses_8854mde7�4
-�*
(�%
inputs���������@
� ".�+
$�!
0����������
� �
*__inference_Block3_Conv_layer_call_fn_8843`de7�4
-�*
(�%
inputs���������@
� "!������������
E__inference_Block3_Norm_layer_call_and_return_conditional_losses_8908�stuvN�K
D�A
;�8
inputs,����������������������������
p 
� "@�=
6�3
0,����������������������������
� �
E__inference_Block3_Norm_layer_call_and_return_conditional_losses_8926�stuvN�K
D�A
;�8
inputs,����������������������������
p
� "@�=
6�3
0,����������������������������
� �
*__inference_Block3_Norm_layer_call_fn_8877�stuvN�K
D�A
;�8
inputs,����������������������������
p 
� "3�0,�����������������������������
*__inference_Block3_Norm_layer_call_fn_8890�stuvN�K
D�A
;�8
inputs,����������������������������
p
� "3�0,�����������������������������
E__inference_Block3_Pool_layer_call_and_return_conditional_losses_8864�R�O
H�E
C�@
inputs4������������������������������������
� "H�E
>�;
04������������������������������������
� �
*__inference_Block3_Pool_layer_call_fn_8859�R�O
H�E
C�@
inputs4������������������������������������
� ";�84�������������������������������������
E__inference_Final_Dense_layer_call_and_return_conditional_losses_8957`��0�-
&�#
!�
inputs����������
� "&�#
�
0����������
� �
*__inference_Final_Dense_layer_call_fn_8946S��0�-
&�#
!�
inputs����������
� "������������
J__inference_GlobalMaxPooling_layer_call_and_return_conditional_losses_8937�R�O
H�E
C�@
inputs4������������������������������������
� ".�+
$�!
0������������������
� �
/__inference_GlobalMaxPooling_layer_call_fn_8931wR�O
H�E
C�@
inputs4������������������������������������
� "!��������������������
__inference__wrapped_model_7156� ()*+23ABCDKLZ[\]destuv����:�7
0�-
+�(
input_1�����������
� "/�,
*
output �
output����������
@__inference_output_layer_call_and_return_conditional_losses_8977_��0�-
&�#
!�
inputs����������
� "%�"
�
0���������
� {
%__inference_output_layer_call_fn_8966R��0�-
&�#
!�
inputs����������
� "�����������
D__inference_sequential_layer_call_and_return_conditional_losses_8076� ()*+23ABCDKLZ[\]destuv����B�?
8�5
+�(
input_1�����������
p 

 
� "%�"
�
0���������
� �
D__inference_sequential_layer_call_and_return_conditional_losses_8151� ()*+23ABCDKLZ[\]destuv����B�?
8�5
+�(
input_1�����������
p

 
� "%�"
�
0���������
� �
D__inference_sequential_layer_call_and_return_conditional_losses_8387� ()*+23ABCDKLZ[\]destuv����A�>
7�4
*�'
inputs�����������
p 

 
� "%�"
�
0���������
� �
D__inference_sequential_layer_call_and_return_conditional_losses_8495� ()*+23ABCDKLZ[\]destuv����A�>
7�4
*�'
inputs�����������
p

 
� "%�"
�
0���������
� �
)__inference_sequential_layer_call_fn_7683� ()*+23ABCDKLZ[\]destuv����B�?
8�5
+�(
input_1�����������
p 

 
� "�����������
)__inference_sequential_layer_call_fn_8001� ()*+23ABCDKLZ[\]destuv����B�?
8�5
+�(
input_1�����������
p

 
� "�����������
)__inference_sequential_layer_call_fn_8218 ()*+23ABCDKLZ[\]destuv����A�>
7�4
*�'
inputs�����������
p 

 
� "�����������
)__inference_sequential_layer_call_fn_8279 ()*+23ABCDKLZ[\]destuv����A�>
7�4
*�'
inputs�����������
p

 
� "�����������
"__inference_signature_wrapper_8558� ()*+23ABCDKLZ[\]destuv����E�B
� 
;�8
6
input_1+�(
input_1�����������"/�,
*
output �
output���������