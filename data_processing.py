import librosa as lr
from librosa.util import fix_length
import soundfile as sf
import contextlib
import wave
import glob
import os
import utils
import sys

#path = './audio/test.wav'

#lr.core.load(path, sr=22050, mono=True, offset=0.0, duration=None, dtype=np.dtype(np.float32), res_type='kaiser_best')


def prepare_data(folder, min_audio_length=1, max_audio_length=5):

    
    counter_short = 0
    counter_long = 0
    
    # Retrieve all wave files in a folder
    files = glob.glob(folder + '/*.wav')


    prepared_data_folder = folder + '/../prepared_data'
    if not os.path.exists(prepared_data_folder):
        os.makedirs(prepared_data_folder)

    filtered_files = []

    for file in files:
        with contextlib.closing(wave.open(file,'r')) as file_open:

            frames = file_open.getnframes()
            rate = file_open.getframerate()
            duration = frames / float(rate)

            # Apply filter
            if duration <= max_audio_length and duration >= min_audio_length:
                filtered_files.append(file)
                counter_short += 1
            else:
                counter_long += 1 

    output_list = []

    counter_padding = 0

    for file in filtered_files:


        utils.print_progress_bar(counter_padding, len(filtered_files), prefix="Cutting and Padding progress:", suffix="Complete", length=50 )
        
        counter_padding+=1
        
        data, sr = lr.load(file, mono=True)

        padded_audio = fix_length(data, size= max_audio_length * sr)

        sf.write(prepared_data_folder + "/" + os.path.basename(file), padded_audio, sr, 'PCM_24')

        output_list.append(prepared_data_folder + "/" + os.path.basename(file))

    print(counter_short)
    print(counter_long)

    return output_list




def compile_to_dataset(folder):

    files = glob.glob(folder + '/*.pkl')



if __name__ == '__main__':

    path = sys.argv[1]

    if len(sys.argv) == 1:
        print(">>> No path was given!")


    if os.path.exists(path):

        print(">>> Processing files in folder " + path)


        prepare_data(path, 3, 5)
    
    #save_spectogram_png(create_mel_spectogram("/audio/test"))
    
    #plot_audio("/audio/test")
    
    #data_visualizer.plot_mel_spectogram(create_mel_spectogram("/audio/test.wav", 4))


# TODO
#   
#   Script zum Daten aussortieren
#   Unsupervised
#       Autoencoder
#       UMAP
