import pickle
import glob
import os
import utils
import librosa as lr
import numpy as np
from PIL import Image
import sys
from datetime import datetime


# NOT FUNCTIONAL
def save_spectogram_png(data):
    im = Image.fromarray(data).convert('RGB')
    im.save("your_file.png")

def create_mel_spectogram(audio, sr_ratio=1):


    audio, sr = lr.load(audio)
    
    #lr.feature.melspectrogram(y=audio, sr=sr)

    #D = np.abs(lr.stft(audio))**2
    
    #spectogram = lr.feature.melspectrogram(S=D)

    #spectogram = lr.feature.melspectrogram(y=audio, n_fft=2048, hop_length=256, win_length=2048, n_mels=64, fmax=int(sr/2))

    spectogram = lr.feature.melspectrogram(y=audio, n_fft=2048, hop_length=512, win_length=2048)
    
    # Adjust to fit db scale 
    spectogram = lr.amplitude_to_db(spectogram, ref=np.max)

    return spectogram

def convert_audio_folder_to_spec(folder, sr_ratio=1):

    '''
        Converts folder of audio wave files to a folder of spectograms in pkl format
    '''

    data = []

    with open(path + "/complete_labels.pkl", 'rb') as f:
        data = pickle.load(f)

    #if not os.path.exists(folder + '/../spectograms'):
    #    os.makedirs(folder + '/../spectograms')

    

    print("Converting audio files to spectogram data...")

    counter = 0

    spectogram_array = []
    label_array = []
    
    for file in data:

        spectogram = create_mel_spectogram(folder + "/" + file[0], sr_ratio)

        if len(spectogram_array) == 0:
            spectogram_array = [spectogram]
        else:
            spectogram_array = np.concatenate((spectogram_array, [spectogram]))

        print(spectogram.shape)

        if len(label_array) == 0:
            label_array = [file[1]]
        else:
            label_array = np.concatenate((label_array, [file[1]]))


        #spectogram_array.append(spectogram)
        #label_array.append(label_array)

        

        utils.print_progress_bar(counter, len(data), prefix="Conversion progress:", suffix="Complete", length=50)
        counter += 1

        #print("Written " + os.path.basename(file))


    #print(spectogram_array)

    result_array = [spectogram_array, label_array]


    # print("Resulting data shape:")
    # print(result_np_array.shape)
    # print("Spectogram shape:")
    # print(result_np_array[0][0].shape)


    if not os.path.exists(folder + '/../complete_datasets'):
        os.makedirs(folder + '/../complete_datasets')

    now = datetime.now()
    date_time = now.strftime("%m-%d-%Y-%H-%M-%S")

    filehandler = open(folder + '/../complete_datasets/my_dataset'+  str(date_time) + '.pkl' ,"wb")
    pickle.dump(result_array ,filehandler)
    filehandler.close()








if __name__ == '__main__':
    path = sys.argv[1]

    if len(sys.argv) == 1:
        print(">>> No path was given!")


    if os.path.exists(path):

        print(">>> Starting conversion process for files in folder " + path)

        if os.path.exists(path + "/complete_labels.pkl"):
            convert_audio_folder_to_spec(path, 1)

        else:
            print(">>> No 'complete_labels.pkl' file was found! Execution aborted.")

