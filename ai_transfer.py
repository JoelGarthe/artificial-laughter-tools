from os.path import exists
import pickle
import math
import tensorflow as tf
from tensorflow.keras import layers
import numpy as np
import utils
import matplotlib.pyplot as plt
import datetime
import os 
import sys 

#data_file = "./complete_datasets/my_dataset_shifted_high_freq.pkl"

data_file = sys.argv[1]

if len(sys.argv) == 1:
    print(">>> No data file was given!")


# Data array to save later
data = []

CLASSES_AMOUNT = 1

# Load the csv/labeled data if there is an uncomplete one 
if exists(data_file):
    file_opener = open(data_file,'rb')
    
    temp = pickle.load(file_opener)
    
    data = temp[0]
    labels = temp [1]

    file_opener.close()

# Spread data on three layers since Transfer Learning requires RBG Images
data = np.repeat(data[..., np.newaxis], 3, -1)


# Seperate data into training and testdata
split_index=math.floor(len(data)*0.8)
X_train= data[:split_index]
Y_train = labels[:split_index]
X_test = data[split_index+1:]
Y_test = labels[split_index+1:]

print(len(X_train))
print(len(Y_train))
print(len(X_test))
print(len(Y_test))



base_model = tf.keras.applications.MobileNetV2(
    input_shape=(data[0].shape[0], data[1].shape[1], 3), 
    include_top=False, 
    weights='imagenet', 
    pooling='avg'
    )

base_model.trainable = False

#print(base_model.summary())


model = tf.keras.Sequential([
    base_model,
    layers.Dense(512, activation='relu', name='hidden_layer'),
    layers.Dense(CLASSES_AMOUNT, activation='sigmoid', name='output')
])

#print(model.summary())


model.compile(optimizer='adam', loss='binary_crossentropy', metrics=['binary_accuracy'])


now = datetime.datetime.now()

checkpoint_path = "./saved_models/" + str(now.strftime("%m-%d-%Y-%H-%M-%S-transfer")) + "/"

filename = str(now.strftime("%m-%d-%Y-%H-%M-%S") + ".h5")

if not os.path.exists(checkpoint_path):
        os.makedirs(checkpoint_path)

checkpoint_dir = os.path.dirname(checkpoint_path + filename)

checkpoint = tf.keras.callbacks.ModelCheckpoint(save_best_only=True,
                                                monitor="val_binary_accuracy",
                                                filepath=checkpoint_path,
                                                 save_weights_only=False,
                                                 verbose=1)
callbacks = [checkpoint]

history = model.fit(X_train, Y_train, batch_size = 13, epochs = 100, shuffle=True, validation_data=(X_test, Y_test), callbacks=callbacks)

model.evaluate(X_test, Y_test, batch_size = 1, verbose = 1)

Y_pred = model.predict(X_test)

print(utils.get_confusion_matrix(Y_test, Y_pred, threshold=0.8))


# summarize history for accuracy
plt.plot(history.history['binary_accuracy'])
plt.plot(history.history['val_binary_accuracy'])
plt.title('model accuracy')
plt.ylabel('accuracy')
plt.xlabel('epoch')
plt.legend(['train', 'test'], loc='upper left')
plt.show()

print(history)

